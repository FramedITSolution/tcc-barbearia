﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH.Vales
{
    class BusinessVR
    {
        Database.RH.Vales.DatabaseVR db = new Database.RH.Vales.DatabaseVR();

        public void InserirVR(Database.Entity.tb_vale_refeicao vr)
        {
            if (string.IsNullOrWhiteSpace(vr.ds_codigo_vr))
                throw new ArgumentException("Informe o código do vale refeição");

            if (vr.vl_vale_refeicao == 0.00m)
                throw new ArgumentException("Informe o valor do vale refeição");

            db.InserirVR(vr);
        }

        public void AlterarVR(Database.Entity.tb_vale_refeicao vr)
        {
            if (string.IsNullOrWhiteSpace(vr.ds_codigo_vr))
                throw new ArgumentException("Informe o código do vale refeição");

            if (vr.vl_vale_refeicao == 0.00m)
                throw new ArgumentException("Informe o valor do vale refeição");

            db.AlterarVR(vr);
        }

        public Database.Entity.tb_vale_refeicao ConsultarVR(string código, decimal valor)
        {
            if (string.IsNullOrWhiteSpace(código))
                throw new ArgumentException("Informe o código do vale refeição");

            if (valor == 0.00m)
                throw new ArgumentException("Informe o valor do vale refeição");
            return db.ConsultarVR(código, valor);
        }

        public Database.Entity.tb_vale_refeicao ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o id");

            return db.ConsultarPorId(id);
        }

        public void RemoverVR(Database.Entity.tb_vale_refeicao vr)
        {
            if (vr.id_vale_refeicao == 0)
                throw new ArgumentException("Informe o vale refeição");

            db.RemoverVR(vr);
        }
    }
}
