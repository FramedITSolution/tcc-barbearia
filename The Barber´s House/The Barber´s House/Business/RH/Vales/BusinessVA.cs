﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH.Vales
{
    class BusinessVA
    {
        Database.RH.Vales.DatabaseVA db = new Database.RH.Vales.DatabaseVA();

        public void InserirVA(Database.Entity.tb_vale_alimentacao va)
        {
            if (string.IsNullOrWhiteSpace(va.ds_codigo_va))
                throw new ArgumentException("Informe o código do vale alimentação");

            if (va.vl_vale_alimentacao == 0.00m)
                throw new ArgumentException("Informe o valor do vale alimentação");

            db.InserirVA(va);
        }

        public void AlterarVA(Database.Entity.tb_vale_alimentacao va)
        {
            if (string.IsNullOrWhiteSpace(va.ds_codigo_va))
                throw new ArgumentException("Informe o código do vale alimentação");

            if (va.vl_vale_alimentacao == 0.00m)
                throw new ArgumentException("Informe o valor do vale alimentação");

            db.AlterarVA(va);
        }

        public Database.Entity.tb_vale_alimentacao ConsultarVa(string código, decimal valor)
        {
            if (string.IsNullOrWhiteSpace(código))
                throw new ArgumentException("Informe o código do vale alimentação");

            if (valor == 0.00m)
                throw new ArgumentException("Informe o valor do vale alimentação");
            return db.ConsultarVA(código, valor);
        }

        public Database.Entity.tb_vale_alimentacao ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o id");

            return db.ConsultarPorId(id);
        }

        public void RemoverVA(Database.Entity.tb_vale_alimentacao va)
        {
            if (va.id_vale_alimentacao == 0)
                throw new ArgumentException("Informe o vale alimentação");

            db.RemoverVA(va);
        }
    }
}
