﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH.Vales
{
    class BusinessVT
    {
        Database.RH.Vales.DatabaseVT db = new Database.RH.Vales.DatabaseVT();

        public void InserirVT(Database.Entity.tb_vale_transporte vt)
        {
            if (string.IsNullOrWhiteSpace(vt.ds_codigo_bilhete_unico))
                throw new ArgumentException("Informe o código do vale alimentação");

            if (vt.vl_vt == 0.00m)
                throw new ArgumentException("Informe o valor do vale alimentação");

            db.InserirVT(vt);
        }

        public void AlterarVT(Database.Entity.tb_vale_transporte vt)
        {
            if (string.IsNullOrWhiteSpace(vt.ds_codigo_bilhete_unico))
                throw new ArgumentException("Informe o código do vale alimentação");

            if (vt.vl_vt == 0.00m)
                throw new ArgumentException("Informe o valor do vale alimentação");

            db.AlterarVT(vt);
        }

        public Database.Entity.tb_vale_transporte ConsultarVT(string código, decimal valor)
        {
            if (string.IsNullOrWhiteSpace(código))
                throw new ArgumentException("Informe o código do vale alimentação");

            if (valor == 0.00m)
                throw new ArgumentException("Informe o valor do vale alimentação");

            return db.ConsultarVT(código, valor);
        }

        public Database.Entity.tb_vale_transporte ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o id");

            return db.ConsultarPorId(id);
        }

        public void RemoverVT(Database.Entity.tb_vale_transporte vt)
        {
            if (vt.id_vale_transporte == 0)
                throw new ArgumentException("Informe o vale transporte");

            db.RemoverVT(vt);
        }
    }
}
