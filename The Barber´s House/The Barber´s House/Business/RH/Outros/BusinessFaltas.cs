﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH.Outros
{
    class BusinessFaltas
    {
        Database.RH.Outros.DatabaseFaltas faltas = new Database.RH.Outros.DatabaseFaltas();

        public void NovaFalta(Database.Entity.tb_faltas falta)
        {
            if (falta.id_funcionario == 0)
                throw new ArgumentException("Informe o funcionário");

            if (falta.dt_falta < DateTime.Now.Date)
                throw new ArgumentException("Essa data já passou, informe outra");

            if (falta.dt_falta > DateTime.Now.Date)
                throw new ArgumentException("Data inválida");

            faltas.NovaFalta(falta);
        }

        public List<Database.Entity.tb_faltas> ConsultarFaltasPorFunc(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe um id");
            return faltas.ConsultarFaltasPorFunc(id);
        }

        public List<Database.Entity.tb_faltas> ListarFaltas()
        {
            return faltas.ListarFaltas();
        }

        public void AlterarFalta(Database.Entity.tb_faltas falta)
        {
            if (falta.id_funcionario == 0)
                throw new ArgumentException("Informe o funcionário");

            if (falta.dt_falta < DateTime.Now.Date)
                throw new ArgumentException("Essa data já passou, informe outra");

            if (falta.dt_falta > DateTime.Now.Date)
                throw new ArgumentException("Data inválida");

            faltas.AlterarFalta(falta);
        }

        public void RemoverFaltaPorIDF(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o funcionário");

            faltas.RemoverFaltaPorIDF(id);
        }
    }
}
