﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH.Outros
{
    class BusinessAtrasos
    {
        Database.RH.Outros.DatabaseAtrasos db = new Database.RH.Outros.DatabaseAtrasos();

        public void NovaAtraso(Database.Entity.tb_atrasos atraso)
        {
            if (atraso.id_funcionario == 0)
                throw new ArgumentException("Informe o funcionário");

            if (atraso.dt_atraso < DateTime.Now.Date)
                throw new ArgumentException("Essa data já passou, informe outra");

            if (atraso.dt_atraso > DateTime.Now.Date)
                throw new ArgumentException("Data inválida");

            db.NovoAtraso(atraso);
        }

        public List<Database.Entity.tb_atrasos> ConsultarAtrasosPorFunc(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe um id");
            return db.ConsultarAtrasosPorFunc(id);
        }

        public List<Database.Entity.tb_atrasos> ListarAtrasos()
        {
            return db.ListarAtrasos();
        }

        public void AlterarAtraso(Database.Entity.tb_atrasos atraso)
        {
            if (atraso.id_funcionario == 0)
                throw new ArgumentException("Informe o funcionário");

            if (atraso.dt_atraso < DateTime.Now.Date)
                throw new ArgumentException("Essa data já passou, informe outra");

            if (atraso.dt_atraso > DateTime.Now.Date)
                throw new ArgumentException("Data inválida");

            db.AlterarAtraso(atraso);
        }

        public void RemoverAtrasoPorIDF(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o funcionário");

            db.RemoverAtrasoPorIDF(id);
        }
    }
}
