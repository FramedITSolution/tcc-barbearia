﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH.Outros
{
    class BusinessSegurodeVida
    {
        Database.RH.Outros.DatabaseSeguroVida seguroDeVidaDatabase = new Database.RH.Outros.DatabaseSeguroVida();

        public Database.Entity.tb_seguro_vida ConsultarPlano(decimal plano)
        {
            return seguroDeVidaDatabase.ConsultarPlano(plano);
        }

        public List<Database.Entity.tb_seguro_vida> ListarTodos()
        {
            return seguroDeVidaDatabase.ListarTodos();
        }
    }
}
