﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH.Outros
{
    class BusinessFGTS
    {
        Database.RH.Outros.DatabaseFGTS fgts = new Database.RH.Outros.DatabaseFGTS();

        public Database.Entity.tb_fgts ListarTodos()
        {
            return fgts.ListarTodos();
        }

        public Database.Entity.tb_inss Listar()
        {
            return fgts.Listar();
        }
    }
}
