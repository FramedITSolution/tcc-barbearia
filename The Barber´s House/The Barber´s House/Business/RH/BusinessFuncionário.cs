﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.RH
{
    class BusinessFuncionário
    {
        Database.RH.DatabaseFuncionário db = new Database.RH.DatabaseFuncionário();
        Objetos.UsoGeral validar = new Objetos.UsoGeral();

        public void NovoFuncionário(Database.Entity.tb_funcionario func)
        {
            //Regex regex = new Regex("^[0-9]{3}.?[0-9]{3}.?[0-9]{3}-?[0-9]{2}");

            //if (regex.IsMatch(func.ds_cpf))
            //    throw new ArgumentException("Informe um CPF válido");

            if (string.IsNullOrWhiteSpace(func.nm_funcionario))
            {
                throw new ArgumentException("Informe o funcionário");
            }

            if (string.IsNullOrWhiteSpace(func.nm_mae))
            {
                throw new ArgumentException("Informe o nome da mãe do funcionário");
            }

            if (string.IsNullOrWhiteSpace(func.nm_empresa))
            {
                throw new ArgumentException("Informe a empresa");
            }

            if (func.vl_salario_bruto == 0.00m)
            {
                throw new ArgumentException("Informe o salário bruto");
            }

            if (func.vl_inss == 0.00m)
                throw new ArgumentException("Informe um valor acima de 0,00");

            if (func.vl_salario_familia == 0.00m)
                throw new ArgumentException("Informe um valor acima de 0,00");

            if (func.vl_irrf == 0.00m)
            {
                throw new ArgumentException("Informe o valor do irrf");
            }

            if (string.IsNullOrWhiteSpace(func.ds_genero))
            {
                throw new ArgumentException("Informe o gênero");
            }

            if (func.ds_genero == "Masculino")
                func.ds_genero = "M";
            if (func.ds_genero == "Feminino")
                func.ds_genero = "F";

            if (string.IsNullOrWhiteSpace(func.ds_email))
            {
                throw new ArgumentException("Informe o e-mail");
            }

            if (string.IsNullOrWhiteSpace(func.ds_cpf))
            {
                throw new ArgumentException("Informe o cpf");
            }

            if (string.IsNullOrWhiteSpace(func.ds_rg))
            {
                throw new ArgumentException("Informe o rg");
            }

            if (string.IsNullOrWhiteSpace(func.ds_endereco))
            {
                throw new ArgumentException("Informe o endereço");
            }

            if (string.IsNullOrWhiteSpace(func.ds_telefone))
            {
                throw new ArgumentException("Informe o telefone");
            }

            if (func.dt_nascimento == DateTime.Now)
            {
                throw new ArgumentException("Informe uma data válida");
            }          

            bool valido = validar.ValidarDatadeNasc(func.dt_nascimento, DateTime.Now);
            if (valido == false)
                throw new ArgumentException("Data de nascimento inválida, preencha novamente");

            if (func.dt_adimissao < DateTime.Now.Date)
                throw new ArgumentException("essa data já passou");

            bool contemFuncionario = db.VerificarFuncionario(func.nm_funcionario);

            if (contemFuncionario == true)
                throw new ArgumentException("Funcionario existente");

            db.NovoFuncionário(func);
        }

        public bool VerificarFuncionario(string funcionario)
        {
            //Remove os espaços
            funcionario = funcionario.Trim();
            //Verifica se não tem nada ou espaços em brancos
            if (string.IsNullOrWhiteSpace(funcionario))
            {
                throw new ArgumentException("Informe o Funcionário");
            }

            bool contem = db.VerificarFuncionario(funcionario);
            return contem;
        }

        public List<Database.Entity.tb_funcionario> ListarFuncionarios()
        {
            List<Database.Entity.tb_funcionario> lista = db.ListarFuncionarios();
            return lista;
        }

        public Database.Entity.tb_funcionario ConsultarPorNome(string nome)
        {
            Database.Entity.tb_funcionario model = db.ConsultarPorNome(nome);
            return model;
        }

        public List<Database.Entity.tb_funcionario> ConsultarPorID(int id)
        {            
            return db.ConsultarPorID(id);
        }

        public void AlterarFuncionario(Database.Entity.tb_funcionario func)
        {
            if (string.IsNullOrWhiteSpace(func.nm_funcionario))
            {
                throw new ArgumentException("Informe o funcionário");
            }

            if (string.IsNullOrWhiteSpace(func.nm_mae))
            {
                throw new ArgumentException("Informe o nome da mãe do funcionário");
            }

            if (string.IsNullOrWhiteSpace(func.nm_empresa))
            {
                throw new ArgumentException("Informe a empresa");
            }

            if (func.vl_salario_bruto == 0.00m)
            {
                throw new ArgumentException("Informe o salário bruto");
            }

            if (func.vl_inss == 0.00m)
                throw new ArgumentException("Informe um valor acima de 0,00");

            if (func.vl_salario_familia == 0.00m)
                throw new ArgumentException("Informe um valor acima de 0,00");

            if (func.vl_irrf == 0.00m)
            {
                throw new ArgumentException("Informe o valor do irrf");
            }

            if (string.IsNullOrWhiteSpace(func.ds_genero))
            {
                throw new ArgumentException("Informe o gênero");
            }

            if (func.ds_genero == "Masculino")
                func.ds_genero = "M";
            if (func.ds_genero == "Feminino")
                func.ds_genero = "F";

            if (string.IsNullOrWhiteSpace(func.ds_email))
            {
                throw new ArgumentException("Informe o e-mail");
            }

            if (string.IsNullOrWhiteSpace(func.ds_cpf))
            {
                throw new ArgumentException("Informe o cpf");
            }

            if (string.IsNullOrWhiteSpace(func.ds_rg))
            {
                throw new ArgumentException("Informe o rg");
            }

            if (string.IsNullOrWhiteSpace(func.ds_endereco))
            {
                throw new ArgumentException("Informe o endereço");
            }

            if (string.IsNullOrWhiteSpace(func.ds_telefone))
            {
                throw new ArgumentException("Informe o telefone");
            }

            if (func.dt_nascimento == DateTime.Now)
            {
                throw new ArgumentException("Informe uma data válida");
            }

            //if (string.IsNullOrWhiteSpace(func.tb_cargo.ds_cargo))
            //{
            //    throw new ArgumentException("Informe o cargo");
            //}

            //if (func.tb_seguro_vida.vl_plano == 0)
            //{
            //    throw new ArgumentException("Informe o valor do seguro de vida");
            //}

            if (func.dt_adimissao <= func.dt_nascimento)
                throw new ArgumentException("Data de admissão inválida");

            db.AlterarFuncionario(func);
        }


        public void RemoverFuncionário(Database.Entity.tb_funcionario func)
        {
            if (func.id_funcionario == 0)
                throw new ArgumentException("Informe o funcionário");
            db.RemoverFuncionario(func);
        }
    }
}
