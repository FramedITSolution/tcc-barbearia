﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Cortes
{
    class BusinessCortes
    {
        Database.Cortes.DatabaseCortes db = new Database.Cortes.DatabaseCortes();

        public void NovoCorte(Database.Entity.tb_corte corte)
        {
            if (string.IsNullOrWhiteSpace(corte.nm_corte))
            {
                throw new ArgumentException("Informe o cliente");
            }

            if (corte.vl_corte == 0)
                throw new ArgumentException("Informe o preço do corte");

            bool contemCorte = db.VerificarCorte(corte.nm_corte);

            if (contemCorte == true)
                throw new ArgumentException("Nome do corte já registrado, digite outro");

            db.NovoCorte(corte);
        }

        public List<Database.Entity.tb_corte> ConsultarPorID(int id)
        {
            List<Database.Entity.tb_corte> lista = db.ConsultarCorte(id);
            return lista;
        }

        public List<Database.Entity.tb_corte> ListarCortes()
        {
            List<Database.Entity.tb_corte> lista = db.ListarCortes();
            return lista;
        }

        public void AlterarCorte(Database.Entity.tb_corte corte)
        {
            if (string.IsNullOrWhiteSpace(corte.nm_corte))
            {
                throw new ArgumentException("Informe o cliente");
            }

            if (corte.vl_corte == 0)
                throw new ArgumentException("Informe o preço do corte");

            bool contemCorte = db.VerificarCorte(corte.nm_corte);

            if (contemCorte == true)
                throw new ArgumentException("Nome do corte já registrado, digite outro");

            db.AlterarCorte(corte);
        }

        public void RemoverCorte(Database.Entity.tb_corte model)
        {
            if (string.IsNullOrWhiteSpace(model.nm_corte))
            {
                throw new ArgumentException("Informe um corte");
            }
            db.RemoverCorte(model);
        }

        public void RemoverServiçoPorCorte(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o corte");

            db.RemoverServiçoPorCorte(id);
        }
    }             
}
