﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Fornecedor
{
    class BusinessFornecedor
    {
        Database.Fornecedor.DatabaseFornecedor db = new Database.Fornecedor.DatabaseFornecedor();

        public void NovoFornecedor(Database.Entity.tb_fornecedor model)
        {
            if (string.IsNullOrWhiteSpace(model.ds_razao_social))
            {
                throw new ArgumentException("Informe a razão social");
            }

            if (string.IsNullOrWhiteSpace(model.nm_fantasia))
            {
                throw new ArgumentException("Informe o fornecedor");
            }

            if (string.IsNullOrWhiteSpace(model.ds_email))
            {
                throw new ArgumentException("Informe o e-mail");
            }

            if (string.IsNullOrWhiteSpace(model.ds_cnpj))
            {
                throw new ArgumentException("Informe o cnpj");
            }

            if (string.IsNullOrWhiteSpace(model.ds_endereco))
            {
                throw new ArgumentException("Informe o endereço");
            }

            if (string.IsNullOrWhiteSpace(model.ds_cep))
                throw new ArgumentException("Informe o cep");

            if (model.ds_telefone.Length < 15)
                throw new ArgumentException("Telefone Inválido");

            if (model.ds_cep.Length < 9)
                throw new ArgumentException("CEP Inválido");

            if (model.ds_cnpj.Length < 18)
                throw new ArgumentException("CNPJ Inválido");

            bool contemFornecedor = db.FornecedorExistente(model.nm_fantasia);

            if (contemFornecedor == true)
               throw new ArgumentException("Fornecedor já registrado");

            db.NovoFornecedor(model);
        }

        public List<Database.Entity.tb_fornecedor> ListarFornecedores()
        {
            List<Database.Entity.tb_fornecedor> lista = db.ListarFornecedores();
            return lista;
        }

        public Database.Entity.tb_fornecedor ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe um id válido");

            Database.Entity.tb_fornecedor model = db.ConsultarPorId(id);
            return model;
        }

        public List<Database.Entity.tb_fornecedor> Consultar_Id(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe um id válido");

            List<Database.Entity.tb_fornecedor> lista = db.Consultar_Id(id);
            return lista;
        }

        public void AlterarFornecedor(Database.Entity.tb_fornecedor model)
        {
            if (string.IsNullOrWhiteSpace(model.ds_razao_social))
            {
                throw new ArgumentException("Informe a razão social");
            }

            if (string.IsNullOrWhiteSpace(model.nm_fantasia))
            {
                throw new ArgumentException("Informe o fornecedor");
            }

            if (string.IsNullOrWhiteSpace(model.ds_email))
            {
                throw new ArgumentException("Informe o e-mail");
            }

            if (string.IsNullOrWhiteSpace(model.ds_cnpj))
            {
                throw new ArgumentException("Informe o cnpj");
            }

            if (string.IsNullOrWhiteSpace(model.ds_endereco))
            {
                throw new ArgumentException("Informe o endereço");
            }

            if (string.IsNullOrWhiteSpace(model.ds_cep))
                throw new ArgumentException("Informe o cep");

            if (model.ds_telefone.Length < 15)
                throw new ArgumentException("Telefone Inválido");

            if (model.ds_cep.Length < 9)
                throw new ArgumentException("CEP Inválido");

            if (model.ds_cnpj.Length < 18)
                throw new ArgumentException("CNPJ Inválido");

            db.AlterarFornecedor(model);
        }

        public void RemoverFornecedor(Database.Entity.tb_fornecedor model)
        {
            if (string.IsNullOrWhiteSpace(model.nm_fantasia))
            {
                throw new ArgumentException("Informe o fornecedor");
            }

            db.RemoverFornecedor(model);
        }
    }
}
