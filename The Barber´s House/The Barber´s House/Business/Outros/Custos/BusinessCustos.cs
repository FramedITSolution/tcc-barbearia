﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Outros.Custos
{
    class BusinessCustos
    {
        Database.Outros.Custos.DatabaseCustos custos = new Database.Outros.Custos.DatabaseCustos();

        public void InserirCusto(Database.Entity.tb_custo custo)
        {
            if (custo.qt_compra == 0)
                throw new ArgumentException("Informe a quantidade");

            custos.InserirCusto(custo);
        }

        public void InserirCustoItem(Database.Entity.tb_custo_item custo)
        {
            if (custo.vl_unitario == 0)
                throw new ArgumentException("Informe o valor do produto");

            custos.InserirCustoItem(custo);
        }

        public Database.Entity.tb_custo_item ConsultarPorIDP(int id_produto_compra)
        {
            if (id_produto_compra == 0)
                throw new ArgumentException("Informe um ID");
            Database.Entity.tb_custo_item model = custos.ConsultarPorIDP(id_produto_compra);
            return model;
        }

        public Database.Entity.tb_custo_item ConsultarPorValorUnitário(decimal valor)
        {
            if (valor == 0)
                throw new ArgumentException("Informe o valor unitário");
            Database.Entity.tb_custo_item model = custos.ConsultarPorValorUnitário(valor);
            return model;
        }

        public Database.Entity.tb_custo ConsultarPorIDF(int id_fornecedor)
        {
            if (id_fornecedor == 0)
                throw new ArgumentException("Informe um ID");
            Database.Entity.tb_custo model = custos.ConsultarPorIDF(id_fornecedor);
            return model;
        }

        public Database.Entity.tb_custo ConsultarPorData(DateTime data)
        {         
            Database.Entity.tb_custo model = custos.ConsultarPorDatadeCompra(data);
            return model;
        }

        public void AlterarCustoItem(Database.Entity.tb_custo_item model)
        {
            if (model.vl_unitario == 0)
                throw new ArgumentException("Informe o valor unitário");
            custos.AlterarCustoItem(model);
        }

        public void AlterarCusto(Database.Entity.tb_custo model)
        {
            if (model.qt_compra == 0)
                throw new ArgumentException("Informe a quantidade");

            if (model.dt_compra < DateTime.Now)
                throw new ArgumentException("Informe uma data válida");

            custos.AlterarCusto(model);
        }

        public void RemoverCusto(Database.Entity.tb_custo model)
        {
            if (model.id_custo == 0)
                throw new ArgumentException("Informe um Id");

            custos.RemoverCusto(model);
        }

        public void RemoverCustoItem(Database.Entity.tb_custo_item model)
        {
            if (model.id_custo_item == 0)
                throw new ArgumentException("Informe um Id");

            custos.RemoverCustoItem(model);
        }
    }
}
