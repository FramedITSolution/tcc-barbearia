﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Usuário
{
    class BusinessLog
    {
        Database.Usuário.DatabaseLog logdb = new Database.Usuário.DatabaseLog();        

        public void NovoLog(Database.Entity.tb_log log)
        {
            logdb.NovoLog(log);
        }

        public void RemoverLog(int id)
        {
                logdb.RemoverLog(id);
        }
    }
}
