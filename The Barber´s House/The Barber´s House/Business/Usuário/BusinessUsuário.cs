﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Usuário
{
    class BusinessUsuário
    {
        Database.Usuário.DatabaseUsuário usuarioDatabase = new Database.Usuário.DatabaseUsuário();

        public bool EfetuarLogin(string usuario, string senha)
        {
            //Remove os espaços
            usuario = usuario.Trim();
            senha = senha.Trim();
            // Condições para Verificar se os campos não estão vazios
            if (string.IsNullOrWhiteSpace(usuario))
            {
                throw new ArgumentException("Por favor, informe um usuário!");
            }           
            if (string.IsNullOrWhiteSpace(senha))
            {
                throw new ArgumentException("Por favor, informe sua senha");
            }
            
            Objetos.AESCript cript = new Objetos.AESCript();
            senha = cript.Criptografar(senha);
            // Invocamento da função EfetuarLogin do Database
            bool contem = usuarioDatabase.EfetuarLogin(usuario, senha);

            if (contem == false)
                throw new ArgumentException("Usuário não encontrado");
            return true;
            
        }
        
        public void Cadastro(Database.Entity.tb_usuario usuario)
        {
            Objetos.AESCript cript = new Objetos.AESCript();
            usuario.nm_senha = cript.Criptografar(usuario.nm_senha);

            if (string.IsNullOrWhiteSpace(usuario.nm_usuario))
            {
                throw new ArgumentException("Informe o usuário");
            }

            if (string.IsNullOrWhiteSpace(usuario.nm_senha))
            {
                throw new ArgumentException("Informe uma senha");
            }

            if (usuario.id_funcionario == 0)
            {
                throw new ArgumentException("Informe o funcionário");
            }

            bool contemVerificarFuncionario = usuarioDatabase.VerificarFuncionario(usuario.id_funcionario);

            if (contemVerificarFuncionario == false)
                throw new ArgumentException("Funcionário não existente");

            bool contemUsuario = usuarioDatabase.UsuarioExistente(usuario.nm_usuario);

            if (contemUsuario == true)
                throw new ArgumentException("Usuário existente");

            FuncionarioPossuiLogin(usuario.id_funcionario);
            usuarioDatabase.Cadastro(usuario);
        }

        public void FuncionarioPossuiLogin(int idFuncionario)
        {
            if (idFuncionario == 0)
                throw new ArgumentException("Informe o usuário");

            bool contem = usuarioDatabase.FuncionarioPossuiLogin(idFuncionario);
            if (contem == true)
            {
                throw new ArgumentException("Esse funcionário já possui uma conta nesse sistema");
            }

        }

        public void ConsultarUsuario(string usuario, bool whatsapp)
        {
            if (string.IsNullOrWhiteSpace(usuario))
                throw new ArgumentException("Informe o usuário");

            Database.Entity.tb_usuario usu = usuarioDatabase.ConsultarPorUsuario(usuario);
            Objetos.CodigoAleatorio codigoAleatorio = new Objetos.CodigoAleatorio();
            Objetos.GmailSender gmailSender = new Objetos.GmailSender();

            string codigo = codigoAleatorio.GerarCodigo();

            string mensagem = "Olá, " + usu.tb_funcionario.nm_funcionario + ", seu código de alteração é: " + codigo;
            if (whatsapp == false)
            {
                gmailSender.EnviarSMS("+55" + usu.tb_funcionario.ds_telefone, mensagem);
                gmailSender.Enviar(usu.tb_funcionario.ds_email, "Código de recuperação", mensagem);
            }
            else
            {
                //if ("+55" + usu.tb_funcionario.ds_telefone != "+5511994465801")
                 //   throw new ArgumentException("É diferente");

                gmailSender.EnviarWhatsApp("+55" + usu.tb_funcionario.ds_telefone, mensagem);
            }

            usuarioDatabase.AdicionarCodigo(codigo, usuario);
        }

        public Database.Entity.tb_usuario ModeloUsuarioAtivo(string usuario)
        {
            return usuarioDatabase.ModeloUsuarioAtivo(usuario);
        }

        public List<Database.Entity.tb_usuario> ConsultarPorFuncionario(int funcionario)
        {
            List<Database.Entity.tb_usuario> lista = usuarioDatabase.ConsultarPorFuncionario(funcionario);
            return lista;
        }

        public Database.Entity.tb_usuario ConsultarPorUsuario(string nome)
        {
            Database.Entity.tb_usuario model = usuarioDatabase.ConsultarPorUsuario(nome);
            return model;
        }

        public void AlterarUsuario(Database.Entity.tb_usuario usuario)
        {
            if (string.IsNullOrWhiteSpace(usuario.nm_usuario))
            {
                throw new ArgumentException("Informe o usuário");
            }

            if (string.IsNullOrWhiteSpace(usuario.nm_senha))
            {
                throw new ArgumentException("Informe uma senha");
            }

            if (usuario.id_funcionario == 0)
            {
                throw new ArgumentException("Informe o funcionário");
            }

            bool contemUsuario = usuarioDatabase.UsuarioExistente(usuario.nm_usuario);

            if (contemUsuario == true)
                throw new ArgumentException("Usuário existente");

            usuarioDatabase.AlterarUsuario(usuario);
        }        

        public List<Database.Entity.tb_usuario> ListarUsuarios()
        {
            List<Database.Entity.tb_usuario> lista = usuarioDatabase.ListarUsuarios();
            return lista;
        }

        public List<Database.Entity.tb_usuario> ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe um id válido");

            List<Database.Entity.tb_usuario> lista = usuarioDatabase.ConsultarPorId(id);
            return lista;
        }

        public Database.Entity.tb_usuario ConsultarPorIDF(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o funcionário");
            bool contem = usuarioDatabase.FuncionarioPossuiLogin(id);
            if (contem == true)
            {
                return usuarioDatabase.ConsultarPorIDF(id);
            }
            return null;
        }

        public bool VerificarFuncionario(int funcionario)
        {
            //Verifica se não tem nada ou espaços em brancos
            if (funcionario == 0)
            {
                throw new ArgumentException("Informe o Funcionário");
            }

            bool contem = usuarioDatabase.VerificarFuncionario(funcionario);
            return contem;
        }

        public void AlterarSenha(string usuario, string senha, string codigo)
        {
            if (string.IsNullOrWhiteSpace(usuario))
            {
                throw new ArgumentException("Informe o usuário");
            }

            if (string.IsNullOrWhiteSpace(senha))
            {
                throw new ArgumentException("Informe a senha");
            }

            if (string.IsNullOrWhiteSpace(codigo))
            {
                throw new ArgumentException("Informe o código");
            }

            bool existeUsuario = usuarioDatabase.UsuarioExistente(usuario);
            bool codigoValido = usuarioDatabase.VerificarCodigo(usuario, codigo);

            if (existeUsuario == false)
                throw new ArgumentException("Usuário não encontrado");

            if (codigoValido == false)
                throw new ArgumentException("Código não válido");

            usuarioDatabase.AlterarSenha(usuario, senha, codigo);

        }

        public void RemoverUsuario(Database.Entity.tb_usuario usuario)
        {
            if (string.IsNullOrWhiteSpace(usuario.nm_usuario))
                throw new ArgumentException("Informe o usuário");
            usuarioDatabase.RemoverUsuario(usuario);
        }

        public void RemoverUsuarioPorIDF(int id)
        {
            if (id== 0)
                throw new ArgumentException("Informe o funcionário");
            bool contem = usuarioDatabase.FuncionarioPossuiLogin(id);
            if(contem == true)
            {
                usuarioDatabase.RemoverUsuarioPorID(id);
            }
        }
    }
}
