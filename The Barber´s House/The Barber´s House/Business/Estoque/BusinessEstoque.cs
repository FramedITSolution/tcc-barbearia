﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Estoque
{
    class BusinessEstoque
    {
        Database.Estoque.DatabaseEstoque db = new Database.Estoque.DatabaseEstoque();

        public void InserirNoEstoque(Database.Entity.tb_estoque estoque)
        {
            if (string.IsNullOrWhiteSpace(estoque.ds_detalhe))
                throw new ArgumentException("Informe um detalhe do produto");

            if (string.IsNullOrWhiteSpace(estoque.ds_situacao_produto))
                throw new ArgumentException("Informe a situação do produto");

            if (estoque.id_produto_compra == 0)
                throw new ArgumentException("Informe o produto");

            if (estoque.dt_validade_prevista < DateTime.Now)
                throw new ArgumentException("Informe uma data válida");

            if (estoque.qt_estoque <= 4)
            {
                estoque.bt_abastecer = true;
                estoque.bt_urgente = true;
            }

            db.InserirNoEstoque(estoque);
        }
        
        public List<Database.Entity.tb_estoque> ListaEstoque()
        {
            return db.ListarEstoque();
        }

        public List<Database.Entity.tb_estoque> Consultar_ID(int id_produto_compra)
        {
            if (id_produto_compra == 0)
                throw new ArgumentException("Informe um ID");
            List<Database.Entity.tb_estoque> lista = db.Consultar_ID(id_produto_compra);
            return lista;
        }

        public Database.Entity.tb_estoque ConsultarPorIDP(int id_produto_compra)
        {
            if (id_produto_compra == 0)
                throw new ArgumentException("Informe um ID");
            Database.Entity.tb_estoque model = db.ConsultarPorIDP(id_produto_compra);
            return model;
        }

        public void AlterarEstoque(Database.Entity.tb_estoque estoque)
        {
            if(string.IsNullOrWhiteSpace(estoque.ds_situacao_produto))
            {
                throw new ArgumentException("Informe a situação do produto");
            }

            if (string.IsNullOrWhiteSpace(estoque.ds_detalhe))
            {
                throw new ArgumentException("Informe o detalhe do produto");
            }

            if (estoque.qt_estoque <= 4)
            {
                estoque.bt_abastecer = true;
                estoque.bt_urgente = true;
            }

            db.AlterarEstoque(estoque); 
        }

        public void RemoverDoEstoque(Database.Entity.tb_estoque estoque)
        {
            if (estoque.id_estoque == 0)
                throw new ArgumentException("Informe um Id");

            db.RemoverDoEstoque(estoque);
        }
    }
}
