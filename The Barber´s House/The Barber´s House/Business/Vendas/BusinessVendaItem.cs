﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Vendas
{
    class BusinessVendaItem
    {
        Database.Vendas.DatabaseVendaItem VendaItem = new Database.Vendas.DatabaseVendaItem();

        public void InserirVendaItem(Database.Entity.tb_venda_item venda)
        {
            VendaItem.InserirVendaItem(venda);
        }

        public List<Database.Entity.tb_venda_item> ListarVendas()
        {
            return VendaItem.ConsultarTodos();
        }

        public void AlterarVenda(Database.Entity.tb_venda_item vendas)
        {            
            VendaItem.AlterarVenda(vendas);
        }

        public void RemoverVenda(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o Id da venda");

            VendaItem.RemoverVenda(id);
        }

        public void RemoverVendaPorPV(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o produto");

            VendaItem.RemoverVendaPorPV(id);
        }

        public void RemoverVendaPorFunc(int id)
        {
            VendaItem.RemoverVendaPorFunc(id);
        }
    }
}
