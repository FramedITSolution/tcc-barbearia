﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Vendas
{
    class BusinessVendasPorCliente
    {
        Database.Vendas.DatabaseVendaPorCliente VendaPorCliente = new Database.Vendas.DatabaseVendaPorCliente();

        public void InserirVendaPorCliente(Database.Entity.tb_venda_por_cliente venda)
        {
            if (string.IsNullOrWhiteSpace(venda.ds_pagamento))
                throw new ArgumentException("Informe o modo de pagamento");

            if (string.IsNullOrWhiteSpace(venda.nm_responsavel_venda) || venda.id_funcionario == 0)
                throw new ArgumentException("Informe o responsável pela venda");

            if (venda.id_cliente == 0)
                throw new ArgumentException("Informe o cliente");

            VendaPorCliente.InserirVendaPorCliente(venda);
        }

        public List<Database.Entity.tb_venda_por_cliente> ListarVendas()
        {
            return VendaPorCliente.ConsultarTodos();
        }

        public List<Database.Entity.tb_venda_por_cliente> ConsultarVendaPorData(DateTime data)
        {
            return VendaPorCliente.ConsultarVenda(data);
        }

        public Database.Entity.tb_venda_por_cliente ConsultarVendaPorFunc(int id)
        {
            bool contem = VendaPorCliente.FuncionarioPossuiVenda(id);
            while(contem == true)
            {
                return VendaPorCliente.ConsultarVendaPorFunc(id);
            }
            return null;
        }

        public void AlterarVenda(Database.Entity.tb_venda_por_cliente vendas)
        {
            if (string.IsNullOrWhiteSpace(vendas.ds_pagamento))
                throw new ArgumentException("Informe o modo de pagamento");

            if (string.IsNullOrWhiteSpace(vendas.nm_responsavel_venda) || vendas.id_funcionario == 0)
                throw new ArgumentException("Informe o responsável pela venda");

            if (vendas.id_cliente == 0)
                throw new ArgumentException("Informe o cliente");

            VendaPorCliente.AlterarVenda(vendas);
        }

        public void RemoverVenda(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o Id da venda");

            VendaPorCliente.RemoverVenda(id);
        }

        public void RemoverVendaPorCliente(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o cliente");

            VendaPorCliente.RemoverVendaPorCliente(id);
        }

        public void RemoverVendaPorFunc(int id)
        {
                VendaPorCliente.RemoverVendaPorFunc(id);
        }
    }
}
