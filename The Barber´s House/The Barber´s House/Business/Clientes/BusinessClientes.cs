﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Clientes
{
    class BusinessClientes
    {
        Database.Clientes.DatabaseClientes db = new Database.Clientes.DatabaseClientes();

        public void NovoCliente(Database.Entity.tb_cliente model)
        {
            model.nm_cliente.Trim();
            model.ds_email.Trim();
            model.ds_endereco.Trim();
            if (string.IsNullOrWhiteSpace(model.nm_cliente))
            {
                throw new ArgumentException("Informe o cliente");
            }

            if (string.IsNullOrWhiteSpace(model.ds_endereco))
            {
                throw new ArgumentException("Informe o endereço");
            }

            if (string.IsNullOrWhiteSpace(model.ds_telefone))
            {
                throw new ArgumentException("Informe o telefone");
            }

            bool contemCliente = db.VerificarCliente(model.nm_cliente);

            if (contemCliente == true)
                throw new ArgumentException("Nome do cliente já registrado, digite outro");

            db.NovoCliente(model);
        }

        public List<Database.Entity.tb_cliente> ListarClientes()
        {
            List<Database.Entity.tb_cliente> lista = db.ListarClientes();
            return lista;
        }

        public Database.Entity.tb_cliente ConsultarPorNome(string nome)
        {
            if (string.IsNullOrWhiteSpace(nome))
                throw new ArgumentException("Informe o nome do cliente");
            return db.ConsultarPorNome(nome);
        }

        public Database.Entity.tb_cliente ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe id do cliente");
            return db.ConsultarPorID(id);
        }

        public List<Database.Entity.tb_cliente> Consultar_Id(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe id do cliente");
            return db.Consultar_ID(id);
        }

        public void AlterarCliente(Database.Entity.tb_cliente model)
        {
            model.nm_cliente.Trim();
            model.ds_email.Trim();
            model.ds_endereco.Trim();
            if (string.IsNullOrWhiteSpace(model.nm_cliente))
            {
                throw new ArgumentException("Informe o cliente");
            }

            if (string.IsNullOrWhiteSpace(model.ds_endereco))
            {
                throw new ArgumentException("Informe o endereço");
            }

            if (string.IsNullOrWhiteSpace(model.ds_telefone))
            {
                throw new ArgumentException("Informe o telefone");
            }

            db.AlterarCliente(model);
        }

        public void RemoverCliente(Database.Entity.tb_cliente model)
        {
            if (string.IsNullOrWhiteSpace(model.nm_cliente))
            {
                throw new ArgumentException("Informe o cliente");
            }

            db.RemoverCliente(model);
        }
    }
}
