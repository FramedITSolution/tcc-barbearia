﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Financeiro
{
    class BusinessFinanceiro
    {
        Database.Financeiro.DatabaseFinanceiro financeiro = new Database.Financeiro.DatabaseFinanceiro();

        public List<Database.Entity.vw_fluxo_de_caixa> ConsultarViewFluxoDeCaixa(DateTime dt_inicio, DateTime dt_final)
        {
            List<Database.Entity.vw_fluxo_de_caixa> lista =
                financeiro.ConsultarViewFluxoDeCaixa(dt_inicio, dt_final);
            return lista;
        }

        public List<Database.Entity.vw_fluxo_de_caixa> ConsultarTodos()
        {
            return financeiro.ConsultarTodos();
        }

        public List<Database.Entity.tb_fluxo_caixa> ListarDespesas()
        {
            return financeiro.ListarDespesas();
        }

        public List<Database.Entity.tb_fluxo_caixa> ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o id da despesa para consultar");

            return financeiro.ConsultarPorId(id);
        }

        public void InserirDespesa(Database.Entity.tb_fluxo_caixa fluxo)
        {
            if (string.IsNullOrWhiteSpace(fluxo.nm_referencia))
            {
                throw new ArgumentException("Informe o nome de referência");
            }

            if (fluxo.ds_previsto == 0)
            {
                throw new ArgumentException("Informe o valor previsto");
            }

            if (fluxo.ds_realizado > fluxo.ds_previsto)
                throw new ArgumentException("Valor realizado inválido");

            financeiro.InserirDespesa(fluxo);
        }

        public void AlterarDespesa(Database.Entity.tb_fluxo_caixa fluxo)
        {
            if (string.IsNullOrWhiteSpace(fluxo.nm_referencia))
            {
                throw new ArgumentException("Informe o nome de referência");
            }

            if (fluxo.ds_previsto == 0)
            {
                throw new ArgumentException("Informe o valor previsto");
            }

            if (fluxo.ds_realizado > fluxo.ds_previsto)
                throw new ArgumentException("Valor realizado inválido");

            financeiro.AlterarDespesa(fluxo);
        }

        public void RemoverDespesa(Database.Entity.tb_fluxo_caixa fluxo)
        {            
            if (fluxo.id_fluxo_caixa == 0)
            {
                throw new ArgumentException("Informe o id");
            }

            financeiro.RemoverDespesa(fluxo);
        }
    }
}
