﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Produtos
{
    class BusinessProdutos_de_Compras
    {

        Database.Produtos.DatabaseProdutos_de_Compra db = new Database.Produtos.DatabaseProdutos_de_Compra();

        public void NovoProduto(Database.Entity.tb_produto_compra produto)
        {
            if (string.IsNullOrWhiteSpace(produto.nm_produto))
            {
                throw new ArgumentException("Informe o produto");
            }

            if (produto.vl_preco == 0)
                throw new ArgumentException("Informe o preço do produto");

            bool contemProduto = db.ProdutoExistente(produto.nm_produto);

            if (contemProduto == true)
                throw new ArgumentException("Nome do produto já registrado, digite outro");

            db.NovoProduto(produto);
        }

        public List<Database.Entity.tb_produto_compra> ConsultarPorID(int id)
        {
            List<Database.Entity.tb_produto_compra> lista = db.ConsultarPorId(id);
            return lista;
        }

        public Database.Entity.tb_produto_compra ConsultarPorNome(string nome)
        {
            Database.Entity.tb_produto_compra model = db.ConsultarPorNome(nome);
            return model;
        }

        public List<Database.Entity.tb_produto_compra> ListarProdutos()
        {
            List<Database.Entity.tb_produto_compra> lista = db.ListarProdutos();
            return lista;
        }

        public void AlterarProduto(Database.Entity.tb_produto_compra produto)
        {
            if (string.IsNullOrWhiteSpace(produto.nm_produto))
            {
                throw new ArgumentException("Informe o produto");
            }

            if (produto.vl_preco == 0)
                throw new ArgumentException("Informe o preço do produto");          

            db.AlterarProduto(produto);
        }

        public void RemoverProduto(Database.Entity.tb_produto_compra produto)
        {
            if (string.IsNullOrWhiteSpace(produto.nm_produto))
            {
                throw new ArgumentException("Informe o produto");
            }
            db.RemoverProduto(produto);
        }
    }
}
