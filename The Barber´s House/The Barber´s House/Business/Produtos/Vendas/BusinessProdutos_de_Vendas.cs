﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Produtos.Vendas
{
    class BusinessProdutos_de_Vendas
    {
        Database.Produtos.Vendas.DatabaseProdutos_de_Vendas db = new Database.Produtos.Vendas.DatabaseProdutos_de_Vendas();

        public void NovoProduto(Database.Entity.tb_produto_venda produto)
        {
            if (string.IsNullOrWhiteSpace(produto.nm_produto))
            {
                throw new ArgumentException("Informe o produto");
            }

            if (produto.vl_preco == 0)
                throw new ArgumentException("Informe o preço do produto");

            bool contemProduto = db.ProdutoExistente(produto.nm_produto);

            if (contemProduto == true)
                throw new ArgumentException("Nome do produto já registrado, digite outro");

            db.NovoProduto(produto);
        }

        public List<Database.Entity.tb_produto_venda> ConsultarPorID(int id)
        {
            List<Database.Entity.tb_produto_venda> lista = db.ConsultarPorId(id);
            return lista;
        }

        public List<Database.Entity.tb_produto_venda> ListarProdutos()
        {
            List<Database.Entity.tb_produto_venda> lista = db.ListarProdutos();
            return lista;
        }

        public void AlterarProduto(Database.Entity.tb_produto_venda produto)
        {
            if (string.IsNullOrWhiteSpace(produto.nm_produto))
            {
                throw new ArgumentException("Informe o produto");
            }

            if (produto.vl_preco == 0)
                throw new ArgumentException("Informe o preço do produto");            

            db.AlterarProduto(produto);
        }

        public void RemoverProduto(Database.Entity.tb_produto_venda produto)
        {
            if (produto.id_produtos == 0)
            {
                throw new ArgumentException("Informe o id do produto");
            }
            db.RemoverProduto(produto);
        }
    }
}
