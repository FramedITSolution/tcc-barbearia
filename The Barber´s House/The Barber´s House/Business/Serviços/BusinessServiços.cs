﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Serviços
{
    class BusinessServiços
    {
        Database.Serviços.DatabaseServiços db = new Database.Serviços.DatabaseServiços();
        Objetos.UsoGeral validar = new Objetos.UsoGeral();

        public void NovoAtendimento(Database.Entity.tb_servico model)
        {

            if (string.IsNullOrWhiteSpace(model.nm_responsavel_servico))
            {
                throw new ArgumentException("Informe o responsável");
            }

            if (model.vl_servico == 0 )
            {
                throw new ArgumentException("Informe o valor do serviço");
            }

            if (model.hr_servico == TimeSpan.Zero)
                throw new ArgumentException("Horário inválido, digite outro");

            if (model.hr_servico >= TimeSpan.MaxValue)
                throw new OverflowException("Horário inválido, digite outro");            

            bool contemServiço = db.ServiçoExistente(model.hr_servico, model.dt_servico);

            if (contemServiço == true)
                throw new ArgumentException("Horário agendado, digite outro");

            db.NovoAtendimento(model);
        }

        public List<Database.Entity.tb_servico> ListarServiços()
        {
            List<Database.Entity.tb_servico> lista = db.ListarServiços();
            return lista;
        }

        public Database.Entity.tb_servico ConsultarPorId(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe um id válido");

            Database.Entity.tb_servico model = db.ConsultarPorID(id);
            return model;
        }

        public List<Database.Entity.tb_servico> Consultar_Id(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe um id válido");
            return db.Consultar_ID(id);
        }

        public Database.Entity.tb_servico ConsultarPorCorte(int id)
        {
            if (id == 0)
                throw new ArgumentException("Informe o corte");
            return db.ConsultarPorCorte(id);
        }

        public void AlterarServiço(Database.Entity.tb_servico model)
        {

            if (string.IsNullOrWhiteSpace(model.nm_responsavel_servico))
            {
                throw new ArgumentException("Informe o responsável");
            }

            if (model.vl_servico == 0)
            {
                throw new ArgumentException("Informe o valor do serviço");
            }

            bool contemServiço = db.ServiçoExistente(model.hr_servico, model.dt_servico);

            if (contemServiço == true)
               throw new ArgumentException("Horário agendado, digite outro");

            db.AlterarServiço(model);
        }

        public void RemoverServiço(Database.Entity.tb_servico model)
        {
            if (model.id_servico == 0)
            {
                throw new ArgumentException("Informe um serviço");
            }               
            db.RemoverServiço(model);
        }

        public void RemoverServiçoPorID(int id)
        {
            db.RemoverServiçoPorID(id);
        }

        public bool VerificarHorário(string horario)
        {
            if (string.IsNullOrWhiteSpace(horario))
                throw new ArgumentException("Diga o horário");

            if (horario.Length < 5)
                throw new ArgumentException("Preencha todo o horário");

            return validar.VerificarHorário(horario);
        }
    }
}
