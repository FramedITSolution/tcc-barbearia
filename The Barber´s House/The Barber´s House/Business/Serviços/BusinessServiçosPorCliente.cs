﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Business.Serviços
{
    class BusinessServiçosPorCliente
    {
        Database.Serviços.DatabaseServiçosPorCliente db = new Database.Serviços.DatabaseServiçosPorCliente();

        public Database.Entity.tb_servicos_por_cliente ConsultarServiçoPorFunc(int id)
        {
            bool contem = db.FuncRealizouServiço(id);
            while (contem == true)
            {
                return db.ConsultarServiçoPorFunc(id);
            }
            return null;
        }


        public void RemoverServiçoPorCliente(Database.Entity.tb_servicos_por_cliente model)
        {
            if (model.id_servico == 0)
            {
                throw new ArgumentException("Informe um serviço");
            }
            db.RemoverServiçoPorCliente(model);
        }

        public void RemoverServiçoPorClientePorID(int id)
        {
            db.RemoverServiçoPorClientePorID(id);
        }

        public void RemoverServiçoPorFunc(int id)
        {
            db.RemoverServiçoPorFunc(id);
        }
    }
}
