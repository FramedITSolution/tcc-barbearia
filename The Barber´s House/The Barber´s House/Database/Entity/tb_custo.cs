//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace The_Barber_s_House.Database.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_custo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tb_custo()
        {
            this.tb_custo_item = new HashSet<tb_custo_item>();
        }
    
        public int id_custo { get; set; }
        public int qt_compra { get; set; }
        public decimal vl_total { get; set; }
        public System.DateTime dt_compra { get; set; }
        public System.DateTime dt_previsao_de_entrega { get; set; }
        public int id_fornecedor { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_custo_item> tb_custo_item { get; set; }
        public virtual tb_fornecedor tb_fornecedor { get; set; }
    }
}
