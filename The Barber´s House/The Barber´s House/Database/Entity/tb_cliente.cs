//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace The_Barber_s_House.Database.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tb_cliente()
        {
            this.tb_servico = new HashSet<tb_servico>();
            this.tb_venda_por_cliente = new HashSet<tb_venda_por_cliente>();
        }
    
        public int id_cliente { get; set; }
        public string nm_cliente { get; set; }
        public string ds_telefone { get; set; }
        public string ds_endereco { get; set; }
        public Nullable<System.DateTime> dt_primeira_compra { get; set; }
        public bool bt_ativo { get; set; }
        public bool bt_fidelizado { get; set; }
        public string ds_email { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_servico> tb_servico { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_venda_por_cliente> tb_venda_por_cliente { get; set; }
    }
}
