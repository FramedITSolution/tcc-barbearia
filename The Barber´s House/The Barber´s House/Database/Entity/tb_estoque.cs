//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace The_Barber_s_House.Database.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_estoque
    {
        public int id_estoque { get; set; }
        public int qt_estoque { get; set; }
        public string ds_situacao_produto { get; set; }
        public string ds_detalhe { get; set; }
        public bool bt_abastecer { get; set; }
        public bool bt_urgente { get; set; }
        public decimal vl_total { get; set; }
        public System.DateTime dt_validade_prevista { get; set; }
        public int id_produto_compra { get; set; }
    
        public virtual tb_produto_compra tb_produto_compra { get; set; }
    }
}
