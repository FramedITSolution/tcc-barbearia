﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Vendas
{
    class DatabaseVendaPorCliente
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirVendaPorCliente(Entity.tb_venda_por_cliente venda)
        {
            db.tb_venda_por_cliente.Add(venda);
            db.SaveChanges();
        }

        public List<Entity.tb_venda_por_cliente> ConsultarTodos()
        {
            return db.tb_venda_por_cliente.ToList();
        }

        public List<Entity.tb_venda_por_cliente> ConsultarVenda(DateTime dataDeVenda)
        {
            return db.tb_venda_por_cliente.Where(x => x.dt_venda == dataDeVenda).ToList();
        }

        public Entity.tb_venda_por_cliente ConsultarVendaPorFunc(int id)
        {
            return db.tb_venda_por_cliente.First(x => x.id_funcionario == id);
        }

        public void AlterarVenda(Entity.tb_venda_por_cliente venda)
        {
            Entity.tb_venda_por_cliente alterar = db.tb_venda_por_cliente.First(x => x.id_venda_por_cliente == venda.id_venda_por_cliente);
            alterar.ds_pagamento = venda.ds_pagamento;
            alterar.dt_venda = venda.dt_venda;
            alterar.nm_responsavel_venda = venda.nm_responsavel_venda;
            alterar.qt_parcelas = venda.qt_parcelas;
            alterar.id_cliente = venda.id_cliente;
            alterar.id_funcionario = venda.id_funcionario;

            db.SaveChanges();
        }

        public void RemoverVenda(int id)
        {
            Entity.tb_venda_por_cliente remover = db.tb_venda_por_cliente.First(x => x.id_venda_por_cliente == id);
            db.tb_venda_por_cliente.Remove(remover);
            db.SaveChanges();
        }

        public bool ClientePossuiVenda(int id)
        {
            bool contem = db.tb_venda_por_cliente.Any(x => x.id_cliente == id);
            return contem;
        }

        public void RemoverVendaPorCliente(int id)
        {
            bool contem = ClientePossuiVenda(id);
            while(contem == true)
            {
                Entity.tb_venda_por_cliente remover = db.tb_venda_por_cliente.First(x => x.id_cliente == id);
                db.tb_venda_por_cliente.Remove(remover);
                db.SaveChanges();
                contem = ClientePossuiVenda(id);
            }            
        }

        public bool FuncionarioPossuiVenda(int id)
        {
            bool contem = db.tb_venda_por_cliente.Any(x => x.id_funcionario == id);
            return contem;
        }

        public void RemoverVendaPorFunc(int id)
        {
            bool contem = FuncionarioPossuiVenda(id);
            while(contem == true)
            {
                Entity.tb_venda_por_cliente remover = db.tb_venda_por_cliente.First(x => x.id_funcionario == id);
                db.tb_venda_por_cliente.Remove(remover);
                db.SaveChanges();
                contem = FuncionarioPossuiVenda(id);
            }
        }
    }
}
