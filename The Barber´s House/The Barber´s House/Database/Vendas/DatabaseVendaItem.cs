﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Vendas
{
    class DatabaseVendaItem
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirVendaItem(Entity.tb_venda_item venda)
        {
            db.tb_venda_item.Add(venda);
            db.SaveChanges();
        }

        public List<Entity.tb_venda_item> ConsultarTodos()
        {
            return db.tb_venda_item.ToList();
        }

        public void AlterarVenda(Entity.tb_venda_item venda)
        {
            Entity.tb_venda_item alterar = db.tb_venda_item.First(x => x.id_venda_item == venda.id_venda_item);
            alterar.qt_produto = venda.qt_produto;
            alterar.vl_total = venda.vl_total;
            alterar.id_produto_venda = venda.id_produto_venda;
            alterar.id_venda_por_cliente = venda.id_venda_por_cliente;

            db.SaveChanges();
        }


        public void RemoverVenda(int id)
        {
            Entity.tb_venda_item remover = db.tb_venda_item.First(x => x.id_venda_item == id);
            db.tb_venda_item.Remove(remover);
            db.SaveChanges();
        }

        public bool VendaPorProdutoExiste(int id)
        {
            bool contem = db.tb_venda_item.Any(x => x.id_produto_venda == id);
            return contem;
        }

        public void RemoverVendaPorPV(int id)
        {
            bool contem = VendaPorProdutoExiste(id);
            while(contem == true)
            {
                Entity.tb_venda_item remover = db.tb_venda_item.First(x => x.id_produto_venda == id);
                db.tb_venda_item.Remove(remover);
                db.SaveChanges();
                contem = VendaPorProdutoExiste(id);
            }         
        }

        public bool VendaPorClienteExiste(int id)
        {
            bool contem = db.tb_venda_item.Any(x => x.id_venda_por_cliente == id);
            return contem;
        }

        public void RemoverVendaPorFunc(int id)
        {
            bool contem = VendaPorClienteExiste(id);
            while(contem == true)
            {
                Entity.tb_venda_item remover = db.tb_venda_item.First(x => x.id_venda_por_cliente == id);
                db.tb_venda_item.Remove(remover);
                db.SaveChanges();
                contem = VendaPorClienteExiste(id);
            }          
        }
    }
}
