﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Serviços
{
    class DatabaseServiçosPorCliente
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public Entity.tb_servicos_por_cliente ConsultarServiçoPorFunc(int id)
        {
            return db.tb_servicos_por_cliente.First(x => x.id_funcionario == id);
        }

        public void RemoverServiçoPorCliente(Entity.tb_servicos_por_cliente model)
        {
            Entity.tb_servicos_por_cliente remover = db.tb_servicos_por_cliente.First(x => x.id_servico == model.id_servico);
            db.tb_servicos_por_cliente.Remove(remover);
            db.SaveChanges();
        }

        public bool PossuiServiçoPorCliente(int id)
        {
            bool contem = db.tb_servicos_por_cliente.Any(x => x.id_servico == id);
            return contem;
        }

        public void RemoverServiçoPorClientePorID(int id)
        {
            bool contem = PossuiServiçoPorCliente(id);
            while (contem == true)
            {
                Entity.tb_servicos_por_cliente remover = db.tb_servicos_por_cliente.First(x => x.id_servico == id);
                db.tb_servicos_por_cliente.Remove(remover);
                db.SaveChanges();
                contem = PossuiServiçoPorCliente(id);
            }               
        }

        public bool FuncRealizouServiço(int id)
        {
            bool contem = db.tb_servicos_por_cliente.Any(x => x.id_funcionario == id);
            return contem;
        }

        public void RemoverServiçoPorFunc(int id)
        {
            bool contem = FuncRealizouServiço(id);
            while (contem == true)
            {
                Entity.tb_servicos_por_cliente remover = db.tb_servicos_por_cliente.First(x => x.id_funcionario == id);
                db.tb_servicos_por_cliente.Remove(remover);
                db.SaveChanges();
                contem = FuncRealizouServiço(id);
            }
        }
    }
}
