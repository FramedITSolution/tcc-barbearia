﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Financeiro
{
    class DatabaseFinanceiro
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public List<Entity.vw_fluxo_de_caixa> ConsultarViewFluxoDeCaixa(DateTime data_inicio, DateTime data_final)
        {
            List<Entity.vw_fluxo_de_caixa> lista = db.vw_fluxo_de_caixa.
                                                                     Where(x => x.dt_pagamento >= data_inicio
                                                                             && x.dt_pagamento <= data_final).
                                                                     ToList();
            return lista;
        }

        public List<Entity.vw_fluxo_de_caixa> ConsultarTodos()
        {
            return db.vw_fluxo_de_caixa.ToList();
        }

        public List<Entity.tb_fluxo_caixa> ListarDespesas()
        {
            return db.tb_fluxo_caixa.ToList();
        }

        public List<Entity.tb_fluxo_caixa> ConsultarPorId(int id)
        {
            return db.tb_fluxo_caixa.Where(x => x.id_fluxo_caixa == id).ToList();
        }

        public void InserirDespesa(Entity.tb_fluxo_caixa fluxo)
        {
            db.tb_fluxo_caixa.Add(fluxo);
            db.SaveChanges();
        }

        public void AlterarDespesa(Entity.tb_fluxo_caixa fluxo)
        {
            Entity.tb_fluxo_caixa alterar = db.tb_fluxo_caixa.First(x => x.id_fluxo_caixa == fluxo.id_fluxo_caixa);
            alterar.nm_referencia = fluxo.nm_referencia;
            alterar.ds_previsto = fluxo.ds_previsto;
            alterar.ds_realizado = fluxo.ds_realizado;
            alterar.dt_entrada = fluxo.dt_entrada;
            alterar.dt_saida = fluxo.dt_saida;
            db.SaveChanges();
        }

        public void RemoverDespesa(Entity.tb_fluxo_caixa fluxo)
        {
            Entity.tb_fluxo_caixa fluxocaixa = db.tb_fluxo_caixa.First(x=> x.id_fluxo_caixa == fluxo.id_fluxo_caixa);
            db.tb_fluxo_caixa.Remove(fluxocaixa);
            db.SaveChanges();
        }
    }
}
