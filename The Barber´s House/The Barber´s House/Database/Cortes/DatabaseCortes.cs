﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Cortes
{
    class DatabaseCortes
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovoCorte(Entity.tb_corte corte)
        {
            db.tb_corte.Add(corte);
            db.SaveChanges();
        }
        public List<Entity.tb_corte> ConsultarCorte(int id)
        {
            List<Entity.tb_corte> lista = db.tb_corte.Where(x => x.id_corte == id).ToList();
            return lista;
        }

        public List<Entity.tb_corte> ListarCortes()
        {
            List<Entity.tb_corte> lista = db.tb_corte.ToList();
            return lista;
        }

        public bool VerificarCorte(string corte)
        {
            bool contem = db.tb_corte
                                           .Any(x => x.nm_corte == corte);
            return contem;
        }

        public void AlterarCorte(Entity.tb_corte corte)
        {
            Entity.tb_corte alterar = db.tb_corte.First(x => x.id_corte == corte.id_corte);
            alterar.nm_corte = corte.nm_corte;
            alterar.vl_corte = corte.vl_corte;
            db.SaveChanges();
        }

        public void RemoverCorte(Entity.tb_corte corte)
        {
            Entity.tb_corte remover = db.tb_corte.First(x => x.id_corte == corte.id_corte);
            db.tb_corte.Remove(remover);
            db.SaveChanges();
        }

        public bool SerrviçoPossuiCorte(int corte)
        {
            //Verifica se esse usuário possui log
            bool contem = db.tb_servico
                                           .Any(x => x.id_corte == corte);

            return contem;
        }

        public void RemoverServiçoPorCorte(int id)
        {
            bool contem = SerrviçoPossuiCorte(id);
            while (contem == true)
            {
                Entity.tb_servico remover = db.tb_servico.First(x => x.id_corte == id);
                db.tb_servico.Remove(remover);
                db.SaveChanges();
                contem = SerrviçoPossuiCorte(id);
            }
        }       
    }
}
