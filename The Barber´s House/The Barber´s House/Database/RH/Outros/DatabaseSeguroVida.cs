﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Outros
{
    class DatabaseSeguroVida
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public Entity.tb_seguro_vida ConsultarPlano(decimal plano)
        {
            return db.tb_seguro_vida.First(x => x.vl_plano == plano);
        }

        public List<Entity.tb_seguro_vida> ListarTodos()
        {
            return db.tb_seguro_vida.ToList();
        }
    }
}
