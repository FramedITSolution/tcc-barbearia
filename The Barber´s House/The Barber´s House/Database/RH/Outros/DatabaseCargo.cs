﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Outros
{
    class DatabaseCargo
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirCargo(Entity.tb_cargo cargo)
        {
            db.tb_cargo.Add(cargo);
            db.SaveChanges();
        }

        public List<Entity.tb_cargo> ConsultarTodos()
        {
            List<Entity.tb_cargo> lista = db.tb_cargo.ToList();
            return lista;
        }

        public List<Entity.tb_cargo> Consultarcargo(string cargo)
        {
            List<Entity.tb_cargo> lista = db.tb_cargo.
                                                               Where(x => x.ds_cargo == cargo).
                                                              ToList();
            return lista;
        }

        public void AlterarCodigo(Entity.tb_cargo cargo)
        {
            Entity.tb_cargo alterar = db.tb_cargo.
                                                           First(x => x.id_cargo == cargo.id_cargo);
            alterar.ds_cargo = cargo.ds_cargo;
            alterar.ds_codigo_cargo = cargo.ds_codigo_cargo;

            db.SaveChanges();
        }

        public void RemoverCargo(int id)
        {
            Entity.tb_cargo remover = db.tb_cargo.
                                                           First(x => x.id_cargo == id);
            db.tb_cargo.
                                Remove(remover);

            db.SaveChanges();
        }

        public List<Entity.tb_funcionario> ConsultarCargoEFuncionario(string funcionario, string cargo)
        {
            return db.tb_funcionario.
                                              Where(x => x.nm_funcionario.
                                           Contains(funcionario) && x.tb_cargo.ds_cargo == cargo).ToList();
        }
    }
}
