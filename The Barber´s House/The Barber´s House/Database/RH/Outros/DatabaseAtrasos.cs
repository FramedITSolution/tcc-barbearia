﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Outros
{
    class DatabaseAtrasos
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovoAtraso(Entity.tb_atrasos atraso)
        {
            db.tb_atrasos.Add(atraso);
            db.SaveChanges();
        }

        public List<Entity.tb_atrasos> ConsultarAtrasosPorFunc(int id)
        {
            return db.tb_atrasos.Where(x => x.id_funcionario == id).ToList();
        }

        public List<Entity.tb_atrasos> ListarAtrasos()
        {
            return db.tb_atrasos.ToList();
        }

        public void AlterarAtraso(Entity.tb_atrasos atraso)
        {
            Entity.tb_atrasos alterar = db.tb_atrasos.First(x => x.id_atraso == atraso.id_atraso);
            alterar.dt_atraso = atraso.dt_atraso;
            alterar.ds_tempo_de_atraso = atraso.ds_tempo_de_atraso;
            alterar.id_funcionario = atraso.id_funcionario;
            db.SaveChanges();
        }

        public bool FuncionarioPossuiAtraso(int func)
        {
            //Verifica se esse usuário possui log
            bool contem = db.tb_atrasos
                                           .Any(x => x.id_funcionario == func);

            return contem;
        }

        public void RemoverAtrasoPorIDF(int id)
        {
            bool contem = FuncionarioPossuiAtraso(id);
            while(contem == true)
            {
                Entity.tb_atrasos remover = db.tb_atrasos.First(x => x.id_funcionario == id);
                db.tb_atrasos.Remove(remover);
                db.SaveChanges();
                contem = FuncionarioPossuiAtraso(id);
            }           
        }
    }
}
