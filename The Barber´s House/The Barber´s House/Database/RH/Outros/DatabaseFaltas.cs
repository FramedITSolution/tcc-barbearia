﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Outros
{
    class DatabaseFaltas
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovaFalta(Entity.tb_faltas falta)
        {
            db.tb_faltas.Add(falta);
            db.SaveChanges();
        }

        public List<Entity.tb_faltas> ConsultarFaltasPorFunc(int id)
        {
            return db.tb_faltas.Where(x => x.id_funcionario == id).ToList();
        }

        public List<Entity.tb_faltas> ListarFaltas()
        {
            return db.tb_faltas.ToList();
        }

        public void AlterarFalta(Entity.tb_faltas falta)
        {
            Entity.tb_faltas alterar = db.tb_faltas.First(x => x.id_faltas == falta.id_faltas);
            alterar.dt_falta = falta.dt_falta;
            alterar.id_funcionario = falta.id_funcionario;
            db.SaveChanges();
        }

        public bool FuncionarioPossuiFalta(int func)
        {
            //Verifica se esse usuário possui log
            bool contem = db.tb_faltas
                                           .Any(x => x.id_funcionario == func);

            return contem;
        }

        public void RemoverFaltaPorIDF(int id)
        {
            bool contem = FuncionarioPossuiFalta(id);
            while(contem == true)
            {
                Entity.tb_faltas remover = db.tb_faltas.First(x => x.id_funcionario == id);
                db.tb_faltas.Remove(remover);
                db.SaveChanges();
                contem = FuncionarioPossuiFalta(id);
            }          
        }
    }
}
