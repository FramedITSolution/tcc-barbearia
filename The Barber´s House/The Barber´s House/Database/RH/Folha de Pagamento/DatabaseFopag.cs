﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Folha_de_Pagamento
{
    class DatabaseFopag
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirFolhaDePagamento(Entity.tb_fopag folha)
        {
            db.tb_fopag.Add(folha);
            db.SaveChanges();
        }


        public Entity.tb_fopag ConsultarFolhaDePagamento(string funcionario, int mes)
        {
            return db.tb_fopag.
                                        First(x => x.tb_funcionario.nm_funcionario.Contains(funcionario) &&
                                                   x.dt_pagamento.Month == mes);

        }
        public Entity.tb_fopag ConsultarFolhaDePagamentoInserir(string funcionario)
        {
            return db.tb_fopag.
                                        First(x => x.tb_funcionario.nm_funcionario == funcionario);

        }

        public List<Entity.tb_fopag> ConsultarTodos()
        {
            return db.tb_fopag.
                                       ToList();

        }

        public void AlterarFolhaDePagamento(Entity.tb_fopag folha)
        {
            Entity.tb_fopag alterar = db.tb_fopag.
                                                           First(x => x.id_fopag == folha.id_fopag);
            alterar.id_funcionario = folha.id_funcionario;
            alterar.qt_falta = folha.qt_falta;
            alterar.qt_hora_extra = folha.qt_hora_extra;
            alterar.vl_adicional_noturno = folha.vl_adicional_noturno;
            alterar.vl_gratificacao = folha.vl_gratificacao;
            alterar.vl_plr = folha.vl_plr;
            alterar.vl_salario = folha.vl_salario;
            alterar.vl_total_descontos = folha.vl_total_descontos;
            alterar.vl_total_proventos = folha.vl_total_proventos;
            alterar.dt_pagamento = folha.dt_pagamento;
            alterar.ds_cnpj = folha.ds_cnpj;
            alterar.ds_adiantamento_semanal = folha.ds_adiantamento_semanal;

            db.SaveChanges();
        }

        public Entity.tb_fopag ConsultarPorIDDoFuncionario(int id)
        {
            return db.tb_fopag.First(x => x.tb_funcionario.id_funcionario == id);
        }

        public bool FuncionarioPossuiFolha(int id)
        {
            bool contem = db.tb_fopag.Any(x => x.id_funcionario == id);
            return contem;
        }

        public void RemoverFopag(int id)
        {
            bool contem = FuncionarioPossuiFolha(id);
            while(contem == true)
            {
                Entity.tb_fopag remover = db.tb_fopag.First(x => x.id_funcionario == id);
                db.tb_fopag.Remove(remover);
                db.SaveChanges();
                contem = FuncionarioPossuiFolha(id);
            }
        }
    }
}
