﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH
{
    class DatabaseFuncionário
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovoFuncionário(Entity.tb_funcionario func)
        {
            db.tb_funcionario.Add(func);
            db.SaveChanges();
        }

        public bool VerificarFuncionario(string funcionario)
        {
            bool contem = db.tb_funcionario
                                           .Any(x => x.nm_funcionario == funcionario);
            return contem;
        }

        public void AlterarFuncionario(Entity.tb_funcionario func)
        {
            Entity.tb_funcionario alterar = db.tb_funcionario.First(x => x.nm_funcionario == func.nm_funcionario);
            alterar.nm_empresa = func.nm_empresa;
            alterar.nm_mae = func.nm_mae;
            alterar.nm_pai = func.nm_pai;
            alterar.dt_nascimento = func.dt_nascimento;
            alterar.dt_adimissao = func.dt_adimissao;
            alterar.ds_genero = func.ds_genero;
            alterar.ds_endereco = func.ds_endereco;
            alterar.ds_email = func.ds_email;
            alterar.ds_cpf = func.ds_cpf;
            alterar.ds_rg = func.ds_rg;
            alterar.vl_comissao = func.vl_comissao;
            alterar.vl_insalubridade = func.vl_insalubridade;
            alterar.vl_inss = func.vl_inss;
            alterar.vl_irrf = func.vl_irrf;
            alterar.vl_periculosidade = func.vl_periculosidade;
            alterar.vl_salario_bruto = func.vl_salario_bruto;
            alterar.vl_salario_familia = func.vl_salario_familia;
            alterar.id_vale_transporte = func.id_vale_transporte;
            alterar.id_vale_alimentacao = func.id_vale_alimentacao;
            alterar.id_vale_refeicao = func.id_vale_refeicao;
            alterar.id_seguro_vida = func.id_seguro_vida;
            alterar.id_plano_odontologico = func.id_plano_odontologico;
            alterar.id_plano_de_saude = func.id_plano_de_saude;
            alterar.id_cargo = func.id_cargo;
            alterar.ds_telefone = func.ds_telefone;
            alterar.ds_telefone_de_emergencia = func.ds_telefone_de_emergencia;
            db.SaveChanges();
        }

        public void RemoverFuncionario(Entity.tb_funcionario func)
        {
            Entity.tb_funcionario remover = db.tb_funcionario.First(x => x.id_funcionario == func.id_funcionario);
            db.tb_funcionario.Remove(remover);
            db.SaveChanges();
        }

        public List<Entity.tb_funcionario> ListarFuncionarios()
        {
            List<Entity.tb_funcionario> lista = db.tb_funcionario.ToList();
            return lista;
        }

        public Entity.tb_funcionario ConsultarPorNome(string nome)
        {
            Entity.tb_funcionario model = db.tb_funcionario.FirstOrDefault(x => x.nm_funcionario == nome);
            return model;
        }

        public List<Entity.tb_funcionario> ConsultarPorID(int id)
        {
            List<Entity.tb_funcionario> model = db.tb_funcionario.Where(x => x.id_funcionario == id).ToList();
            return model;
        }
    }
}
