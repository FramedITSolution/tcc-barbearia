﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Vales
{
    class DatabaseVT
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirVT(Entity.tb_vale_transporte vt)
        {
            db.tb_vale_transporte.Add(vt);
            db.SaveChanges();
        }

        public void AlterarVT(Entity.tb_vale_transporte vt)
        {
            Entity.tb_vale_transporte alterar = db.tb_vale_transporte.First(x => x.id_vale_transporte == vt.id_vale_transporte);
            alterar.vl_vt = vt.vl_vt;
            alterar.ds_codigo_bilhete_unico = vt.ds_codigo_bilhete_unico;
            db.SaveChanges();
        }

        public Entity.tb_vale_transporte ConsultarVT(string codigo, decimal valor)
        {
            return db.tb_vale_transporte.
                                                  First(x => x.ds_codigo_bilhete_unico == codigo &&
                                                             x.vl_vt == valor);
        }

        public Entity.tb_vale_transporte ConsultarPorId(int id)
        {
            return db.tb_vale_transporte.
                                                  First(x => x.id_vale_transporte == id);

        }

        public void RemoverVT(Entity.tb_vale_transporte transporte)
        {
            Entity.tb_vale_transporte remover = db.tb_vale_transporte.First(x => x.id_vale_transporte == transporte.id_vale_transporte);
            db.tb_vale_transporte.Remove(remover);
            db.SaveChanges();
        }
    }
}
