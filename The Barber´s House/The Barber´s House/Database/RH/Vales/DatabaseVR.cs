﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Vales
{
    class DatabaseVR
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirVR(Entity.tb_vale_refeicao vr)
        {
            db.tb_vale_refeicao.Add(vr);
            db.SaveChanges();
        }

        public void AlterarVR(Entity.tb_vale_refeicao vr)
        {
            Entity.tb_vale_refeicao alterar = db.tb_vale_refeicao.First(x => x.id_vale_refeicao == vr.id_vale_refeicao);
            alterar.vl_vale_refeicao = vr.vl_vale_refeicao;
            alterar.ds_codigo_vr = vr.ds_codigo_vr;
            db.SaveChanges();
        }

        public Entity.tb_vale_refeicao ConsultarVR(string codigo, decimal valor)
        {
            return db.tb_vale_refeicao.
                                                  First(x => x.ds_codigo_vr == codigo &&
                                                             x.vl_vale_refeicao == valor);
        }

        public Entity.tb_vale_refeicao ConsultarPorId(int id)
        {
            return db.tb_vale_refeicao.
                                                  First(x => x.id_vale_refeicao == id);

        }

        public void RemoverVR(Entity.tb_vale_refeicao refeicao)
        {
            Entity.tb_vale_refeicao remover = db.tb_vale_refeicao.First(x => x.id_vale_refeicao == refeicao.id_vale_refeicao);
            db.tb_vale_refeicao.Remove(remover);
            db.SaveChanges();
        }
    }
}
