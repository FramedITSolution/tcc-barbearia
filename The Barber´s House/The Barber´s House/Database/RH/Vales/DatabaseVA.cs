﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.RH.Vales
{
    class DatabaseVA
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirVA(Entity.tb_vale_alimentacao va)
        {
            db.tb_vale_alimentacao.Add(va);
            db.SaveChanges();
        }

        public void AlterarVA(Entity.tb_vale_alimentacao va)
        {
            Entity.tb_vale_alimentacao alterar = db.tb_vale_alimentacao.First(x => x.id_vale_alimentacao == va.id_vale_alimentacao);
            alterar.vl_vale_alimentacao = va.vl_vale_alimentacao;
            alterar.ds_codigo_va = va.ds_codigo_va;
            db.SaveChanges();
        }

        public Entity.tb_vale_alimentacao ConsultarVA(string codigo, decimal valor)
        {
            return db.tb_vale_alimentacao.
                                                  First(x => x.ds_codigo_va == codigo &&
                                                             x.vl_vale_alimentacao == valor);
        }

        public Entity.tb_vale_alimentacao ConsultarPorId(int id)
        {
            return db.tb_vale_alimentacao.
                                                  First(x => x.id_vale_alimentacao == id);
                                                             
        }

        public void RemoverVA(Entity.tb_vale_alimentacao alimentacao)
        {
            Entity.tb_vale_alimentacao remover = db.tb_vale_alimentacao.First(x => x.id_vale_alimentacao == alimentacao.id_vale_alimentacao);
            db.tb_vale_alimentacao.Remove(remover);
            db.SaveChanges();
        }
    }
}
