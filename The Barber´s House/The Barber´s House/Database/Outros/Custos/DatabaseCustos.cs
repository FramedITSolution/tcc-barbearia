﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Outros.Custos
{
    class DatabaseCustos
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirCusto(Entity.tb_custo custo)
        {
            db.tb_custo.Add(custo);
            db.SaveChanges();
        }

        public void InserirCustoItem(Entity.tb_custo_item custo_item)
        {
            db.tb_custo_item.Add(custo_item);
            db.SaveChanges();
        }

        public Entity.tb_custo_item ConsultarPorIDP(int id_produto)
        {
            return db.tb_custo_item.FirstOrDefault(x => x.id_produto_compra == id_produto);
        }

        public Entity.tb_custo_item ConsultarPorValorUnitário(decimal valor)
        {
            return db.tb_custo_item.FirstOrDefault(x => x.vl_unitario == valor);
        }


        public Entity.tb_custo ConsultarPorIDF(int id_fornecedor)
        {
            return db.tb_custo.FirstOrDefault(x => x.id_fornecedor == id_fornecedor);
        }

        public Entity.tb_custo ConsultarPorDatadeCompra(DateTime data)
        {
            return db.tb_custo.FirstOrDefault(x => x.dt_compra == data);
        }
        public void AlterarCusto(Entity.tb_custo custo)
        {
            Entity.tb_custo alterar = db.tb_custo.First(x => x.id_custo == custo.id_custo);
            alterar.qt_compra = custo.qt_compra;
            alterar.vl_total = custo.vl_total;
            alterar.dt_compra = custo.dt_compra;
            alterar.id_fornecedor = custo.id_fornecedor;
            db.SaveChanges();
        }

        public void AlterarCustoItem(Entity.tb_custo_item custo)
        {
            Entity.tb_custo_item alterar = db.tb_custo_item.First(x => x.id_produto_compra == custo.id_produto_compra);
            alterar.vl_unitario = custo.vl_unitario;
            db.SaveChanges();
        }

        public void RemoverCusto(Entity.tb_custo custo)
        {
            Entity.tb_custo remover = db.tb_custo.First(x => x.id_custo == custo.id_custo);
            db.tb_custo.Remove(remover);
            db.SaveChanges();
        }

        public void RemoverCustoItem(Entity.tb_custo_item custos)
        {
            Entity.tb_custo_item remover = db.tb_custo_item.First(x => x.id_produto_compra == custos.id_produto_compra);
            db.tb_custo_item.Remove(remover);
            db.SaveChanges();
        }
    }
}
