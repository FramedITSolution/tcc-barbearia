﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Produtos
{
    class DatabaseProdutos_de_Compra
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovoProduto(Entity.tb_produto_compra produto)
        {
            db.tb_produto_compra.Add(produto);
            db.SaveChanges();
        }

        public void AlterarProduto(Entity.tb_produto_compra produto)
        {
            Entity.tb_produto_compra alterar = db.tb_produto_compra.First(x => x.nm_produto == produto.nm_produto);
            alterar.vl_preco = produto.vl_preco;
            alterar.ds_categoria = produto.ds_categoria;
            db.SaveChanges();
        }

        public void RemoverProduto(Entity.tb_produto_compra produto)
        {
            Entity.tb_produto_compra remover = db.tb_produto_compra.First(x => x.id_produto_compra == produto.id_produto_compra);
            db.tb_produto_compra.Remove(remover);
            db.SaveChanges();
        }

        public List<Entity.tb_produto_compra> ListarProdutos()
        {
            List<Entity.tb_produto_compra> lista = db.tb_produto_compra.ToList();
            return lista;
        }

        public List<Entity.tb_produto_compra> ConsultarPorId(int id)
        {
            List<Entity.tb_produto_compra> lista = db.tb_produto_compra.Where(x => x.id_produto_compra == id).ToList();
            return lista;
        }

        public Entity.tb_produto_compra ConsultarPorNome(string nome)
        {
            Entity.tb_produto_compra model = db.tb_produto_compra.First(x => x.nm_produto == nome);
            return model;
        }

        public bool ProdutoExistente(string produto)
        {
            //Verifica se já existe esse produto
            bool contem = db.tb_produto_compra
                                           .Any(x => x.nm_produto == produto);

            return contem;
        }
    }
}
