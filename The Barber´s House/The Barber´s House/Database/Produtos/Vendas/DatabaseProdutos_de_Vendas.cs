﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Produtos.Vendas
{
    class DatabaseProdutos_de_Vendas
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovoProduto(Entity.tb_produto_venda produto)
        {
            db.tb_produto_venda.Add(produto);
            db.SaveChanges();
        }

        public void AlterarProduto(Entity.tb_produto_venda produto)
        {
            Entity.tb_produto_venda alterar = db.tb_produto_venda.First(x => x.nm_produto == produto.nm_produto);
            alterar.vl_preco = produto.vl_preco;
            alterar.ds_categoria = produto.ds_categoria;
            alterar.ds_observacao = produto.ds_observacao;
            alterar.id_fornecedor = produto.id_fornecedor;
            db.SaveChanges();
        }

        public void RemoverProduto(Entity.tb_produto_venda produto)
        {
            Entity.tb_produto_venda remover = db.tb_produto_venda.First(x => x.nm_produto == produto.nm_produto);
            db.tb_produto_venda.Remove(remover);
            db.SaveChanges();
        }

        public List<Entity.tb_produto_venda> ListarProdutos()
        {
            List<Entity.tb_produto_venda> lista = db.tb_produto_venda.ToList();
            return lista;
        }

        public List<Entity.tb_produto_venda> ConsultarPorId(int id)
        {
            List<Entity.tb_produto_venda> lista = db.tb_produto_venda.Where(x => x.id_produtos == id).ToList();
            return lista;
        }

        public bool ProdutoExistente(string produto)
        {
            //Verifica se já existe esse produto
            bool contem = db.tb_produto_venda
                                           .Any(x => x.nm_produto == produto);

            return contem;
        }
    }
}
