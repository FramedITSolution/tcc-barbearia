﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Clientes
{
    class DatabaseClientes
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovoCliente(Entity.tb_cliente cliente)
        {
            db.tb_cliente.Add(cliente);
            db.SaveChanges();
        }

        public List<Entity.tb_cliente> ListarClientes()
        {
            List<Entity.tb_cliente> lista = db.tb_cliente.ToList();
            return lista;
        }

        public Entity.tb_cliente ConsultarPorNome(string nome)
        {           
            return db.tb_cliente.FirstOrDefault(x => x.nm_cliente == nome);
        }

        public Entity.tb_cliente ConsultarPorID(int id)
        {
            return db.tb_cliente.FirstOrDefault(x => x.id_cliente == id);
        }

        public List<Entity.tb_cliente> Consultar_ID(int id)
        {
            return db.tb_cliente.Where(x => x.id_cliente == id).ToList();
        }

        public bool VerificarCliente(string cliente)
        {
            bool contem = db.tb_cliente
                                           .Any(x => x.nm_cliente == cliente);
            return contem;
        }        

        public void AlterarCliente(Entity.tb_cliente cliente)
        {
            Entity.tb_cliente alterar = db.tb_cliente.First(x => x.nm_cliente == cliente.nm_cliente);
            alterar.dt_primeira_compra = cliente.dt_primeira_compra;
            alterar.ds_endereco = cliente.ds_endereco;
            alterar.ds_email = cliente.ds_email;
            alterar.bt_fidelizado = cliente.bt_fidelizado;
            alterar.bt_ativo = cliente.bt_ativo;           
            db.SaveChanges();
        }

        public void RemoverCliente(Entity.tb_cliente cliente)
        {
            Entity.tb_cliente remover = db.tb_cliente.First(x => x.id_cliente == cliente.id_cliente);
            db.tb_cliente.Remove(remover);
            db.SaveChanges();
        }
    }
}
