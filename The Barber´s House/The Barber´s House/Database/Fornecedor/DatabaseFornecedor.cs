﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Fornecedor
{
    class DatabaseFornecedor
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void NovoFornecedor(Entity.tb_fornecedor fornecedor)
        {
            db.tb_fornecedor.Add(fornecedor);
            db.SaveChanges();
        }

        public List<Entity.tb_fornecedor> ListarFornecedores()
        {
            List<Entity.tb_fornecedor> lista = db.tb_fornecedor.ToList();
            return lista;
        }

        public Entity.tb_fornecedor ConsultarPorId(int id)
        {
            Entity.tb_fornecedor model = db.tb_fornecedor.FirstOrDefault(x => x.id_fornecedor == id);
            return model;
        }

        public List<Entity.tb_fornecedor> Consultar_Id(int id)
        {
            List<Entity.tb_fornecedor> lista = db.tb_fornecedor.Where(x => x.id_fornecedor == id).ToList();
            return lista;
        }

        public Entity.tb_fornecedor ConsultarPorNome(string nome)
        {
            Entity.tb_fornecedor model = db.tb_fornecedor.FirstOrDefault(x => x.nm_fantasia == nome);
            return model;
        }

        public void AlterarFornecedor(Entity.tb_fornecedor model)
        {
            Entity.tb_fornecedor alterar = db.tb_fornecedor.First(x => x.ds_razao_social == model.ds_razao_social);
            alterar.nm_fantasia = model.nm_fantasia;
            alterar.ds_telefone = model.ds_telefone;
            alterar.ds_endereco = model.ds_endereco;
            alterar.ds_email = model.ds_email;
            alterar.ds_cnpj = model.ds_cnpj;
            alterar.ds_cep = model.ds_cep;
            db.SaveChanges();
        }

        public void RemoverFornecedor(Entity.tb_fornecedor model)
        {
            Entity.tb_fornecedor remover = db.tb_fornecedor.First(x => x.id_fornecedor == model.id_fornecedor);
            db.tb_fornecedor.Remove(remover);
            db.SaveChanges();
        }

        public bool FornecedorExistente(string fornecedor)
        {
            //Verifica se já existe esse fornecedor
            bool contem = db.tb_fornecedor
                                           .Any(x => x.nm_fantasia == fornecedor);

            return contem;
        }
    }
}
