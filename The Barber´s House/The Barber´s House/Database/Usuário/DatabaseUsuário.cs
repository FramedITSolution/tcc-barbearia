﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Usuário
{
    class DatabaseUsuário
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public bool EfetuarLogin(string usuario, string senha)
        {
            bool contem = db.tb_usuario
                                           .Any(x => x.nm_usuario == usuario && x.nm_senha == senha);

            return contem;
        }        

        public bool UsuarioExistente(string usuario)
        {
            //Verifica se já existe esse usuário
            bool contem = db.tb_usuario
                                           .Any(x => x.nm_usuario == usuario);

            return contem;
        }

        public bool FuncionarioUsuario(string funcionario)
        {
            //Verifica se esse funcioário já tem um usuário
            bool contem = db.tb_usuario
                                           .Any(x => x.tb_funcionario.nm_funcionario == funcionario);

            return contem;
        }

        public bool VerificarFuncionario(int funcionario)
        {
            bool contem = db.tb_funcionario
                                           .Any(x => x.id_funcionario == funcionario);
            return contem;
        }

        public void Cadastro(Entity.tb_usuario usuario)
        {
                db.tb_usuario.Add(usuario);
                db.SaveChanges();            
        }        

        public List<Entity.tb_usuario> ConsultarPorFuncionario(int funcionario)
        {
            List<Entity.tb_usuario> lista = db.tb_usuario.Where(x => x.id_funcionario == funcionario).ToList();
            return lista;
        }

        public Entity.tb_usuario ConsultarPorUsuario(string nome)
        {
            Entity.tb_usuario model = db.tb_usuario.FirstOrDefault(x => x.nm_usuario == nome);
            return model;
        }

        public void AlterarUsuario(Entity.tb_usuario usuario)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario.nm_usuario);
            alterar.nm_senha = usuario.nm_senha;
            db.SaveChanges();
        }
      

        public List<Entity.tb_usuario> ListarUsuarios()
        {
            List<Entity.tb_usuario> lista = db.tb_usuario.ToList();
            return lista;
        }

        public List<Entity.tb_usuario> ConsultarPorId(int id)
        {
            List<Entity.tb_usuario> lista = db.tb_usuario.Where(x=> x.id_usuario == id).ToList();
            return lista;
        }

        public bool FuncionarioPossuiLogin(int idFuncionario)
        {
            return db.tb_usuario.Any(x => x.id_funcionario == idFuncionario);
        }

        public Entity.tb_usuario ConsultarPorIDF(int id)
        {
            return db.tb_usuario.First(x => x.id_funcionario == id);
        }

        public void AdicionarCodigo(string codigo, string usuario)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario);
            alterar.ds_codigo_alteracao = codigo;
            db.SaveChanges();
        }

        public void LimparCodigo(string usuario)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario);
            alterar.ds_codigo_alteracao = string.Empty;
            db.SaveChanges();
        }

        public Entity.tb_usuario ModeloUsuarioAtivo(string usuario)
        {
            Entity.tb_usuario lista = db.tb_usuario.
                                                    FirstOrDefault(x => x.nm_usuario == usuario);

            return lista;
        }

        public void AlterarSenha(string usuario, string senha, string codigo)
        {
            Entity.tb_usuario alterar = db.tb_usuario.First(x => x.nm_usuario == usuario &&
                                                                                x.ds_codigo_alteracao == codigo);
            alterar.ds_codigo_alteracao = "";
            alterar.nm_senha = senha;

            db.SaveChanges();
        }

        public bool VerificarCodigo(string usuario, string codigo)
        {
            return db.tb_usuario.Any(x => x.nm_usuario == usuario &&
                                                         x.ds_codigo_alteracao == codigo);
        }

        public void RemoverUsuario(Entity.tb_usuario usuario)
        {
            Entity.tb_usuario remover = db.tb_usuario.First(x => x.nm_usuario == usuario.nm_usuario);
            db.tb_usuario.Remove(remover);
            db.SaveChanges();
        }

        public void RemoverUsuarioPorID(int id)
        {
            Entity.tb_usuario remover = db.tb_usuario.First(x => x.id_funcionario == id);
            db.tb_usuario.Remove(remover);
            db.SaveChanges();
        }
    }
}
