﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Database.Estoque
{
    class DatabaseEstoque
    {
        Entity.mydbEntities1 db = new Entity.mydbEntities1();

        public void InserirNoEstoque(Entity.tb_estoque estoque)
        {
            db.tb_estoque.Add(estoque);
            db.SaveChanges();
        }

        public List<Entity.tb_estoque> ListarEstoque()
        {
            return db.tb_estoque.ToList();
        }

        public Entity.tb_estoque ConsultarPorIDP(int id_produto)
        {
            return db.tb_estoque.First(x => x.id_produto_compra == id_produto);
        }

        public List<Entity.tb_estoque> Consultar_ID(int id_produto)
        {
             
           /*var query = db.tb_estoque
                   .Join(db.tb_produto_compra, a => a.id_produto_compra, u => u.id_produto_compra,
                         (a, u) => new { a, u })
                   .Where(r => r.u.id_produto_compra == id_produto)
           .Select(q => new Entity.tb_produto_compra
           {
               id_produto_compra = q.a.id_produto_compra,
               nm_produto = q.u.nm_produto,
               ds_categoria = q.u.ds_categoria,
               vl_preco = q.u.vl_preco,               
           })
           .ToList();*/

            return db.tb_estoque.Where(x => x.id_produto_compra == id_produto).ToList(); 
        }

        public void AlterarEstoque(Entity.tb_estoque estoque)
        {
            Entity.tb_estoque alterar = db.tb_estoque.First(x => x.id_produto_compra == estoque.id_produto_compra);
            alterar.qt_estoque = estoque.qt_estoque;
            alterar.vl_total = estoque.vl_total;
            alterar.dt_validade_prevista = estoque.dt_validade_prevista;
            alterar.ds_detalhe = estoque.ds_detalhe;
            alterar.ds_situacao_produto = estoque.ds_situacao_produto;
            alterar.bt_urgente = estoque.bt_urgente;
            alterar.bt_abastecer = estoque.bt_abastecer;
            alterar.id_produto_compra = estoque.id_produto_compra;
            db.SaveChanges();
        }

        public void RemoverDoEstoque(Entity.tb_estoque estoque)
        {
            Entity.tb_estoque remover = db.tb_estoque.First(x => x.id_estoque == estoque.id_estoque);
            db.tb_estoque.Remove(remover);
            db.SaveChanges();
        }
    }
}
