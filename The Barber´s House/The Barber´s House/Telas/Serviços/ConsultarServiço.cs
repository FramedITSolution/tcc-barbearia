﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Serviços
{
    public partial class ConsultarServiço : Form
    {
        public ConsultarServiço()
        {
            InitializeComponent();
            dgvServiço.AutoGenerateColumns = false;
        }

        Business.Serviços.BusinessServiços business = new Business.Serviços.BusinessServiços();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {                
                List<Database.Entity.tb_servico> serviços = business.ListarServiços();
                dgvServiço.DataSource = serviços;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
               MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudID.Value);
                dgvServiço.DataSource = business.Consultar_Id(id);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }           
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
    }
}
