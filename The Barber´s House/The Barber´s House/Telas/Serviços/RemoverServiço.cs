﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Serviços
{
    public partial class RemoverServiço : Form
    {
        public RemoverServiço()
        {
            InitializeComponent();
            CarregarServiços();
        }

        Business.Serviços.BusinessServiços servicos = new Business.Serviços.BusinessServiços();
        Business.Serviços.BusinessServiçosPorCliente servicosPorCliente = new Business.Serviços.BusinessServiçosPorCliente();
        Business.RH.BusinessFuncionário funcionário = new Business.RH.BusinessFuncionário();
        Business.Cortes.BusinessCortes cortes = new Business.Cortes.BusinessCortes();
        Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
      

        private void CarregarServiços()
        {           
            List<Database.Entity.tb_servico> lista = servicos.ListarServiços();

            cboServiço.ValueMember = nameof(Database.Entity.tb_servico.id_servico);
            cboServiço.DataSource = lista;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_servico model = new Database.Entity.tb_servico();
                Database.Entity.tb_servicos_por_cliente servicos_Por_Cliente = new Database.Entity.tb_servicos_por_cliente();
                Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
                Database.Entity.tb_corte corte = cboCorte.SelectedItem as Database.Entity.tb_corte;
                Database.Entity.tb_servico servico = cboServiço.SelectedItem as Database.Entity.tb_servico;
                model.id_servico = servico.id_servico;
                model.vl_servico = nudValor.Value;
                model.dt_servico = dtpDataServiço.Value.Date;
                model.nm_responsavel_servico = cboResponsável.Text;
                model.hr_servico = TimeSpan.Parse(txtHorário.Text);
                model.id_cliente = clientes.ConsultarPorNome(txtCliente.Text).id_cliente;
                model.id_corte = corte.id_corte;
                servicos_Por_Cliente.id_servico = servico.id_servico;
                servicosPorCliente.RemoverServiçoPorCliente(servicos_Por_Cliente);
                servicos.RemoverServiço(model);
                if (txtHorário.MaskCompleted == true)
                {
                    MessageBox.Show("Atendimento removido com sucesso");
                    CarregarServiços();
                }
                else
                {
                    MessageBox.Show("Prencha todo o horário");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void cboServiço_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_servico combo = cboServiço.SelectedItem as Database.Entity.tb_servico;
            Database.Entity.tb_servico servico = servicos.ConsultarPorId(combo.id_servico);
            Database.Entity.tb_cliente entity = clientes.ConsultarPorId(combo.id_cliente);
            txtCliente.Text = entity.nm_cliente;
            cboResponsável.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboResponsável.DataSource = funcionário.ListarFuncionarios();
            cboCorte.DisplayMember = nameof(Database.Entity.tb_corte.nm_corte);
            cboCorte.DataSource = cortes.ListarCortes();
            nudValor.Value = servico.vl_servico;
            dtpDataServiço.Value = servico.dt_servico;
            txtHorário.Text = Convert.ToString(servico.hr_servico);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Database.Entity.tb_servico model = new Database.Entity.tb_servico();
                Database.Entity.tb_servicos_por_cliente servicos_Por_Cliente = new Database.Entity.tb_servicos_por_cliente();
                Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
                Database.Entity.tb_corte corte = cboCorte.SelectedItem as Database.Entity.tb_corte;
                Database.Entity.tb_servico servico = cboServiço.SelectedItem as Database.Entity.tb_servico;
                model.id_servico = servico.id_servico;
                model.vl_servico = nudValor.Value;
                model.dt_servico = dtpDataServiço.Value.Date;
                model.nm_responsavel_servico = cboResponsável.Text;
                model.hr_servico = TimeSpan.Parse(txtHorário.Text);
                model.id_cliente = clientes.ConsultarPorNome(txtCliente.Text).id_cliente;
                model.id_corte = corte.id_corte;
                servicos_Por_Cliente.id_servico = servico.id_servico;
                servicosPorCliente.RemoverServiçoPorCliente(servicos_Por_Cliente);
                servicos.RemoverServiço(model);
                if (txtHorário.MaskCompleted == true)
                {
                    MessageBox.Show("Atendimento removido com sucesso");
                    CarregarServiços();
                }
                else
                {
                    MessageBox.Show("Prencha todo o horário");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
    }
}
