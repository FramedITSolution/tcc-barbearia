﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Serviços
{
    public partial class AlterarServiço : Form
    {
        public AlterarServiço()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Serviços.BusinessServiços servicos = new Business.Serviços.BusinessServiços();
        Business.RH.BusinessFuncionário funcionário = new Business.RH.BusinessFuncionário();
        Business.Cortes.BusinessCortes cortes = new Business.Cortes.BusinessCortes();
        Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_servico model = new Database.Entity.tb_servico();
                Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
                Database.Entity.tb_corte corte = cboCorte.SelectedItem as Database.Entity.tb_corte;
                model.vl_servico = nudValor.Value;
                model.dt_servico = dtpDataServiço.Value.Date;
                model.nm_responsavel_servico = cboResponsável.Text;
                model.id_cliente = clientes.ConsultarPorNome(txtCliente.Text).id_cliente;
                model.id_corte = corte.id_corte;

                if (txtHorário.Text == "  :" || txtHorário.Text == "00:")
                {
                    MessageBox.Show("Informe um horário");
                }
                else if (servicos.VerificarHorário(txtHorário.Text) == false && txtHorário.MaskCompleted == true)
                {
                    model.hr_servico = TimeSpan.Parse(txtHorário.Text);
                    servicos.AlterarServiço(model);
                    MessageBox.Show("Atendimento agendado com sucesso");
                }
                else
                {
                    MessageBox.Show("Horário inválido, digite outro");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void CarregarCombo()
        {
            cboServiço.DisplayMember = nameof(Database.Entity.tb_servico.id_servico);
            cboServiço.DataSource = servicos.ListarServiços();
        }

        private void cboServiço_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_servico combo = cboServiço.SelectedItem as Database.Entity.tb_servico;
            Database.Entity.tb_servico servico = servicos.ConsultarPorId(combo.id_servico);
            Database.Entity.tb_cliente entity = clientes.ConsultarPorId(combo.id_cliente);
            txtCliente.Text = entity.nm_cliente;
            cboResponsável.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboResponsável.DataSource = funcionário.ListarFuncionarios();
            cboCorte.DisplayMember = nameof(Database.Entity.tb_corte.nm_corte);
            cboCorte.DataSource = cortes.ListarCortes();
            nudValor.Value = servico.vl_servico;
            dtpDataServiço.Value = servico.dt_servico;
            txtHorário.Text = Convert.ToString(servico.hr_servico);
        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Database.Entity.tb_servico model = new Database.Entity.tb_servico();
                Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
                Database.Entity.tb_corte corte = cboCorte.SelectedItem as Database.Entity.tb_corte;
                model.vl_servico = nudValor.Value;
                model.dt_servico = dtpDataServiço.Value.Date;
                model.nm_responsavel_servico = cboResponsável.Text;
                model.id_cliente = clientes.ConsultarPorNome(txtCliente.Text).id_cliente;
                model.id_corte = corte.id_corte;

                if (txtHorário.Text == "  :" || txtHorário.Text == "00:")
                {
                    MessageBox.Show("Informe um horário");
                }
                else if (servicos.VerificarHorário(txtHorário.Text) == false && txtHorário.MaskCompleted == true)
                {
                    model.hr_servico = TimeSpan.Parse(txtHorário.Text);
                    servicos.AlterarServiço(model);
                    MessageBox.Show("Atendimento agendado com sucesso");
                }
                else
                {
                    MessageBox.Show("Horário inválido, digite outro");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void LblMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
            Hide();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
            Hide();
        }

    }
}
