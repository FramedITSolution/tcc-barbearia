﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Serviços
{
    public partial class NovoAtendimento : Form
    {
        public NovoAtendimento()
        {
            InitializeComponent();
            CarregarCombos();
        }

        Database.Entity.tb_servico serviço = new Database.Entity.tb_servico();
        Business.Serviços.BusinessServiços business = new Business.Serviços.BusinessServiços();
        Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
        Business.RH.BusinessFuncionário funcionário = new Business.RH.BusinessFuncionário();
        Business.Cortes.BusinessCortes cortes = new Business.Cortes.BusinessCortes();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_corte corte = cboCorte.SelectedItem as Database.Entity.tb_corte;
                serviço.nm_responsavel_servico = cboResponsável.Text;
                serviço.id_cliente = clientes.ConsultarPorNome(cboCliente.Text).id_cliente;
                serviço.id_corte = corte.id_corte;
                serviço.vl_servico = nudPreço.Value;
                serviço.dt_servico = dtpServiço.Value;

                Database.Entity.tb_cliente model = clientes.ConsultarPorNome(cboCliente.Text);
                serviço.id_cliente = model.id_cliente;

                if (txtHorário.Text == "  :" || txtHorário.Text == "00:")
                {
                    MessageBox.Show("Informe um horário");
                }
                else if (business.VerificarHorário(txtHorário.Text) == false && txtHorário.MaskCompleted == true)
                {
                    serviço.hr_servico = TimeSpan.Parse(txtHorário.Text);
                    business.NovoAtendimento(serviço);
                    MessageBox.Show("Atendimento agendado com sucesso");
                }
                else
                {
                    MessageBox.Show("Horário inválido, digite outro");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void CarregarCombos()
        {
            List<Database.Entity.tb_cliente> lista = clientes.ListarClientes();

            cboCliente.ValueMember = nameof(Database.Entity.tb_cliente.nm_cliente);
            cboCliente.DataSource = lista;

            cboResponsável.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboResponsável.DataSource = funcionário.ListarFuncionarios();

            cboCorte.DisplayMember = nameof(Database.Entity.tb_corte.nm_corte);
            cboCorte.DataSource = cortes.ListarCortes();

        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void cboCorte_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_corte model = cboCorte.SelectedItem as Database.Entity.tb_corte;
            nudPreço.Value = model.vl_corte;
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void LblMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_corte corte = cboCorte.SelectedItem as Database.Entity.tb_corte;
                serviço.nm_responsavel_servico = cboResponsável.Text;
                serviço.id_cliente = clientes.ConsultarPorNome(cboCliente.Text).id_cliente;
                serviço.id_corte = corte.id_corte;
                serviço.vl_servico = nudPreço.Value;
                serviço.dt_servico = dtpServiço.Value;

                Database.Entity.tb_cliente model = clientes.ConsultarPorNome(cboCliente.Text);
                serviço.id_cliente = model.id_cliente;

                if (txtHorário.Text == "  :" || txtHorário.Text == "00:")
                {
                    MessageBox.Show("Informe um horário");
                }
                else if (business.VerificarHorário(txtHorário.Text) == false && txtHorário.MaskFull == true)
                {
                    serviço.hr_servico = TimeSpan.Parse(txtHorário.Text);
                    business.NovoAtendimento(serviço);
                    MessageBox.Show("Atendimento agendado com sucesso");
                }
                else
                {
                    MessageBox.Show("Horário inválido, digite outro");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
    }
}
