﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Fornecedor
{
    public partial class RemoverFornecedor : Form
    {
        public RemoverFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Fornecedor.BusinessFornecedor business = new Business.Fornecedor.BusinessFornecedor();

        private void CarregarCombo()
        {
            cboRazãoSocial.DisplayMember = nameof(Database.Entity.tb_fornecedor.ds_razao_social);
            cboRazãoSocial.DataSource = business.ListarFornecedores();
        }

        private void cboNomedofornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_fornecedor forn = cboRazãoSocial.SelectedItem as Database.Entity.tb_fornecedor;
            txtFantasia.Text = forn.nm_fantasia;
            txtEndereco.Text = forn.ds_endereco;
            txtEmail.Text = forn.ds_email;
            mktCEP.Text = forn.ds_cep;
            mktCNPJ.Text = forn.ds_cnpj;
            txtTelefone.Text = forn.ds_telefone;
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_fornecedor model = new Database.Entity.tb_fornecedor();
                Database.Entity.tb_fornecedor forn = cboRazãoSocial.SelectedItem as Database.Entity.tb_fornecedor;
                Business.Outros.Custos.BusinessCustos custos = new Business.Outros.Custos.BusinessCustos();
                model.id_fornecedor = forn.id_fornecedor;
                model.nm_fantasia = cboRazãoSocial.Text;
                model.ds_telefone = txtTelefone.Text;
                model.ds_endereco = txtEndereco.Text;
                model.ds_email = txtEmail.Text;
                model.ds_cnpj = mktCNPJ.Text;
                model.ds_cep = mktCEP.Text;
                
                business.RemoverFornecedor(model);
                MessageBox.Show("Fornecedor removido com sucesso");
                CarregarCombo();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
    }
}
