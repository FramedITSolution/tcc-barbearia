﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Fornecedor
{
    public partial class ConsultarFornecedor : Form
    {
        public ConsultarFornecedor()
        {
            InitializeComponent();
            dgvFornecedores.AutoGenerateColumns = false;
        }

        Business.Fornecedor.BusinessFornecedor forn = new Business.Fornecedor.BusinessFornecedor();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvFornecedores.DataSource = forn.ListarFornecedores();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudIDFornecedor.Value);
                dgvFornecedores.DataSource = forn.Consultar_Id(id);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }            
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
    }
}
