﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Fornecedor
{
    public partial class NovoFornecedor : Form
    {
        public NovoFornecedor()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();
                Business.Fornecedor.BusinessFornecedor business = new Business.Fornecedor.BusinessFornecedor();
                Database.Entity.tb_fornecedor forn = new Database.Entity.tb_fornecedor();

                forn.ds_razao_social = txtRazãoSocial.Text;
                forn.nm_fantasia = txtFantasia.Text;
                forn.ds_endereco = txtEndereço.Text;
                forn.ds_cep = mktCEP.Text;
                forn.ds_cnpj = mktCNPJ.Text;
                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, true);

                if (contem == true && txtEmail.Text != string.Empty
                    && invalido == false && telefoneValido == true)
                {
                    forn.ds_email = txtEmail.Text;
                    forn.ds_telefone = txtTelefone.Text;
                    business.NovoFornecedor(forn);

                    MessageBox.Show("Fornecedor inserido com sucesso");
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show("Email/Telefone inválido, preencha novamente");
                }

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void LimparCampos()
        {
            txtRazãoSocial.Text = string.Empty;
            txtFantasia.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtEndereço.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            mktCEP.Text = string.Empty;
            mktCNPJ.Text = string.Empty;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
