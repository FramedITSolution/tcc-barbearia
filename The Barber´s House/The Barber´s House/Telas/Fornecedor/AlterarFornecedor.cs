﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Fornecedor
{
    public partial class AlterarFornecedor : Form
    {
        public AlterarFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Fornecedor.BusinessFornecedor business = new Business.Fornecedor.BusinessFornecedor();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();
                Database.Entity.tb_fornecedor model = new Database.Entity.tb_fornecedor();
                model.ds_razao_social = cboRazãoSocial.Text;
                model.ds_endereco = txtEndereço.Text;
                model.ds_cep = mktCEP.Text;
                model.ds_cnpj = mktCNPJ.Text;
                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, true);
               
                if (contem == true && txtEmail.Text != string.Empty 
                    && invalido == false && telefoneValido == true)
                {
                    model.ds_email = txtEmail.Text;
                    model.ds_telefone = txtTelefone.Text;
                    business.AlterarFornecedor(model);

                    MessageBox.Show("Fornecedor alterado com sucesso");
                }
                else
                {
                    MessageBox.Show("Email/Telefone inválido, preencha novamente");
                }                              
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void CarregarCombo()
        {
            cboRazãoSocial.DisplayMember = nameof(Database.Entity.tb_fornecedor.ds_razao_social);
            cboRazãoSocial.DataSource = business.ListarFornecedores();
        }

        private void cboNomedofornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_fornecedor forn = cboRazãoSocial.SelectedItem as Database.Entity.tb_fornecedor;
            txtFantasia.Text = forn.nm_fantasia;
            txtEndereço.Text = forn.ds_endereco;
            txtEmail.Text = forn.ds_email;
            mktCEP.Text = forn.ds_cep;
            mktCNPJ.Text = forn.ds_cnpj;
            txtTelefone.Text = forn.ds_telefone;
        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();
                Database.Entity.tb_fornecedor model = new Database.Entity.tb_fornecedor();
                model.ds_razao_social = cboRazãoSocial.Text;
                model.ds_endereco = txtEndereço.Text;
                model.ds_cep = mktCEP.Text;
                model.ds_cnpj = mktCNPJ.Text;
                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, true);

                if (contem == true && txtEmail.Text != string.Empty
                    && invalido == false && telefoneValido == true)
                {
                    model.ds_email = txtEmail.Text;
                    model.ds_telefone = txtTelefone.Text;
                    business.AlterarFornecedor(model);

                    MessageBox.Show("Fornecedor alterado com sucesso");
                }
                else
                {
                    MessageBox.Show("Email/Telefone inválido, preencha novamente");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
    }
}
