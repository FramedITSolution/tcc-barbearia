﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.ProdutosVendas
{
    public partial class RemoverProduto : Form
    {
        public RemoverProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Produtos.Vendas.BusinessProdutos_de_Vendas vendas = new Business.Produtos.Vendas.BusinessProdutos_de_Vendas();
        Business.Fornecedor.BusinessFornecedor fornecedor = new Business.Fornecedor.BusinessFornecedor();
        Business.Vendas.BusinessVendaItem vendaitem = new Business.Vendas.BusinessVendaItem();

        private void btnRemoverproduto_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_produto_venda product = cboProduto.SelectedItem as Database.Entity.tb_produto_venda;
                Database.Entity.tb_produto_venda model = new Database.Entity.tb_produto_venda();
                model.id_produtos = product.id_produtos;
                model.nm_produto = cboProduto.Text;
                model.vl_preco = nudUnitario.Value;
                model.ds_categoria = cboCategoria.Text;
                model.ds_observacao = txtObservacao.Text;
                Database.Entity.tb_fornecedor forn = cboFornecedor.SelectedItem as Database.Entity.tb_fornecedor;
                model.id_fornecedor = forn.id_fornecedor;
                vendaitem.RemoverVendaPorPV(product.id_produtos);
                vendas.RemoverProduto(model);
                MessageBox.Show("Produto removido com sucesso");
                CarregarCombo();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //catch (Exception)
            //{
            //    MessageBox.Show("Ocorreu um erro");
            //}
        }

        private void CarregarCombo()
        {
            try
            {
                cboProduto.DisplayMember = nameof(Database.Entity.tb_produto_venda.nm_produto);
                cboProduto.DataSource = vendas.ListarProdutos();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
            
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_produto_venda model = cboProduto.SelectedItem as Database.Entity.tb_produto_venda;
                nudUnitario.Value = model.vl_preco;
                txtObservacao.Text = model.ds_observacao;
                cboFornecedor.DisplayMember = nameof(Database.Entity.tb_fornecedor.nm_fantasia);
                cboFornecedor.DataSource = fornecedor.ListarFornecedores();

                cboCategoria.DisplayMember = nameof(Database.Entity.tb_produto_venda.ds_categoria);
                cboCategoria.DataSource = vendas.ListarProdutos();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }            
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
