﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.ProdutosVendas
{
    public partial class ConsultarProdutos : Form
    {
        public ConsultarProdutos()
        {
            InitializeComponent();
            dgvProdutos.AutoGenerateColumns = false;
        }

        Business.Produtos.Vendas.BusinessProdutos_de_Vendas vendas = new Business.Produtos.Vendas.BusinessProdutos_de_Vendas();

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudIDProduto.Value);
                dgvProdutos.DataSource = vendas.ConsultarPorID(id);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvProdutos.DataSource = vendas.ListarProdutos();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu start = new Outros.Menu();
            start.Show();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
