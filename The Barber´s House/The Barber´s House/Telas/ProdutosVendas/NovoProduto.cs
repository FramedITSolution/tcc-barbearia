﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.ProdutosVendas
{
    public partial class NovoProduto : Form
    {
        public NovoProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }


        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                Business.Produtos.Vendas.BusinessProdutos_de_Vendas vendas = new Business.Produtos.Vendas.BusinessProdutos_de_Vendas();
                Database.Entity.tb_produto_venda model = new Database.Entity.tb_produto_venda();
                Database.Entity.tb_fornecedor forn = cboFornecedor.SelectedItem as Database.Entity.tb_fornecedor;
                model.nm_produto = txtProduto.Text;
                model.vl_preco = nudUnitario.Value;
                model.ds_observacao = txtObservacao.Text;
                model.ds_categoria = txtCategoria.Text;
                model.id_fornecedor = forn.id_fornecedor;
                vendas.NovoProduto(model);

                MessageBox.Show("Produto inserido com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void CarregarCombo()
        {
            try
            {
                Business.Fornecedor.BusinessFornecedor forn = new Business.Fornecedor.BusinessFornecedor();
                cboFornecedor.DisplayMember = nameof(Database.Entity.tb_fornecedor.nm_fantasia);
                cboFornecedor.DataSource = forn.ListarFornecedores();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
    }
}
