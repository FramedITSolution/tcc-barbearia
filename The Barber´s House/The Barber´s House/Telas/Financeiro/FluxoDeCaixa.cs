﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Financeiro
{
    public partial class FluxoDeCaixa : Form
    {
        public FluxoDeCaixa()
        {
            InitializeComponent();
            dgvFluxoCaixa.AutoGenerateColumns= false;
        }

        Business.Financeiro.BusinessFinanceiro financeiro = new Business.Financeiro.BusinessFinanceiro();
        private void FluxoDeCaixa_Load(object sender, EventArgs e)
        {            
            List<Database.Entity.vw_fluxo_de_caixa> lista = financeiro.ConsultarTodos();
            dgvFluxoCaixa.DataSource = lista;
        }

        private void btnButao_Click(object sender, EventArgs e)
        {
            DateTime inicio = dtpInicio.Value.Date;
            DateTime final = dtpFinal.Value.Date;

            List<Database.Entity.vw_fluxo_de_caixa> lista = financeiro.ConsultarViewFluxoDeCaixa(inicio, final);
            dgvFluxoCaixa.DataSource = lista;
        }

        private void lblFechar_Click_1(object sender, EventArgs e)
        {
            Hide();
            WindowState = FormWindowState.Minimized;
            //Outros.MenuProvisório start = new Outros.MenuProvisório();
            //start.Show();
        }

        private void lblMinimizar_Click_1(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu start = new Outros.Menu();
            start.Show();
        }
    }
}
