﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Financeiro
{
    public partial class InserirDespesa : Form
    {
        public InserirDespesa()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Business.Financeiro.BusinessFinanceiro despesas = new Business.Financeiro.BusinessFinanceiro();
                Database.Entity.tb_fluxo_caixa fluxo = new Database.Entity.tb_fluxo_caixa();
                fluxo.nm_referencia = txtReferência.Text;
                fluxo.dt_saida = dtpSaída.Value.Date;
                fluxo.dt_entrada = dtpEntrada.Value.Date;
                fluxo.ds_previsto = nudPrevisto.Value;
                fluxo.ds_realizado = nudRealizado.Value;
                despesas.InserirDespesa(fluxo);
                MessageBox.Show("Despesa inserida com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }

        }
        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
