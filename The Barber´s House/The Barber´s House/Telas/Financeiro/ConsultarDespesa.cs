﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Financeiro
{
    public partial class ConsultarDespesa : Form
    {
        public ConsultarDespesa()
        {
            InitializeComponent();
            dgvLista.AutoGenerateColumns = false;
        }

        Business.Financeiro.BusinessFinanceiro despesas = new Business.Financeiro.BusinessFinanceiro();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvLista.DataSource = despesas.ListarDespesas();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudIDDespesa.Value);
                dgvLista.DataSource = despesas.ConsultarPorId(id);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu start = new Outros.Menu();
            start.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu start = new Outros.Menu();
            start.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;
        }
    }
}
