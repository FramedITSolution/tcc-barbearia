﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Financeiro
{
    public partial class AlterarDespesa : Form
    {
        public AlterarDespesa()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Financeiro.BusinessFinanceiro financeiro = new Business.Financeiro.BusinessFinanceiro();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Alterar();
        }

        private void CarregarCombo()
        {
            cboReferência.DisplayMember = nameof(Database.Entity.tb_fluxo_caixa.nm_referencia);
            cboReferência.DataSource = financeiro.ListarDespesas();
        }

        private void cboReferência_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_fluxo_caixa model = cboReferência.SelectedItem as Database.Entity.tb_fluxo_caixa;
            nudValorPrevisto.Value = model.ds_previsto;
            nudValorRealizado.Value = model.ds_realizado;
            dtpEntrada.Value = model.dt_entrada.Date;
            dtpSaída.Value = model.dt_saida.Date;
        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
            Alterar();
        }

        private void Alterar()
        {
            try
            {
                Database.Entity.tb_fluxo_caixa fluxo = new Database.Entity.tb_fluxo_caixa();
                Database.Entity.tb_fluxo_caixa model = cboReferência.SelectedItem as Database.Entity.tb_fluxo_caixa;
                fluxo.id_fluxo_caixa = model.id_fluxo_caixa;
                fluxo.nm_referencia = cboReferência.Text;
                fluxo.dt_entrada = dtpEntrada.Value;
                fluxo.dt_saida = dtpSaída.Value;
                fluxo.ds_previsto = nudValorPrevisto.Value;
                fluxo.ds_realizado = nudValorRealizado.Value;
                financeiro.AlterarDespesa(fluxo);
                MessageBox.Show("Despesa alterada com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
               MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }
        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
