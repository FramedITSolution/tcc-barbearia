﻿namespace The_Barber_s_House.Telas.RH.Funcionário
{
    partial class ConsultarFuncionário
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.nudIDFuncionário = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvFuncionarios = new System.Windows.Forms.DataGridView();
            this.btnListar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.picVoltar = new System.Windows.Forms.PictureBox();
            this.IDF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.detalhe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomedaMãe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomedoPai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_empresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bt_urgente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_inss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_irrf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_salario_familia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_periculosidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_insalubridade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_nascimento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_adimissão = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_salario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelefoneEmergencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_cpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_endereco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_vale_trans = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDvalealimentacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_vale_refeicao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_plano_de_saude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_plano_odontológico = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_seguro_vida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_cargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIDFuncionário)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionarios)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox1.Controls.Add(this.btnConsultar);
            this.groupBox1.Controls.Add(this.nudIDFuncionário);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dgvFuncionarios);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(21, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(456, 245);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // btnConsultar
            // 
            this.btnConsultar.BackColor = System.Drawing.Color.Maroon;
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.ForeColor = System.Drawing.Color.White;
            this.btnConsultar.Location = new System.Drawing.Point(295, 19);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(110, 23);
            this.btnConsultar.TabIndex = 5;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = false;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // nudIDFuncionário
            // 
            this.nudIDFuncionário.Location = new System.Drawing.Point(153, 22);
            this.nudIDFuncionário.Name = "nudIDFuncionário";
            this.nudIDFuncionário.Size = new System.Drawing.Size(120, 20);
            this.nudIDFuncionário.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Id do funcionário";
            // 
            // dgvFuncionarios
            // 
            this.dgvFuncionarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFuncionarios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDF,
            this.detalhe,
            this.NomedaMãe,
            this.NomedoPai,
            this.nm_empresa,
            this.bt_urgente,
            this.vl_inss,
            this.vl_irrf,
            this.vl_salario_familia,
            this.vl_periculosidade,
            this.vl_insalubridade,
            this.dt_nascimento,
            this.dt_adimissão,
            this.vl_salario,
            this.ds_telefone,
            this.TelefoneEmergencia,
            this.ds_cpf,
            this.RG,
            this.ds_endereco,
            this.ds_email,
            this.id_vale_trans,
            this.IDvalealimentacao,
            this.id_vale_refeicao,
            this.id_plano_de_saude,
            this.id_plano_odontológico,
            this.id_seguro_vida,
            this.id_cargo});
            this.dgvFuncionarios.Location = new System.Drawing.Point(12, 63);
            this.dgvFuncionarios.Name = "dgvFuncionarios";
            this.dgvFuncionarios.Size = new System.Drawing.Size(444, 165);
            this.dgvFuncionarios.TabIndex = 1;
            // 
            // btnListar
            // 
            this.btnListar.BackColor = System.Drawing.Color.Maroon;
            this.btnListar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListar.ForeColor = System.Drawing.Color.White;
            this.btnListar.Location = new System.Drawing.Point(21, 303);
            this.btnListar.Name = "btnListar";
            this.btnListar.Size = new System.Drawing.Size(456, 28);
            this.btnListar.TabIndex = 11;
            this.btnListar.Text = "Listar todos funcionários";
            this.btnListar.UseVisualStyleBackColor = false;
            this.btnListar.Click += new System.EventHandler(this.btnListar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.Controls.Add(this.btnMinimizar);
            this.panel1.Controls.Add(this.btnFechar);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.picVoltar);
            this.panel1.Location = new System.Drawing.Point(-4, -3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(502, 128);
            this.panel1.TabIndex = 10;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.ForeColor = System.Drawing.Color.Black;
            this.btnMinimizar.Location = new System.Drawing.Point(439, 9);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(19, 26);
            this.btnMinimizar.TabIndex = 70;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.ForeColor = System.Drawing.Color.Black;
            this.btnFechar.Location = new System.Drawing.Point(462, 16);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(19, 23);
            this.btnFechar.TabIndex = 71;
            this.btnFechar.Text = "X";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(127, 12);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(269, 29);
            this.label28.TabIndex = 69;
            this.label28.Text = "Consultar Funcionário";
            // 
            // picVoltar
            // 
            this.picVoltar.BackColor = System.Drawing.Color.DarkRed;
            this.picVoltar.Image = global::The_Barber_s_House.Properties.Resources.Previous_free_vector_icons_designed_by_Gregor_Cresnar;
            this.picVoltar.Location = new System.Drawing.Point(28, 13);
            this.picVoltar.Name = "picVoltar";
            this.picVoltar.Size = new System.Drawing.Size(34, 32);
            this.picVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVoltar.TabIndex = 39;
            this.picVoltar.TabStop = false;
            this.picVoltar.Click += new System.EventHandler(this.picVoltar_Click);
            // 
            // IDF
            // 
            this.IDF.DataPropertyName = "id_funcionario";
            this.IDF.HeaderText = "ID do funcionário";
            this.IDF.Name = "IDF";
            this.IDF.ReadOnly = true;
            // 
            // detalhe
            // 
            this.detalhe.DataPropertyName = "nm_funcionario";
            this.detalhe.HeaderText = "Nome do Funcionário";
            this.detalhe.Name = "detalhe";
            this.detalhe.ReadOnly = true;
            // 
            // NomedaMãe
            // 
            this.NomedaMãe.DataPropertyName = "nm_mae";
            this.NomedaMãe.HeaderText = "Nome da Mãe";
            this.NomedaMãe.Name = "NomedaMãe";
            this.NomedaMãe.ReadOnly = true;
            // 
            // NomedoPai
            // 
            this.NomedoPai.DataPropertyName = "nm_pai";
            this.NomedoPai.HeaderText = "Nome do Pai";
            this.NomedoPai.Name = "NomedoPai";
            this.NomedoPai.ReadOnly = true;
            // 
            // nm_empresa
            // 
            this.nm_empresa.DataPropertyName = "nm_empresa";
            this.nm_empresa.HeaderText = "Empresa";
            this.nm_empresa.Name = "nm_empresa";
            this.nm_empresa.ReadOnly = true;
            // 
            // bt_urgente
            // 
            this.bt_urgente.DataPropertyName = "ds_genero";
            this.bt_urgente.HeaderText = "Gênero";
            this.bt_urgente.Name = "bt_urgente";
            this.bt_urgente.ReadOnly = true;
            // 
            // vl_inss
            // 
            this.vl_inss.DataPropertyName = "vl_inss";
            this.vl_inss.HeaderText = "INSS";
            this.vl_inss.Name = "vl_inss";
            this.vl_inss.ReadOnly = true;
            // 
            // vl_irrf
            // 
            this.vl_irrf.DataPropertyName = "vl_irrf";
            this.vl_irrf.HeaderText = "IRRF";
            this.vl_irrf.Name = "vl_irrf";
            this.vl_irrf.ReadOnly = true;
            // 
            // vl_salario_familia
            // 
            this.vl_salario_familia.DataPropertyName = "vl_salario_familia";
            this.vl_salario_familia.HeaderText = "Salário Familia";
            this.vl_salario_familia.Name = "vl_salario_familia";
            this.vl_salario_familia.ReadOnly = true;
            // 
            // vl_periculosidade
            // 
            this.vl_periculosidade.DataPropertyName = "vl_periculosidade";
            this.vl_periculosidade.HeaderText = "Periculosidade";
            this.vl_periculosidade.Name = "vl_periculosidade";
            this.vl_periculosidade.ReadOnly = true;
            // 
            // vl_insalubridade
            // 
            this.vl_insalubridade.DataPropertyName = "vl_insalubridade";
            this.vl_insalubridade.HeaderText = "Insalubridade";
            this.vl_insalubridade.Name = "vl_insalubridade";
            this.vl_insalubridade.ReadOnly = true;
            // 
            // dt_nascimento
            // 
            this.dt_nascimento.DataPropertyName = "dt_nascimento";
            this.dt_nascimento.HeaderText = "Data de nascimento";
            this.dt_nascimento.Name = "dt_nascimento";
            this.dt_nascimento.ReadOnly = true;
            // 
            // dt_adimissão
            // 
            this.dt_adimissão.DataPropertyName = "dt_adimissao";
            this.dt_adimissão.HeaderText = "Data de Admissão";
            this.dt_adimissão.Name = "dt_adimissão";
            this.dt_adimissão.ReadOnly = true;
            // 
            // vl_salario
            // 
            this.vl_salario.DataPropertyName = "vl_salario_bruto";
            this.vl_salario.HeaderText = "Salário";
            this.vl_salario.Name = "vl_salario";
            this.vl_salario.ReadOnly = true;
            // 
            // ds_telefone
            // 
            this.ds_telefone.DataPropertyName = "ds_telefone";
            this.ds_telefone.HeaderText = "Telefone";
            this.ds_telefone.Name = "ds_telefone";
            this.ds_telefone.ReadOnly = true;
            // 
            // TelefoneEmergencia
            // 
            this.TelefoneEmergencia.DataPropertyName = "ds_telefone_de_emergencia";
            this.TelefoneEmergencia.HeaderText = "Telefone de Emergência";
            this.TelefoneEmergencia.Name = "TelefoneEmergencia";
            this.TelefoneEmergencia.ReadOnly = true;
            // 
            // ds_cpf
            // 
            this.ds_cpf.DataPropertyName = "ds_cpf";
            this.ds_cpf.HeaderText = "CPF";
            this.ds_cpf.Name = "ds_cpf";
            this.ds_cpf.ReadOnly = true;
            // 
            // RG
            // 
            this.RG.HeaderText = "RG";
            this.RG.Name = "RG";
            this.RG.ReadOnly = true;
            // 
            // ds_endereco
            // 
            this.ds_endereco.DataPropertyName = "ds_endereco";
            this.ds_endereco.HeaderText = "Endereço";
            this.ds_endereco.Name = "ds_endereco";
            this.ds_endereco.ReadOnly = true;
            // 
            // ds_email
            // 
            this.ds_email.DataPropertyName = "ds_email";
            this.ds_email.HeaderText = "E-mail";
            this.ds_email.Name = "ds_email";
            this.ds_email.ReadOnly = true;
            // 
            // id_vale_trans
            // 
            this.id_vale_trans.DataPropertyName = "id_vale_transporte";
            this.id_vale_trans.HeaderText = "ID vale transporte";
            this.id_vale_trans.Name = "id_vale_trans";
            this.id_vale_trans.ReadOnly = true;
            // 
            // IDvalealimentacao
            // 
            this.IDvalealimentacao.DataPropertyName = "id_vale_alimentacao";
            this.IDvalealimentacao.HeaderText = "id_vale_alimentacao";
            this.IDvalealimentacao.Name = "IDvalealimentacao";
            this.IDvalealimentacao.ReadOnly = true;
            // 
            // id_vale_refeicao
            // 
            this.id_vale_refeicao.DataPropertyName = "id_vale_refeicao";
            this.id_vale_refeicao.HeaderText = "ID do vale refeição";
            this.id_vale_refeicao.Name = "id_vale_refeicao";
            this.id_vale_refeicao.ReadOnly = true;
            // 
            // id_plano_de_saude
            // 
            this.id_plano_de_saude.DataPropertyName = "id_plano_de_saude";
            this.id_plano_de_saude.HeaderText = "Id do plano de saúde";
            this.id_plano_de_saude.Name = "id_plano_de_saude";
            this.id_plano_de_saude.ReadOnly = true;
            // 
            // id_plano_odontológico
            // 
            this.id_plano_odontológico.DataPropertyName = "id_plano_odontologico";
            this.id_plano_odontológico.HeaderText = "ID do plano odontológico";
            this.id_plano_odontológico.Name = "id_plano_odontológico";
            this.id_plano_odontológico.ReadOnly = true;
            // 
            // id_seguro_vida
            // 
            this.id_seguro_vida.DataPropertyName = "id_seguro_vida";
            this.id_seguro_vida.HeaderText = "Id do seguro de vida";
            this.id_seguro_vida.Name = "id_seguro_vida";
            this.id_seguro_vida.ReadOnly = true;
            // 
            // id_cargo
            // 
            this.id_cargo.DataPropertyName = "id_cargo";
            this.id_cargo.HeaderText = "Id do cargo";
            this.id_cargo.Name = "id_cargo";
            this.id_cargo.ReadOnly = true;
            // 
            // ConsultarFuncionário
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 336);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnListar);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ConsultarFuncionário";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConsultarFuncionário";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudIDFuncionário)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionarios)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.NumericUpDown nudIDFuncionário;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvFuncionarios;
        private System.Windows.Forms.Button btnListar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picVoltar;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDF;
        private System.Windows.Forms.DataGridViewTextBoxColumn detalhe;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomedaMãe;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomedoPai;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_empresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn bt_urgente;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_inss;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_irrf;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_salario_familia;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_periculosidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_insalubridade;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_nascimento;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_adimissão;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_salario;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelefoneEmergencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_cpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn RG;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_endereco;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_email;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_vale_trans;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDvalealimentacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_vale_refeicao;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_plano_de_saude;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_plano_odontológico;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_seguro_vida;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_cargo;
    }
}