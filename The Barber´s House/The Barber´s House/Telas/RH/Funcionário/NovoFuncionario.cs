﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Funcionário
{
    public partial class NovoFuncionário : Form
    {
        public NovoFuncionário()
        {
            InitializeComponent();
            Carregarcampos();
        }

        Database.Entity.tb_funcionario db = new Database.Entity.tb_funcionario();
        Business.RH.Vales.BusinessVA va = new Business.RH.Vales.BusinessVA();
        Business.RH.Vales.BusinessVR vr = new Business.RH.Vales.BusinessVR();
        Business.RH.Vales.BusinessVT vt = new Business.RH.Vales.BusinessVT();
        Business.RH.Planos.BusinessPO po = new Business.RH.Planos.BusinessPO();
        Business.RH.Planos.BusinessPS ps = new Business.RH.Planos.BusinessPS();
        Business.RH.Outros.BusinessCargos cargos = new Business.RH.Outros.BusinessCargos();
        Business.RH.Outros.BusinessSegurodeVida sv = new Business.RH.Outros.BusinessSegurodeVida();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();

                // Converte os itens do ComboBox para modelo:
                Database.Entity.tb_cargo cargo = cboCargo.SelectedItem as Database.Entity.tb_cargo;
                Database.Entity.tb_plano_de_saude planoSaude = cboPlanodeSaúde.SelectedItem as Database.Entity.tb_plano_de_saude;
                Database.Entity.tb_plano_odontologico planoOdontologico = cboPlanoOdontológico.SelectedItem as Database.Entity.tb_plano_odontologico;
                Database.Entity.tb_seguro_vida seguroVida = cboSeguroVida.SelectedItem as Database.Entity.tb_seguro_vida;

                // Funções para inserir os novos benefícios:
                InserirVA();
                InserirVR();
                InserirVT();

                // Funções para Consultar Benefícios inseridos:
                Database.Entity.tb_vale_alimentacao vaModel = va.ConsultarVa(txtcdva.Text, nudVLAlimentação.Value);
                Database.Entity.tb_vale_refeicao vrModel = vr.ConsultarVR(txtcdvr.Text, nudVLRefeição.Value);
                Database.Entity.tb_vale_transporte vtModel = vt.ConsultarVT(txtcdvl.Text, nudVLTransporte.Value);

                db.nm_funcionario = txtNFuncionário.Text;
                db.nm_empresa = txtNEmpresa.Text;
                db.ds_genero = cboGenero.Text;
                db.dt_nascimento = dtpDtNascimento.Value;
                db.ds_endereco = txtEndereço.Text;
                db.vl_comissao = nudComissão.Value;
                db.vl_periculosidade = nudPericulosidade.Value;
                db.vl_irrf = nudIRRF.Value;
                db.vl_inss = nudINSS.Value;
                db.vl_salario_bruto = nudSBruto.Value;
                db.vl_salario_familia = nudSFamilia.Value;
                db.id_vale_transporte = vtModel.id_vale_transporte;
                db.id_vale_alimentacao = vaModel.id_vale_alimentacao;
                db.id_vale_refeicao = vrModel.id_vale_refeicao;
                db.id_plano_de_saude = planoSaude.id_plano_de_saude;
                db.id_plano_odontologico = planoOdontologico.id_plano_odontologico;
                db.id_cargo = cargo.id_cargo;
                db.id_seguro_vida = seguroVida.id_seguro_vida;
                db.dt_adimissao = dtpAdimissao.Value;
                db.vl_insalubridade = nudInsalubridade.Value;

                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, true);
                if (txtTelefone.MaskFull == true && telefoneValido == true)
                {
                    db.ds_telefone = txtTelefone.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o telefone");
                }
                if (txtCPF.MaskFull == true)
                {
                    db.ds_cpf = txtCPF.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o cpf");
                }
                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                if (contem == true && txtEmail.Text != string.Empty && invalido == false)
                {
                    db.ds_email = txtEmail.Text;
                    Business.RH.BusinessFuncionário business = new Business.RH.BusinessFuncionário();
                    business.NovoFuncionário(db);

                    MessageBox.Show("Bem vindo a nossa empresa " + txtNFuncionário.Text);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }

        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void InserirVA()
        {
            Database.Entity.tb_vale_alimentacao valeAlimentacao = new Database.Entity.tb_vale_alimentacao();
            valeAlimentacao.ds_codigo_va = txtcdva.Text;
            valeAlimentacao.vl_vale_alimentacao = nudVLAlimentação.Value;

            va.InserirVA(valeAlimentacao);
        }

        private void InserirVR()
        {
            Database.Entity.tb_vale_refeicao valeRefeicao = new Database.Entity.tb_vale_refeicao();
            valeRefeicao.ds_codigo_vr = txtcdvr.Text.Trim();
            valeRefeicao.vl_vale_refeicao = nudVLRefeição.Value;
            vr.InserirVR(valeRefeicao);
        }

        private void InserirVT()
        {
            Database.Entity.tb_vale_transporte valeTransporte = new Database.Entity.tb_vale_transporte();
            valeTransporte.ds_codigo_bilhete_unico = txtcdvl.Text;
            valeTransporte.vl_vt = nudVLTransporte.Value;
            valeTransporte.ds_codigo_uso = "000000";

            vt.InserirVT(valeTransporte);
        }

        public void Carregarcampos()
        {
            // Cargo
            cboCargo.ValueMember = nameof(Database.Entity.tb_cargo.ds_cargo);
            cboCargo.DataSource = cargos.ConsultarTodos();

            // Plano Odontológico
            cboPlanoOdontológico.ValueMember = nameof(Database.Entity.tb_plano_odontologico.ds_plano_funcionario);
            cboPlanoOdontológico.DataSource = po.ListarTodos();

            // Plano de Saúde
            cboPlanodeSaúde.ValueMember = nameof(Database.Entity.tb_plano_de_saude.ds_plano_funcionario);
            cboPlanodeSaúde.DataSource = ps.ListarTodos();

            // Seguro de Vida
            cboSeguroVida.ValueMember = nameof(Database.Entity.tb_seguro_vida.ds_plano_funcionario);
            cboSeguroVida.DataSource = sv.ListarTodos();
        }

        private void nudSBruto_ValueChanged(object sender, EventArgs e)
        {
            decimal salario = nudSBruto.Value;

            nudVLTransporte.Value = salario * 0.06m;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();

                // Converte os itens do ComboBox para modelo:
                Database.Entity.tb_cargo cargo = cboCargo.SelectedItem as Database.Entity.tb_cargo;
                Database.Entity.tb_plano_de_saude planoSaude = cboPlanodeSaúde.SelectedItem as Database.Entity.tb_plano_de_saude;
                Database.Entity.tb_plano_odontologico planoOdontologico = cboPlanoOdontológico.SelectedItem as Database.Entity.tb_plano_odontologico;
                Database.Entity.tb_seguro_vida seguroVida = cboSeguroVida.SelectedItem as Database.Entity.tb_seguro_vida;

                // Funções para inserir os novos benefícios:
                InserirVA();
                InserirVR();
                InserirVT();

                // Funções para Consultar Benefícios inseridos:
                Database.Entity.tb_vale_alimentacao vaModel = va.ConsultarVa(txtcdva.Text, nudVLAlimentação.Value);
                Database.Entity.tb_vale_refeicao vrModel = vr.ConsultarVR(txtcdvr.Text, nudVLRefeição.Value);
                Database.Entity.tb_vale_transporte vtModel = vt.ConsultarVT(txtcdvl.Text, nudVLTransporte.Value);

                db.nm_funcionario = txtNFuncionário.Text;
                db.nm_empresa = txtNEmpresa.Text;
                db.ds_genero = cboGenero.Text;
                db.dt_nascimento = dtpDtNascimento.Value;
                db.ds_endereco = txtEndereço.Text;
                db.vl_comissao = nudComissão.Value;
                db.vl_periculosidade = nudPericulosidade.Value;
                db.vl_irrf = nudIRRF.Value;
                db.vl_inss = nudINSS.Value;
                db.vl_salario_bruto = nudSBruto.Value;
                db.vl_salario_familia = nudSFamilia.Value;
                db.id_vale_transporte = vtModel.id_vale_transporte;
                db.id_vale_alimentacao = vaModel.id_vale_alimentacao;
                db.id_vale_refeicao = vrModel.id_vale_refeicao;
                db.id_plano_de_saude = planoSaude.id_plano_de_saude;
                db.id_plano_odontologico = planoOdontologico.id_plano_odontologico;
                db.id_cargo = cargo.id_cargo;
                db.id_seguro_vida = seguroVida.id_seguro_vida;
                db.dt_adimissao = dtpAdimissao.Value;
                db.vl_insalubridade = nudInsalubridade.Value;

                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, true);
                if (txtTelefone.MaskFull == true && telefoneValido == true)
                {
                    db.ds_telefone = txtTelefone.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o telefone");
                }
                if (txtCPF.MaskFull == true)
                {
                    db.ds_cpf = txtCPF.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o cpf");
                }
                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                if (contem == true && txtEmail.Text != string.Empty && invalido == false)
                {
                    db.ds_email = txtEmail.Text;
                    Business.RH.BusinessFuncionário business = new Business.RH.BusinessFuncionário();
                    business.NovoFuncionário(db);

                    MessageBox.Show("Bem vindo a nossa empresa" + txtNFuncionário);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }
    }
}
