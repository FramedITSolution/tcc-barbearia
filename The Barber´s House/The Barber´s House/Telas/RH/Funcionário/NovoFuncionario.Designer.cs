﻿namespace The_Barber_s_House.Telas.RH.Funcionário
{
    partial class NovoFuncionário
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtNFuncionário = new System.Windows.Forms.TextBox();
            this.txtNEmpresa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudIRRF = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpDtNascimento = new System.Windows.Forms.DateTimePicker();
            this.nudINSS = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.nudComissão = new System.Windows.Forms.NumericUpDown();
            this.nudSFamilia = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.nudPericulosidade = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpAdimissao = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.nudSBruto = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.nudVLAlimentação = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.nudVLRefeição = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.nudVLTransporte = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.cboGenero = new System.Windows.Forms.ComboBox();
            this.txtcdvl = new System.Windows.Forms.TextBox();
            this.txtcdvr = new System.Windows.Forms.TextBox();
            this.txtcdva = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.picVoltar = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPai = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMãe = new System.Windows.Forms.TextBox();
            this.txtEndereço = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.cboSeguroVida = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cboPlanoOdontológico = new System.Windows.Forms.ComboBox();
            this.cboPlanodeSaúde = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.nudInsalubridade = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.mktRG = new System.Windows.Forms.MaskedTextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudIRRF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudINSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudComissão)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSFamilia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLAlimentação)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLRefeição)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLTransporte)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsalubridade)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome do Funcionário";
            // 
            // txtNFuncionário
            // 
            this.txtNFuncionário.Location = new System.Drawing.Point(144, 29);
            this.txtNFuncionário.Name = "txtNFuncionário";
            this.txtNFuncionário.Size = new System.Drawing.Size(121, 20);
            this.txtNFuncionário.TabIndex = 1;
            // 
            // txtNEmpresa
            // 
            this.txtNEmpresa.Location = new System.Drawing.Point(144, 163);
            this.txtNEmpresa.Name = "txtNEmpresa";
            this.txtNEmpresa.Size = new System.Drawing.Size(123, 20);
            this.txtNEmpresa.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nome da Empresa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gênero";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "IRRF";
            // 
            // nudIRRF
            // 
            this.nudIRRF.DecimalPlaces = 2;
            this.nudIRRF.Location = new System.Drawing.Point(144, 248);
            this.nudIRRF.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudIRRF.Name = "nudIRRF";
            this.nudIRRF.Size = new System.Drawing.Size(123, 20);
            this.nudIRRF.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 291);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Data de Nascimento";
            // 
            // dtpDtNascimento
            // 
            this.dtpDtNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDtNascimento.Location = new System.Drawing.Point(144, 284);
            this.dtpDtNascimento.Name = "dtpDtNascimento";
            this.dtpDtNascimento.Size = new System.Drawing.Size(123, 20);
            this.dtpDtNascimento.TabIndex = 10;
            this.dtpDtNascimento.Value = new System.DateTime(2000, 1, 1, 22, 54, 0, 0);
            // 
            // nudINSS
            // 
            this.nudINSS.DecimalPlaces = 2;
            this.nudINSS.Location = new System.Drawing.Point(144, 329);
            this.nudINSS.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudINSS.Name = "nudINSS";
            this.nudINSS.Size = new System.Drawing.Size(121, 20);
            this.nudINSS.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 332);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "INSS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(285, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Comissão";
            // 
            // nudComissão
            // 
            this.nudComissão.DecimalPlaces = 2;
            this.nudComissão.Location = new System.Drawing.Point(403, 31);
            this.nudComissão.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudComissão.Name = "nudComissão";
            this.nudComissão.Size = new System.Drawing.Size(123, 20);
            this.nudComissão.TabIndex = 14;
            // 
            // nudSFamilia
            // 
            this.nudSFamilia.DecimalPlaces = 2;
            this.nudSFamilia.Location = new System.Drawing.Point(662, 289);
            this.nudSFamilia.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudSFamilia.Name = "nudSFamilia";
            this.nudSFamilia.Size = new System.Drawing.Size(142, 20);
            this.nudSFamilia.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(544, 291);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Salário Família";
            // 
            // nudPericulosidade
            // 
            this.nudPericulosidade.DecimalPlaces = 2;
            this.nudPericulosidade.Location = new System.Drawing.Point(403, 164);
            this.nudPericulosidade.Name = "nudPericulosidade";
            this.nudPericulosidade.Size = new System.Drawing.Size(123, 20);
            this.nudPericulosidade.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(285, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Periculosidade";
            // 
            // dtpAdimissao
            // 
            this.dtpAdimissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdimissao.Location = new System.Drawing.Point(403, 245);
            this.dtpAdimissao.Name = "dtpAdimissao";
            this.dtpAdimissao.Size = new System.Drawing.Size(123, 20);
            this.dtpAdimissao.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(285, 251);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Data de Admissão";
            // 
            // nudSBruto
            // 
            this.nudSBruto.DecimalPlaces = 2;
            this.nudSBruto.Location = new System.Drawing.Point(403, 289);
            this.nudSBruto.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudSBruto.Name = "nudSBruto";
            this.nudSBruto.Size = new System.Drawing.Size(123, 20);
            this.nudSBruto.TabIndex = 24;
            this.nudSBruto.ValueChanged += new System.EventHandler(this.nudSBruto_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(285, 291);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Salário Bruto";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(285, 332);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Telefone";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(285, 77);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "CPF";
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(403, 74);
            this.txtCPF.Mask = "###.###.###-##";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(123, 20);
            this.txtCPF.TabIndex = 30;
            // 
            // nudVLAlimentação
            // 
            this.nudVLAlimentação.DecimalPlaces = 2;
            this.nudVLAlimentação.Location = new System.Drawing.Point(662, 204);
            this.nudVLAlimentação.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudVLAlimentação.Name = "nudVLAlimentação";
            this.nudVLAlimentação.Size = new System.Drawing.Size(54, 20);
            this.nudVLAlimentação.TabIndex = 40;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(542, 207);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 39;
            this.label17.Text = "Vale Alimentação";
            // 
            // nudVLRefeição
            // 
            this.nudVLRefeição.DecimalPlaces = 2;
            this.nudVLRefeição.Location = new System.Drawing.Point(662, 164);
            this.nudVLRefeição.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudVLRefeição.Name = "nudVLRefeição";
            this.nudVLRefeição.Size = new System.Drawing.Size(54, 20);
            this.nudVLRefeição.TabIndex = 38;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(542, 166);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 37;
            this.label18.Text = "Vale Refeição";
            // 
            // nudVLTransporte
            // 
            this.nudVLTransporte.DecimalPlaces = 2;
            this.nudVLTransporte.Location = new System.Drawing.Point(660, 121);
            this.nudVLTransporte.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudVLTransporte.Name = "nudVLTransporte";
            this.nudVLTransporte.Size = new System.Drawing.Size(54, 20);
            this.nudVLTransporte.TabIndex = 36;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(542, 123);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(97, 13);
            this.label19.TabIndex = 35;
            this.label19.Text = "Vale Transporte";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(542, 78);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(118, 13);
            this.label20.TabIndex = 43;
            this.label20.Text = "Plano Odontológico";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(542, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 13);
            this.label21.TabIndex = 41;
            this.label21.Text = "Plano de Saúde";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 380);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 45;
            this.label22.Text = "Cargo";
            // 
            // cboCargo
            // 
            this.cboCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Location = new System.Drawing.Point(144, 377);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(123, 21);
            this.cboCargo.TabIndex = 46;
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.DarkRed;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.ForeColor = System.Drawing.Color.White;
            this.btnSalvar.Location = new System.Drawing.Point(17, 462);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(773, 27);
            this.btnSalvar.TabIndex = 49;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            this.btnSalvar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnSalvar_KeyPress);
            // 
            // cboGenero
            // 
            this.cboGenero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGenero.FormattingEnabled = true;
            this.cboGenero.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.cboGenero.Location = new System.Drawing.Point(144, 203);
            this.cboGenero.Name = "cboGenero";
            this.cboGenero.Size = new System.Drawing.Size(123, 21);
            this.cboGenero.TabIndex = 50;
            // 
            // txtcdvl
            // 
            this.txtcdvl.Location = new System.Drawing.Point(720, 120);
            this.txtcdvl.Name = "txtcdvl";
            this.txtcdvl.Size = new System.Drawing.Size(84, 20);
            this.txtcdvl.TabIndex = 53;
            // 
            // txtcdvr
            // 
            this.txtcdvr.Location = new System.Drawing.Point(720, 163);
            this.txtcdvr.Name = "txtcdvr";
            this.txtcdvr.Size = new System.Drawing.Size(84, 20);
            this.txtcdvr.TabIndex = 54;
            // 
            // txtcdva
            // 
            this.txtcdva.Location = new System.Drawing.Point(722, 203);
            this.txtcdva.Name = "txtcdva";
            this.txtcdva.Size = new System.Drawing.Size(82, 20);
            this.txtcdva.TabIndex = 55;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkRed;
            this.panel1.Controls.Add(this.btnMinimizar);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.btnFechar);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.picVoltar);
            this.panel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(843, 195);
            this.panel1.TabIndex = 56;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.ForeColor = System.Drawing.Color.Black;
            this.btnMinimizar.Location = new System.Drawing.Point(782, 6);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(19, 26);
            this.btnMinimizar.TabIndex = 67;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(400, 82);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(19, 26);
            this.button2.TabIndex = 61;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnFechar
            // 
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.ForeColor = System.Drawing.Color.Black;
            this.btnFechar.Location = new System.Drawing.Point(807, 11);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(19, 23);
            this.btnFechar.TabIndex = 68;
            this.btnFechar.Text = "X";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(423, 89);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(19, 23);
            this.button1.TabIndex = 62;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(316, 11);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(219, 29);
            this.label28.TabIndex = 60;
            this.label28.Text = "Novo Funcionário";
            // 
            // picVoltar
            // 
            this.picVoltar.BackColor = System.Drawing.Color.DarkRed;
            this.picVoltar.Image = global::The_Barber_s_House.Properties.Resources.Previous_free_vector_icons_designed_by_Gregor_Cresnar;
            this.picVoltar.Location = new System.Drawing.Point(16, 11);
            this.picVoltar.Name = "picVoltar";
            this.picVoltar.Size = new System.Drawing.Size(34, 32);
            this.picVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVoltar.TabIndex = 57;
            this.picVoltar.TabStop = false;
            this.picVoltar.Click += new System.EventHandler(this.picVoltar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.maskedTextBox1);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtEmail);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.mktRG);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.txtPai);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtMãe);
            this.groupBox1.Controls.Add(this.txtEndereço);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtTelefone);
            this.groupBox1.Controls.Add(this.cboSeguroVida);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.cboPlanoOdontológico);
            this.groupBox1.Controls.Add(this.cboPlanodeSaúde);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.nudInsalubridade);
            this.groupBox1.Controls.Add(this.nudComissão);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtcdva);
            this.groupBox1.Controls.Add(this.txtNFuncionário);
            this.groupBox1.Controls.Add(this.txtcdvr);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtcdvl);
            this.groupBox1.Controls.Add(this.txtNEmpresa);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboGenero);
            this.groupBox1.Controls.Add(this.nudIRRF);
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.dtpDtNascimento);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.cboCargo);
            this.groupBox1.Controls.Add(this.nudINSS);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.nudSFamilia);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.nudPericulosidade);
            this.groupBox1.Controls.Add(this.nudVLAlimentação);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.dtpAdimissao);
            this.groupBox1.Controls.Add(this.nudVLRefeição);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.nudSBruto);
            this.groupBox1.Controls.Add(this.nudVLTransporte);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.txtCPF);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 49);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(810, 505);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(14, 123);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 13);
            this.label25.TabIndex = 73;
            this.label25.Text = "Nome do Pai";
            // 
            // txtPai
            // 
            this.txtPai.Location = new System.Drawing.Point(144, 120);
            this.txtPai.Name = "txtPai";
            this.txtPai.Size = new System.Drawing.Size(123, 20);
            this.txtPai.TabIndex = 74;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(14, 77);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 71;
            this.label10.Text = "Nome da Mãe";
            // 
            // txtMãe
            // 
            this.txtMãe.Location = new System.Drawing.Point(144, 73);
            this.txtMãe.Name = "txtMãe";
            this.txtMãe.Size = new System.Drawing.Size(123, 20);
            this.txtMãe.TabIndex = 72;
            // 
            // txtEndereço
            // 
            this.txtEndereço.Location = new System.Drawing.Point(660, 329);
            this.txtEndereço.Multiline = true;
            this.txtEndereço.Name = "txtEndereço";
            this.txtEndereço.Size = new System.Drawing.Size(144, 65);
            this.txtEndereço.TabIndex = 64;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(542, 332);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 63;
            this.label15.Text = "Endereço";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(403, 329);
            this.txtTelefone.Mask = "(##) #####-####";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(123, 20);
            this.txtTelefone.TabIndex = 62;
            // 
            // cboSeguroVida
            // 
            this.cboSeguroVida.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSeguroVida.FormattingEnabled = true;
            this.cboSeguroVida.Location = new System.Drawing.Point(662, 248);
            this.cboSeguroVida.Name = "cboSeguroVida";
            this.cboSeguroVida.Size = new System.Drawing.Size(142, 21);
            this.cboSeguroVida.TabIndex = 61;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(542, 251);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(93, 13);
            this.label24.TabIndex = 60;
            this.label24.Text = "Seguro de vida";
            // 
            // cboPlanoOdontológico
            // 
            this.cboPlanoOdontológico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPlanoOdontológico.FormattingEnabled = true;
            this.cboPlanoOdontológico.Location = new System.Drawing.Point(662, 74);
            this.cboPlanoOdontológico.Name = "cboPlanoOdontológico";
            this.cboPlanoOdontológico.Size = new System.Drawing.Size(142, 21);
            this.cboPlanoOdontológico.TabIndex = 59;
            // 
            // cboPlanodeSaúde
            // 
            this.cboPlanodeSaúde.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPlanodeSaúde.FormattingEnabled = true;
            this.cboPlanodeSaúde.Location = new System.Drawing.Point(662, 29);
            this.cboPlanodeSaúde.Name = "cboPlanodeSaúde";
            this.cboPlanodeSaúde.Size = new System.Drawing.Size(142, 21);
            this.cboPlanodeSaúde.TabIndex = 58;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(285, 206);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(83, 13);
            this.label23.TabIndex = 56;
            this.label23.Text = "Insalubridade";
            // 
            // nudInsalubridade
            // 
            this.nudInsalubridade.DecimalPlaces = 2;
            this.nudInsalubridade.Location = new System.Drawing.Point(403, 204);
            this.nudInsalubridade.Name = "nudInsalubridade";
            this.nudInsalubridade.Size = new System.Drawing.Size(123, 20);
            this.nudInsalubridade.TabIndex = 57;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(285, 123);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(88, 13);
            this.label26.TabIndex = 77;
            this.label26.Text = "Registro Geral";
            // 
            // mktRG
            // 
            this.mktRG.Location = new System.Drawing.Point(403, 120);
            this.mktRG.Mask = "##.###.###-#";
            this.mktRG.Name = "mktRG";
            this.mktRG.Size = new System.Drawing.Size(123, 20);
            this.mktRG.TabIndex = 78;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(403, 418);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(123, 20);
            this.txtEmail.TabIndex = 80;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(285, 421);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 79;
            this.label16.Text = "E-mail";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(403, 377);
            this.maskedTextBox1.Mask = "(##) #####-####";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(123, 20);
            this.maskedTextBox1.TabIndex = 82;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(285, 380);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 13);
            this.label27.TabIndex = 81;
            this.label27.Text = "Telefone Emer.";
            // 
            // NovoFuncionário
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 550);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NovoFuncionário";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NovoFuncionario";
            ((System.ComponentModel.ISupportInitialize)(this.nudIRRF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudINSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudComissão)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSFamilia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLAlimentação)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLRefeição)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLTransporte)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsalubridade)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNFuncionário;
        private System.Windows.Forms.TextBox txtNEmpresa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudIRRF;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpDtNascimento;
        private System.Windows.Forms.NumericUpDown nudINSS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudComissão;
        private System.Windows.Forms.NumericUpDown nudSFamilia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudPericulosidade;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpAdimissao;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown nudSBruto;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.NumericUpDown nudVLAlimentação;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown nudVLRefeição;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown nudVLTransporte;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.ComboBox cboGenero;
        private System.Windows.Forms.TextBox txtcdvl;
        private System.Windows.Forms.TextBox txtcdvr;
        private System.Windows.Forms.TextBox txtcdva;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown nudInsalubridade;
        private System.Windows.Forms.ComboBox cboSeguroVida;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cboPlanoOdontológico;
        private System.Windows.Forms.ComboBox cboPlanodeSaúde;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.TextBox txtEndereço;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox picVoltar;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtPai;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMãe;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.MaskedTextBox mktRG;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label16;
    }
}