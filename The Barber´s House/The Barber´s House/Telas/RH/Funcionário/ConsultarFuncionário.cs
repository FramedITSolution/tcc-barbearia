﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Funcionário
{
    public partial class ConsultarFuncionário : Form
    {
        public ConsultarFuncionário()
        {
            InitializeComponent();
            dgvFuncionarios.AutoGenerateColumns = false;
        }

        Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvFuncionarios.DataSource = func.ListarFuncionarios();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudIDFuncionário.Value);
                dgvFuncionarios.DataSource = func.ConsultarPorID(id);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu start = new Telas.Outros.Menu();
            start.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu start = new Telas.Outros.Menu();
            start.Show();
        }
    }
}
