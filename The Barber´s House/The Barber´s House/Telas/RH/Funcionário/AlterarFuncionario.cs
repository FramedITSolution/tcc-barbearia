﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Funcionário
{
    public partial class AlterarFuncionario : Form
    {
        public AlterarFuncionario()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();
        Business.RH.Planos.BusinessPO po = new Business.RH.Planos.BusinessPO();
        Business.RH.Planos.BusinessPS ps = new Business.RH.Planos.BusinessPS();
        Business.RH.Outros.BusinessSegurodeVida sv = new Business.RH.Outros.BusinessSegurodeVida();
        Business.RH.Outros.BusinessCargos cargos = new Business.RH.Outros.BusinessCargos();
        Business.RH.Vales.BusinessVA va = new Business.RH.Vales.BusinessVA();
        Business.RH.Vales.BusinessVR vr = new Business.RH.Vales.BusinessVR();
        Business.RH.Vales.BusinessVT vt = new Business.RH.Vales.BusinessVT();


        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();

                // Conversão dos itens do combo para modelo
                Database.Entity.tb_plano_de_saude saudemodel = cboPlanodeSaúde.SelectedItem as Database.Entity.tb_plano_de_saude;
                Database.Entity.tb_plano_odontologico odontomodel = cboPlanoOdontológico.SelectedItem as Database.Entity.tb_plano_odontologico;
                Database.Entity.tb_seguro_vida segurovidamodel = cboSeguroVida.SelectedItem as Database.Entity.tb_seguro_vida;
                Database.Entity.tb_cargo cargomodel = cboCargo.SelectedItem as Database.Entity.tb_cargo;
                Database.Entity.tb_funcionario campos = cboFuncionario.SelectedItem as Database.Entity.tb_funcionario;                

                Database.Entity.tb_funcionario model = new Database.Entity.tb_funcionario();
                // Carregando o modelo com os valores da tela
                model.nm_funcionario = cboFuncionario.Text;
                model.nm_mae = txtMãe.Text;
                model.nm_pai = txtPai.Text;
                model.nm_empresa = txtNEmpresa.Text;
                model.ds_endereco = txtEndereço.Text;
                if (cboGenero.Text == "Masculino")
                {
                    model.ds_genero = "M";
                }
                if (cboGenero.Text == "Feminino")
                {
                    model.ds_genero = "F";
                }
                model.dt_nascimento = dtpDtNascimento.Value.Date;
                model.dt_adimissao = dtpAdimissao.Value.Date;
                model.id_vale_alimentacao = campos.id_vale_alimentacao;
                model.id_vale_refeicao = campos.id_vale_refeicao;
                model.id_vale_transporte = campos.id_vale_transporte;
                model.id_cargo = cargomodel.id_cargo;
                model.id_plano_de_saude = saudemodel.id_plano_de_saude;
                model.id_plano_odontologico = odontomodel.id_plano_odontologico;
                model.id_seguro_vida = segurovidamodel.id_seguro_vida;
                model.vl_comissao = nudComissão.Value;
                model.vl_insalubridade = nudInsalubridade.Value;
                model.vl_periculosidade = nudPericulosidade.Value;
                model.vl_inss = nudINSS.Value;
                model.vl_irrf = nudIRRF.Value;
                model.vl_salario_bruto = nudSBruto.Value;
                model.vl_salario_familia = nudSFamilia.Value;
                model.vl_salario_bruto = nudSBruto.Value;

                // Alteração do Vale ALimentação
                Database.Entity.tb_vale_alimentacao vaModel = new Database.Entity.tb_vale_alimentacao();
                vaModel.id_vale_alimentacao = model.id_vale_alimentacao;
                vaModel.ds_codigo_va = txtcdva.Text;
                vaModel.vl_vale_alimentacao = nudVLAlimentação.Value;
                va.AlterarVA(vaModel);

                // Alteração do Vale Refeição
                Database.Entity.tb_vale_refeicao vrModel = new Database.Entity.tb_vale_refeicao();
                vrModel.id_vale_refeicao = model.id_vale_refeicao;
                vrModel.ds_codigo_vr = txtcdvr.Text;
                vrModel.vl_vale_refeicao = nudVLRefeição.Value;
                vr.AlterarVR(vrModel);

                // Alteração do Vale Transporte
                Database.Entity.tb_vale_transporte vtModel = new Database.Entity.tb_vale_transporte();
                vtModel.id_vale_transporte = model.id_vale_transporte;
                vtModel.ds_codigo_bilhete_unico = txtcdvl.Text;
                vtModel.vl_vt = nudVLTransporte.Value;
                vt.AlterarVT(vtModel);

                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, true);
                bool telefonemergValido = validar.ValidarTelefone(mktTelefoneEmerg.Text, true);
                if (txtTelefone.MaskFull == false 
                    || mktTelefoneEmerg.MaskFull == false 
                    || telefoneValido == false
                    || telefonemergValido == false)
                {
                    MessageBox.Show("Preencha o telefone/telefone de emergência corretamente");
                }

                if (txtCPF.MaskFull == false)
                {
                    MessageBox.Show("Preencha o cpf");
                }

                if (mktRG.MaskFull == false)
                {
                    MessageBox.Show("Preencha o RG");
                }

                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);

                if(contem == false || invalido == false)
                {
                    MessageBox.Show("Preencha o email corretamente");

                }
                if (txtTelefone.MaskFull == true
                    && mktTelefoneEmerg.MaskFull == true
                    && telefoneValido == true
                    && telefonemergValido == true
                    && txtCPF.MaskFull == true
                    && mktRG.MaskFull == true
                    && contem == true 
                    && txtEmail.Text != string.Empty 
                    && invalido == false)
                {
                    model.ds_telefone = txtTelefone.Text;
                    model.ds_telefone_de_emergencia = mktTelefoneEmerg.Text;
                    model.ds_cpf = txtCPF.Text;
                    model.ds_rg = mktRG.Text;
                    model.ds_email = txtEmail.Text;
                    func.AlterarFuncionario(model);
                }               

                MessageBox.Show("Alteração concluída com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void CarregarCombo()
        {
            // Funcionário
            cboFuncionario.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboFuncionario.DataSource = func.ListarFuncionarios();

            //Gênero
            cboGenero.DisplayMember = nameof(Database.Entity.tb_funcionario.ds_genero);
            cboFuncionario.DataSource = func.ListarFuncionarios();

            // Cargo
            cboCargo.ValueMember = nameof(Database.Entity.tb_cargo.ds_cargo);
            cboCargo.DataSource = cargos.ConsultarTodos();

            // Plano Odontológico
            cboPlanoOdontológico.ValueMember = nameof(Database.Entity.tb_plano_odontologico.ds_plano_funcionario);
            cboPlanoOdontológico.DataSource = po.ListarTodos();

            // Plano de Saúde
            cboPlanodeSaúde.ValueMember = nameof(Database.Entity.tb_plano_de_saude.ds_plano_funcionario);
            cboPlanodeSaúde.DataSource = ps.ListarTodos();

            // Seguro de Vida
            cboSeguroVida.ValueMember = nameof(Database.Entity.tb_seguro_vida.ds_plano_funcionario);
            cboSeguroVida.DataSource = sv.ListarTodos();
        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Conversão dos itens do combo para modelo
                Database.Entity.tb_funcionario model = cboFuncionario.SelectedItem as Database.Entity.tb_funcionario;

                // Consultas que carregam o valor do vales
                Database.Entity.tb_vale_alimentacao vaModel = va.ConsultarPorId(model.id_vale_alimentacao);
                Database.Entity.tb_vale_refeicao vrModel = vr.ConsultarPorId(model.id_vale_refeicao);
                Database.Entity.tb_vale_transporte vtModel = vt.ConsultarPorId(model.id_vale_transporte);

                // Carregando os campos apartir do funcionário selecionado
                nudComissão.Value = Convert.ToDecimal(model.vl_comissao);
                nudINSS.Value = Convert.ToDecimal(model.vl_inss);
                nudIRRF.Value = Convert.ToDecimal(model.vl_irrf);
                nudSFamilia.Value = Convert.ToDecimal(model.vl_salario_familia);
                nudSBruto.Value = Convert.ToDecimal(model.vl_salario_bruto);
                nudVLAlimentação.Value = Convert.ToDecimal(vaModel.vl_vale_alimentacao);
                txtcdva.Text = vaModel.ds_codigo_va;
                nudVLRefeição.Value = Convert.ToDecimal(vrModel.vl_vale_refeicao);
                txtcdvr.Text = vrModel.ds_codigo_vr;
                nudVLTransporte.Value = Convert.ToDecimal(vtModel.vl_vt);
                txtcdvl.Text = vtModel.ds_codigo_bilhete_unico;
                nudInsalubridade.Value = Convert.ToDecimal(model.vl_insalubridade);
                nudPericulosidade.Value = Convert.ToDecimal(model.vl_periculosidade);
                txtMãe.Text = model.nm_mae;
                txtPai.Text = model.nm_pai;
                txtCPF.Text = model.ds_cpf;
                txtEmail.Text = model.ds_email;
                txtEndereço.Text = model.ds_endereco;
                txtNEmpresa.Text = model.nm_empresa;
                txtTelefone.Text = model.ds_telefone;
                mktTelefoneEmerg.Text = model.ds_telefone_de_emergencia;
                mktRG.Text = model.ds_rg;
                dtpDtNascimento.Value = model.dt_nascimento.Date;
                dtpAdimissao.Value = model.dt_adimissao.Date;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void LblMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();

                // Conversão dos itens do combo para modelo
                Database.Entity.tb_plano_de_saude saudemodel = cboPlanodeSaúde.SelectedItem as Database.Entity.tb_plano_de_saude;
                Database.Entity.tb_plano_odontologico odontomodel = cboPlanoOdontológico.SelectedItem as Database.Entity.tb_plano_odontologico;
                Database.Entity.tb_seguro_vida segurovidamodel = cboSeguroVida.SelectedItem as Database.Entity.tb_seguro_vida;
                Database.Entity.tb_cargo cargomodel = cboCargo.SelectedItem as Database.Entity.tb_cargo;
                Database.Entity.tb_funcionario campos = cboFuncionario.SelectedItem as Database.Entity.tb_funcionario;

                Database.Entity.tb_funcionario model = new Database.Entity.tb_funcionario();
                // Carregando o modelo com os valores da tela
                model.nm_funcionario = cboFuncionario.Text;
                model.nm_empresa = txtNEmpresa.Text;
                model.ds_endereco = txtEndereço.Text;
                if (cboGenero.Text == "Masculino")
                {
                    model.ds_genero = "M";
                }
                if (cboGenero.Text == "Feminino")
                {
                    model.ds_genero = "F";
                }
                model.dt_nascimento = dtpDtNascimento.Value.Date;
                model.dt_adimissao = dtpAdimissao.Value.Date;
                model.id_vale_alimentacao = campos.id_vale_alimentacao;
                model.id_vale_refeicao = campos.id_vale_refeicao;
                model.id_vale_transporte = campos.id_vale_transporte;
                model.id_cargo = cargomodel.id_cargo;
                model.id_plano_de_saude = saudemodel.id_plano_de_saude;
                model.id_plano_odontologico = odontomodel.id_plano_odontologico;
                model.id_seguro_vida = segurovidamodel.id_seguro_vida;
                model.vl_comissao = nudComissão.Value;
                model.vl_insalubridade = nudInsalubridade.Value;
                model.vl_periculosidade = nudPericulosidade.Value;
                model.vl_inss = nudINSS.Value;
                model.vl_irrf = nudIRRF.Value;
                model.vl_salario_bruto = nudSBruto.Value;
                model.vl_salario_familia = nudSFamilia.Value;
                model.vl_salario_bruto = nudSBruto.Value;

                // Alteração do Vale ALimentação
                Database.Entity.tb_vale_alimentacao vaModel = new Database.Entity.tb_vale_alimentacao();
                vaModel.id_vale_alimentacao = model.id_vale_alimentacao;
                vaModel.ds_codigo_va = txtcdva.Text;
                vaModel.vl_vale_alimentacao = nudVLAlimentação.Value;
                va.AlterarVA(vaModel);

                // Alteração do Vale Refeição
                Database.Entity.tb_vale_refeicao vrModel = new Database.Entity.tb_vale_refeicao();
                vrModel.id_vale_refeicao = model.id_vale_refeicao;
                vrModel.ds_codigo_vr = txtcdvr.Text;
                vrModel.vl_vale_refeicao = nudVLRefeição.Value;
                vr.AlterarVR(vrModel);

                // Alteração do Vale Transporte
                Database.Entity.tb_vale_transporte vtModel = new Database.Entity.tb_vale_transporte();
                vtModel.id_vale_transporte = model.id_vale_transporte;
                vtModel.ds_codigo_bilhete_unico = txtcdvl.Text;
                vtModel.vl_vt = nudVLTransporte.Value;
                vt.AlterarVT(vtModel);

                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, true);
                if (txtTelefone.MaskFull == true && telefoneValido == true)
                {
                    model.ds_telefone = txtTelefone.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o telefone");
                }
                if (txtCPF.MaskFull == true)
                {
                    model.ds_cpf = txtCPF.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o cpf");
                }
                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                if (contem == true && txtEmail.Text != string.Empty && invalido == false)
                {
                    model.ds_email = txtEmail.Text;
                    func.AlterarFuncionario(model);
                }

                MessageBox.Show("Alteração concluída com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
    }
}
