﻿namespace The_Barber_s_House.Telas.RH.Funcionário
{
    partial class RemoverFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtcdva = new System.Windows.Forms.TextBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnFechar = new System.Windows.Forms.Button();
            this.picVoltar = new System.Windows.Forms.PictureBox();
            this.mktTelefoneEmerg = new System.Windows.Forms.MaskedTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.mktRG = new System.Windows.Forms.MaskedTextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtPai = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMãe = new System.Windows.Forms.TextBox();
            this.txtEndereço = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.cboSeguroVida = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cboPlanoOdontológico = new System.Windows.Forms.ComboBox();
            this.cboPlanodeSaúde = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.nudInsalubridade = new System.Windows.Forms.NumericUpDown();
            this.nudComissão = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNEmpresa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboGenero = new System.Windows.Forms.ComboBox();
            this.nudIRRF = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.dtpDtNascimento = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.nudINSS = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.nudSFamilia = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.nudPericulosidade = new System.Windows.Forms.NumericUpDown();
            this.nudVLAlimentação = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpAdimissao = new System.Windows.Forms.DateTimePicker();
            this.nudVLRefeição = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.nudSBruto = new System.Windows.Forms.NumericUpDown();
            this.nudVLTransporte = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtcdvr = new System.Windows.Forms.TextBox();
            this.txtcdvl = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsalubridade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudComissão)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIRRF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudINSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSFamilia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLAlimentação)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLRefeição)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLTransporte)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox2.Controls.Add(this.mktTelefoneEmerg);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.txtEmail);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.mktRG);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.txtPai);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtMãe);
            this.groupBox2.Controls.Add(this.txtEndereço);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtTelefone);
            this.groupBox2.Controls.Add(this.cboSeguroVida);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.cboPlanoOdontológico);
            this.groupBox2.Controls.Add(this.cboPlanodeSaúde);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.nudInsalubridade);
            this.groupBox2.Controls.Add(this.nudComissão);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtNEmpresa);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cboGenero);
            this.groupBox2.Controls.Add(this.nudIRRF);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.dtpDtNascimento);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cboCargo);
            this.groupBox2.Controls.Add(this.nudINSS);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.nudSFamilia);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.nudPericulosidade);
            this.groupBox2.Controls.Add(this.nudVLAlimentação);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.dtpAdimissao);
            this.groupBox2.Controls.Add(this.nudVLRefeição);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.nudSBruto);
            this.groupBox2.Controls.Add(this.nudVLTransporte);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.txtCPF);
            this.groupBox2.Controls.Add(this.txtcdva);
            this.groupBox2.Controls.Add(this.txtcdvr);
            this.groupBox2.Controls.Add(this.txtcdvl);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(21, 110);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(819, 430);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Vefique os valores";
            // 
            // txtcdva
            // 
            this.txtcdva.Enabled = false;
            this.txtcdva.Location = new System.Drawing.Point(719, 206);
            this.txtcdva.Name = "txtcdva";
            this.txtcdva.Size = new System.Drawing.Size(84, 20);
            this.txtcdva.TabIndex = 93;
            // 
            // btnSalvar
            // 
            this.btnSalvar.BackColor = System.Drawing.Color.Maroon;
            this.btnSalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalvar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalvar.ForeColor = System.Drawing.Color.White;
            this.btnSalvar.Location = new System.Drawing.Point(21, 546);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(820, 23);
            this.btnSalvar.TabIndex = 19;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = false;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            this.btnSalvar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnSalvar_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Maroon;
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.btnMinimizar);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btnFechar);
            this.panel1.Controls.Add(this.picVoltar);
            this.panel1.Location = new System.Drawing.Point(0, -2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(859, 163);
            this.panel1.TabIndex = 17;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(327, 8);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(263, 29);
            this.label28.TabIndex = 41;
            this.label28.Text = "Remover Funcionário";
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.Location = new System.Drawing.Point(796, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(19, 26);
            this.btnMinimizar.TabIndex = 39;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Window;
            this.groupBox1.Controls.Add(this.cboFuncionario);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(21, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(820, 63);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selecione um funcionário";
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(378, 24);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(170, 21);
            this.cboFuncionario.TabIndex = 3;
            this.cboFuncionario.SelectedIndexChanged += new System.EventHandler(this.cboFuncionario_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(308, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome ";
            // 
            // btnFechar
            // 
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(819, 7);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(19, 23);
            this.btnFechar.TabIndex = 40;
            this.btnFechar.Text = "X";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // picVoltar
            // 
            this.picVoltar.BackColor = System.Drawing.Color.DarkRed;
            this.picVoltar.Image = global::The_Barber_s_House.Properties.Resources.Previous_free_vector_icons_designed_by_Gregor_Cresnar;
            this.picVoltar.Location = new System.Drawing.Point(21, 5);
            this.picVoltar.Name = "picVoltar";
            this.picVoltar.Size = new System.Drawing.Size(34, 32);
            this.picVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVoltar.TabIndex = 38;
            this.picVoltar.TabStop = false;
            this.picVoltar.Click += new System.EventHandler(this.picVoltar_Click);
            // 
            // mktTelefoneEmerg
            // 
            this.mktTelefoneEmerg.Enabled = false;
            this.mktTelefoneEmerg.Location = new System.Drawing.Point(402, 380);
            this.mktTelefoneEmerg.Mask = "(##) #####-####";
            this.mktTelefoneEmerg.Name = "mktTelefoneEmerg";
            this.mktTelefoneEmerg.Size = new System.Drawing.Size(123, 20);
            this.mktTelefoneEmerg.TabIndex = 202;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(284, 383);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(93, 13);
            this.label27.TabIndex = 201;
            this.label27.Text = "Telefone Emer.";
            // 
            // txtEmail
            // 
            this.txtEmail.Enabled = false;
            this.txtEmail.Location = new System.Drawing.Point(147, 380);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(123, 20);
            this.txtEmail.TabIndex = 200;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(17, 383);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 199;
            this.label16.Text = "E-mail";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(284, 124);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(88, 13);
            this.label26.TabIndex = 197;
            this.label26.Text = "Registro Geral";
            // 
            // mktRG
            // 
            this.mktRG.Enabled = false;
            this.mktRG.Location = new System.Drawing.Point(402, 121);
            this.mktRG.Mask = "##.###.###-#";
            this.mktRG.Name = "mktRG";
            this.mktRG.Size = new System.Drawing.Size(123, 20);
            this.mktRG.TabIndex = 198;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(17, 81);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 13);
            this.label25.TabIndex = 195;
            this.label25.Text = "Nome do Pai";
            // 
            // txtPai
            // 
            this.txtPai.Enabled = false;
            this.txtPai.Location = new System.Drawing.Point(147, 78);
            this.txtPai.Name = "txtPai";
            this.txtPai.Size = new System.Drawing.Size(123, 20);
            this.txtPai.TabIndex = 196;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 13);
            this.label10.TabIndex = 193;
            this.label10.Text = "Nome da Mãe";
            // 
            // txtMãe
            // 
            this.txtMãe.Enabled = false;
            this.txtMãe.Location = new System.Drawing.Point(147, 31);
            this.txtMãe.Name = "txtMãe";
            this.txtMãe.Size = new System.Drawing.Size(123, 20);
            this.txtMãe.TabIndex = 194;
            // 
            // txtEndereço
            // 
            this.txtEndereço.Enabled = false;
            this.txtEndereço.Location = new System.Drawing.Point(659, 332);
            this.txtEndereço.Multiline = true;
            this.txtEndereço.Name = "txtEndereço";
            this.txtEndereço.Size = new System.Drawing.Size(144, 65);
            this.txtEndereço.TabIndex = 192;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(541, 335);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 191;
            this.label15.Text = "Endereço";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Enabled = false;
            this.txtTelefone.Location = new System.Drawing.Point(402, 332);
            this.txtTelefone.Mask = "(##) #####-####";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(123, 20);
            this.txtTelefone.TabIndex = 190;
            // 
            // cboSeguroVida
            // 
            this.cboSeguroVida.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSeguroVida.Enabled = false;
            this.cboSeguroVida.FormattingEnabled = true;
            this.cboSeguroVida.Location = new System.Drawing.Point(661, 251);
            this.cboSeguroVida.Name = "cboSeguroVida";
            this.cboSeguroVida.Size = new System.Drawing.Size(142, 21);
            this.cboSeguroVida.TabIndex = 189;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(541, 254);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(93, 13);
            this.label24.TabIndex = 188;
            this.label24.Text = "Seguro de vida";
            // 
            // cboPlanoOdontológico
            // 
            this.cboPlanoOdontológico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPlanoOdontológico.Enabled = false;
            this.cboPlanoOdontológico.FormattingEnabled = true;
            this.cboPlanoOdontológico.Location = new System.Drawing.Point(661, 77);
            this.cboPlanoOdontológico.Name = "cboPlanoOdontológico";
            this.cboPlanoOdontológico.Size = new System.Drawing.Size(142, 21);
            this.cboPlanoOdontológico.TabIndex = 187;
            // 
            // cboPlanodeSaúde
            // 
            this.cboPlanodeSaúde.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPlanodeSaúde.Enabled = false;
            this.cboPlanodeSaúde.FormattingEnabled = true;
            this.cboPlanodeSaúde.Location = new System.Drawing.Point(661, 32);
            this.cboPlanodeSaúde.Name = "cboPlanodeSaúde";
            this.cboPlanodeSaúde.Size = new System.Drawing.Size(142, 21);
            this.cboPlanodeSaúde.TabIndex = 186;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(284, 209);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(83, 13);
            this.label23.TabIndex = 184;
            this.label23.Text = "Insalubridade";
            // 
            // nudInsalubridade
            // 
            this.nudInsalubridade.DecimalPlaces = 2;
            this.nudInsalubridade.Enabled = false;
            this.nudInsalubridade.Location = new System.Drawing.Point(402, 207);
            this.nudInsalubridade.Name = "nudInsalubridade";
            this.nudInsalubridade.Size = new System.Drawing.Size(123, 20);
            this.nudInsalubridade.TabIndex = 185;
            // 
            // nudComissão
            // 
            this.nudComissão.DecimalPlaces = 2;
            this.nudComissão.Enabled = false;
            this.nudComissão.Location = new System.Drawing.Point(402, 34);
            this.nudComissão.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudComissão.Name = "nudComissão";
            this.nudComissão.Size = new System.Drawing.Size(123, 20);
            this.nudComissão.TabIndex = 161;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 151;
            this.label3.Text = "Nome da Empresa";
            // 
            // txtNEmpresa
            // 
            this.txtNEmpresa.Enabled = false;
            this.txtNEmpresa.Location = new System.Drawing.Point(147, 121);
            this.txtNEmpresa.Name = "txtNEmpresa";
            this.txtNEmpresa.Size = new System.Drawing.Size(123, 20);
            this.txtNEmpresa.TabIndex = 152;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 164);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 153;
            this.label5.Text = "Gênero";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 154;
            this.label6.Text = "IRRF";
            // 
            // cboGenero
            // 
            this.cboGenero.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGenero.Enabled = false;
            this.cboGenero.FormattingEnabled = true;
            this.cboGenero.Items.AddRange(new object[] {
            "Masculino",
            "Feminino"});
            this.cboGenero.Location = new System.Drawing.Point(147, 161);
            this.cboGenero.Name = "cboGenero";
            this.cboGenero.Size = new System.Drawing.Size(123, 21);
            this.cboGenero.TabIndex = 183;
            // 
            // nudIRRF
            // 
            this.nudIRRF.DecimalPlaces = 2;
            this.nudIRRF.Enabled = false;
            this.nudIRRF.Location = new System.Drawing.Point(147, 206);
            this.nudIRRF.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudIRRF.Name = "nudIRRF";
            this.nudIRRF.Size = new System.Drawing.Size(123, 20);
            this.nudIRRF.TabIndex = 155;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 13);
            this.label7.TabIndex = 156;
            this.label7.Text = "Data de Nascimento";
            // 
            // dtpDtNascimento
            // 
            this.dtpDtNascimento.Enabled = false;
            this.dtpDtNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDtNascimento.Location = new System.Drawing.Point(147, 242);
            this.dtpDtNascimento.Name = "dtpDtNascimento";
            this.dtpDtNascimento.Size = new System.Drawing.Size(123, 20);
            this.dtpDtNascimento.TabIndex = 157;
            this.dtpDtNascimento.Value = new System.DateTime(2000, 1, 1, 22, 54, 0, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 290);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 158;
            this.label8.Text = "INSS";
            // 
            // cboCargo
            // 
            this.cboCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCargo.Enabled = false;
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Location = new System.Drawing.Point(147, 335);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(123, 21);
            this.cboCargo.TabIndex = 182;
            // 
            // nudINSS
            // 
            this.nudINSS.DecimalPlaces = 2;
            this.nudINSS.Enabled = false;
            this.nudINSS.Location = new System.Drawing.Point(147, 287);
            this.nudINSS.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudINSS.Name = "nudINSS";
            this.nudINSS.Size = new System.Drawing.Size(121, 20);
            this.nudINSS.TabIndex = 159;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(15, 338);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 181;
            this.label22.Text = "Cargo";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(284, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 160;
            this.label9.Text = "Comissão";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(543, 294);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 162;
            this.label11.Text = "Salário Família";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(541, 81);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(118, 13);
            this.label20.TabIndex = 180;
            this.label20.Text = "Plano Odontológico";
            // 
            // nudSFamilia
            // 
            this.nudSFamilia.DecimalPlaces = 2;
            this.nudSFamilia.Enabled = false;
            this.nudSFamilia.Location = new System.Drawing.Point(661, 292);
            this.nudSFamilia.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudSFamilia.Name = "nudSFamilia";
            this.nudSFamilia.Size = new System.Drawing.Size(142, 20);
            this.nudSFamilia.TabIndex = 163;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(284, 164);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 13);
            this.label12.TabIndex = 164;
            this.label12.Text = "Periculosidade";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(541, 36);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(97, 13);
            this.label21.TabIndex = 179;
            this.label21.Text = "Plano de Saúde";
            // 
            // nudPericulosidade
            // 
            this.nudPericulosidade.DecimalPlaces = 2;
            this.nudPericulosidade.Enabled = false;
            this.nudPericulosidade.Location = new System.Drawing.Point(402, 162);
            this.nudPericulosidade.Name = "nudPericulosidade";
            this.nudPericulosidade.Size = new System.Drawing.Size(123, 20);
            this.nudPericulosidade.TabIndex = 165;
            // 
            // nudVLAlimentação
            // 
            this.nudVLAlimentação.DecimalPlaces = 2;
            this.nudVLAlimentação.Enabled = false;
            this.nudVLAlimentação.Location = new System.Drawing.Point(661, 207);
            this.nudVLAlimentação.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudVLAlimentação.Name = "nudVLAlimentação";
            this.nudVLAlimentação.Size = new System.Drawing.Size(54, 20);
            this.nudVLAlimentação.TabIndex = 178;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(284, 254);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 13);
            this.label13.TabIndex = 166;
            this.label13.Text = "Data de Admissão";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(541, 210);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 177;
            this.label17.Text = "Vale Alimentação";
            // 
            // dtpAdimissao
            // 
            this.dtpAdimissao.Enabled = false;
            this.dtpAdimissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdimissao.Location = new System.Drawing.Point(402, 248);
            this.dtpAdimissao.Name = "dtpAdimissao";
            this.dtpAdimissao.Size = new System.Drawing.Size(123, 20);
            this.dtpAdimissao.TabIndex = 167;
            // 
            // nudVLRefeição
            // 
            this.nudVLRefeição.DecimalPlaces = 2;
            this.nudVLRefeição.Enabled = false;
            this.nudVLRefeição.Location = new System.Drawing.Point(661, 162);
            this.nudVLRefeição.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudVLRefeição.Name = "nudVLRefeição";
            this.nudVLRefeição.Size = new System.Drawing.Size(54, 20);
            this.nudVLRefeição.TabIndex = 176;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(284, 294);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 168;
            this.label14.Text = "Salário Bruto";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(541, 164);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 175;
            this.label18.Text = "Vale Refeição";
            // 
            // nudSBruto
            // 
            this.nudSBruto.DecimalPlaces = 2;
            this.nudSBruto.Enabled = false;
            this.nudSBruto.Location = new System.Drawing.Point(402, 292);
            this.nudSBruto.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudSBruto.Name = "nudSBruto";
            this.nudSBruto.Size = new System.Drawing.Size(123, 20);
            this.nudSBruto.TabIndex = 169;
            // 
            // nudVLTransporte
            // 
            this.nudVLTransporte.DecimalPlaces = 2;
            this.nudVLTransporte.Enabled = false;
            this.nudVLTransporte.Location = new System.Drawing.Point(659, 119);
            this.nudVLTransporte.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudVLTransporte.Name = "nudVLTransporte";
            this.nudVLTransporte.Size = new System.Drawing.Size(54, 20);
            this.nudVLTransporte.TabIndex = 174;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(284, 335);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 170;
            this.label19.Text = "Telefone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(541, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 173;
            this.label2.Text = "Vale Transporte";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(284, 80);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(30, 13);
            this.label29.TabIndex = 171;
            this.label29.Text = "CPF";
            // 
            // txtCPF
            // 
            this.txtCPF.Enabled = false;
            this.txtCPF.Location = new System.Drawing.Point(402, 77);
            this.txtCPF.Mask = "###.###.###-##";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(123, 20);
            this.txtCPF.TabIndex = 172;
            // 
            // txtcdvr
            // 
            this.txtcdvr.Enabled = false;
            this.txtcdvr.Location = new System.Drawing.Point(719, 161);
            this.txtcdvr.Name = "txtcdvr";
            this.txtcdvr.Size = new System.Drawing.Size(84, 20);
            this.txtcdvr.TabIndex = 92;
            // 
            // txtcdvl
            // 
            this.txtcdvl.Enabled = false;
            this.txtcdvl.Location = new System.Drawing.Point(719, 121);
            this.txtcdvl.Name = "txtcdvl";
            this.txtcdvl.Size = new System.Drawing.Size(84, 20);
            this.txtcdvl.TabIndex = 91;
            // 
            // RemoverFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 581);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RemoverFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RemoverFuncionario";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInsalubridade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudComissão)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIRRF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudINSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSFamilia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLAlimentação)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLRefeição)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVLTransporte)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtcdva;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.PictureBox picVoltar;
        private System.Windows.Forms.MaskedTextBox mktTelefoneEmerg;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.MaskedTextBox mktRG;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtPai;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMãe;
        private System.Windows.Forms.TextBox txtEndereço;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.ComboBox cboSeguroVida;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cboPlanoOdontológico;
        private System.Windows.Forms.ComboBox cboPlanodeSaúde;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown nudInsalubridade;
        private System.Windows.Forms.NumericUpDown nudComissão;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNEmpresa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboGenero;
        private System.Windows.Forms.NumericUpDown nudIRRF;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker dtpDtNascimento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.NumericUpDown nudINSS;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown nudSFamilia;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown nudPericulosidade;
        private System.Windows.Forms.NumericUpDown nudVLAlimentação;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker dtpAdimissao;
        private System.Windows.Forms.NumericUpDown nudVLRefeição;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown nudSBruto;
        private System.Windows.Forms.NumericUpDown nudVLTransporte;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.TextBox txtcdvr;
        private System.Windows.Forms.TextBox txtcdvl;
    }
}