﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH
{
    public partial class NovaFopag : Form
    {
        public NovaFopag()
        {
            InitializeComponent();
            CarregarCampos();
        }

        Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();

        private void CarregarCampos()
        {
            cboFuncionario.ValueMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboFuncionario.DataSource = func.ListarFuncionarios();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void lblMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnGerarFolha_Click(object sender, EventArgs e)
        {

        }

        Business.RH.Folha_de_Pagamento.BusinessFopag fopag = new Business.RH.Folha_de_Pagamento.BusinessFopag();
        Business.RH.Outros.BusinessFGTS fgts = new Business.RH.Outros.BusinessFGTS();

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*try
            {
                Database.Entity.tb_funcionario funcionario = cboFuncionario.SelectedItem as Database.Entity.tb_funcionario;

                Database.Entity.tb_funcionario func = cboFuncionario.SelectedItem as Database.Entity.tb_funcionario;

                Database.Entity.tb_fopag lista = fopag.ConsultarFolhaDePagamentoInserir(func.nm_funcionario);
                cboCargo.Text = lista.tb_funcionario.tb_cargo.ds_cargo;
                txtCNPJ.Text = lista.ds_cnpj;
                txtEmpresa.Text = lista.tb_funcionario.nm_empresa;
                txtFuncionario.Text = lista.tb_funcionario.nm_funcionario;
                nudValeAlimentação.Value = Convert.ToDecimal(lista.tb_funcionario.tb_vale_alimentacao.vl_vale_alimentacao);
                nudDSR.Value = lista.ds_adiantamento_semanal;
                nudFaltas.Value = lista.qt_falta;
                //nudFGTS.Value = lista.vl_fgts;
                nudGratificação.Value = lista.vl_gratificacao;
                // nudHoraExtra.Value = lista.qt_hora_extra; Não vou ussar esse campo agora
                nudINSS.Value = lista.tb_funcionario.vl_inss;
                nudPlanoOdontológico.Value = lista.tb_funcionario.tb_plano_odontologico.vl_plano;
                nudPericulosidade.Value = lista.tb_funcionario.vl_periculosidade;
                nudPlanoDeSaude.Value = lista.tb_funcionario.tb_plano_de_saude.vl_plano;
                nudRemuneração.Value = lista.tb_funcionario.vl_salario_bruto;
                nudSalárioBruto.Value = lista.tb_funcionario.vl_salario_bruto;
                nudSalárioFamília.Value = lista.tb_funcionario.vl_salario_familia;
                nudSeguroVida.Value = lista.tb_funcionario.tb_seguro_vida.vl_plano;
                nudValeRefeição.Value = lista.tb_funcionario.tb_vale_refeicao.vl_vale_refeicao;
                nudValeTransporte.Value = lista.tb_funcionario.tb_vale_transporte.vl_vt;
                nudTotalDescontos.Text = "Total: R$" + lista.vl_total_descontos;
                nudTotalProventos.Text = "Total: R$" + lista.vl_total_proventos;
                nudSalárioLíquido.Text = "Total: R$" + lista.vl_salario;

                dtpAdmissão.Value = lista.tb_funcionario.dt_adimissao.Date;
                //dtpDemissão.Value = lista.d.Date;

                decimal taxaAte = 0;
                decimal taxaEntre = 0;
                decimal taxaEntre2 = 0;
                decimal taxaAcima = 0;
                int qtFaltas = 0;
                int passagensVezes = 0;
                decimal valorAte = 0;
                Objetos.FolhasDePagamento folhaDePagamento = new Objetos.FolhasDePagamento();
                Database.Entity.tb_fgts taxaFGTS = fgts.ListarTodos();
                Modelos.folhaDePagamento resultado = new Modelos.folhaDePagamento();

                resultado = folhaDePagamento.GerarFolha(lista, taxaFGTS.vl_fgts, nudHoraExtra.Value, taxaAte, taxaEntre, taxaEntre2, taxaAcima, qtFaltas, passagensVezes, valorAte);

                nudSalárioLíquido.Value = resultado.SalarioLiquido;
                nudTotalProventos.Value = resultado.TotalProventos;
                nudTotalDescontos.Value = resultado.TotalDescontos;
                nudValeTransporte.Value = resultado.VT;
                nudDSR.Value = resultado.DSR;
                nudFGTS.Value = resultado.FGTS;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }*/
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();           
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();           
        }
    }
}
