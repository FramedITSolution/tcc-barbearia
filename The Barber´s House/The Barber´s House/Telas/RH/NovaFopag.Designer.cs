﻿namespace The_Barber_s_House.Telas.RH
{
    partial class NovaFopag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label28 = new System.Windows.Forms.Label();
            this.btnGerarFolha = new System.Windows.Forms.Button();
            this.nudFGTS = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.nudDSR = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCNPJ = new System.Windows.Forms.TextBox();
            this.nudGratificação = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.nudHoraExtra = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.nudSalárioBruto = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.dtpAdmissão = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpPagamento = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.nudValeAlimentação = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.nudValeRefeição = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.nudValeTransporte = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.nudPlanoOdontológico = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.nudPlanoDeSaude = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.nudSeguroVida = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.nudRemuneração = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.nudPericulosidade = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudSalárioFamília = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nudINSS = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpDemissão = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtFuncionario = new System.Windows.Forms.TextBox();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmpresa = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudFaltas = new System.Windows.Forms.NumericUpDown();
            this.nudSalárioLíquido = new System.Windows.Forms.NumericUpDown();
            this.nudTotalDescontos = new System.Windows.Forms.NumericUpDown();
            this.nudTotalProventos = new System.Windows.Forms.NumericUpDown();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.picVoltar = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudFGTS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDSR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGratificação)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraExtra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalárioBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentação)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeição)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTransporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoOdontológico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoDeSaude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSeguroVida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRemuneração)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalárioFamília)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudINSS)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalárioLíquido)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalDescontos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalProventos)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(207, -50);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(264, 31);
            this.label28.TabIndex = 183;
            this.label28.Text = "Folha de Pagamento";
            // 
            // btnGerarFolha
            // 
            this.btnGerarFolha.BackColor = System.Drawing.Color.DarkRed;
            this.btnGerarFolha.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerarFolha.ForeColor = System.Drawing.Color.White;
            this.btnGerarFolha.Location = new System.Drawing.Point(571, 19);
            this.btnGerarFolha.Name = "btnGerarFolha";
            this.btnGerarFolha.Size = new System.Drawing.Size(117, 39);
            this.btnGerarFolha.TabIndex = 239;
            this.btnGerarFolha.Text = "Gerar Folha";
            this.btnGerarFolha.UseVisualStyleBackColor = false;
            this.btnGerarFolha.Click += new System.EventHandler(this.btnGerarFolha_Click);
            // 
            // nudFGTS
            // 
            this.nudFGTS.DecimalPlaces = 2;
            this.nudFGTS.Location = new System.Drawing.Point(554, 542);
            this.nudFGTS.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudFGTS.Name = "nudFGTS";
            this.nudFGTS.Size = new System.Drawing.Size(158, 20);
            this.nudFGTS.TabIndex = 238;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(502, 549);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 13);
            this.label27.TabIndex = 237;
            this.label27.Text = "FGTS:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(502, 523);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 13);
            this.label26.TabIndex = 235;
            this.label26.Text = "Faltas:";
            // 
            // nudDSR
            // 
            this.nudDSR.DecimalPlaces = 2;
            this.nudDSR.Location = new System.Drawing.Point(135, 473);
            this.nudDSR.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudDSR.Name = "nudDSR";
            this.nudDSR.Size = new System.Drawing.Size(158, 20);
            this.nudDSR.TabIndex = 234;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(79, 480);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 233;
            this.label25.Text = "DSR:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(59, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 232;
            this.label11.Text = "CNPJ:";
            // 
            // txtCNPJ
            // 
            this.txtCNPJ.Location = new System.Drawing.Point(104, 127);
            this.txtCNPJ.Name = "txtCNPJ";
            this.txtCNPJ.Size = new System.Drawing.Size(158, 20);
            this.txtCNPJ.TabIndex = 231;
            // 
            // nudGratificação
            // 
            this.nudGratificação.DecimalPlaces = 2;
            this.nudGratificação.Location = new System.Drawing.Point(134, 447);
            this.nudGratificação.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudGratificação.Name = "nudGratificação";
            this.nudGratificação.Size = new System.Drawing.Size(158, 20);
            this.nudGratificação.TabIndex = 230;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(52, 450);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 229;
            this.label10.Text = "Gratificação:";
            // 
            // nudHoraExtra
            // 
            this.nudHoraExtra.DecimalPlaces = 2;
            this.nudHoraExtra.Location = new System.Drawing.Point(135, 421);
            this.nudHoraExtra.Name = "nudHoraExtra";
            this.nudHoraExtra.Size = new System.Drawing.Size(158, 20);
            this.nudHoraExtra.TabIndex = 228;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(53, 424);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 227;
            this.label8.Text = "Hora Extra:";
            // 
            // nudSalárioBruto
            // 
            this.nudSalárioBruto.DecimalPlaces = 2;
            this.nudSalárioBruto.Location = new System.Drawing.Point(134, 341);
            this.nudSalárioBruto.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudSalárioBruto.Name = "nudSalárioBruto";
            this.nudSalárioBruto.Size = new System.Drawing.Size(158, 20);
            this.nudSalárioBruto.TabIndex = 226;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 348);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 225;
            this.label3.Text = "Salário Bruto:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(408, 277);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1, 350);
            this.panel4.TabIndex = 220;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(11, 248);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(782, 1);
            this.panel3.TabIndex = 218;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(538, 283);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(109, 23);
            this.label24.TabIndex = 224;
            this.label24.Text = "Descontos:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(153, 283);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(104, 23);
            this.label23.TabIndex = 223;
            this.label23.Text = "Proventos:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(602, 179);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 13);
            this.label22.TabIndex = 222;
            this.label22.Text = "Admissão:";
            // 
            // dtpAdmissão
            // 
            this.dtpAdmissão.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdmissão.Location = new System.Drawing.Point(673, 173);
            this.dtpAdmissão.Name = "dtpAdmissão";
            this.dtpAdmissão.Size = new System.Drawing.Size(96, 20);
            this.dtpAdmissão.TabIndex = 221;
            this.dtpAdmissão.Value = new System.DateTime(9998, 1, 1, 0, 0, 0, 0);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(54, 198);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 219;
            this.label21.Text = "Cargo:";
            // 
            // cboCargo
            // 
            this.cboCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Location = new System.Drawing.Point(104, 195);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(158, 21);
            this.cboCargo.TabIndex = 217;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(7, 153);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(782, 1);
            this.panel2.TabIndex = 216;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(543, 100);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 13);
            this.label18.TabIndex = 215;
            this.label18.Text = "Data de Pagamento:";
            // 
            // dtpPagamento
            // 
            this.dtpPagamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPagamento.Location = new System.Drawing.Point(672, 97);
            this.dtpPagamento.Name = "dtpPagamento";
            this.dtpPagamento.Size = new System.Drawing.Size(96, 20);
            this.dtpPagamento.TabIndex = 214;
            this.dtpPagamento.Value = new System.DateTime(9998, 1, 1, 0, 0, 0, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(5, 78);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 1);
            this.panel1.TabIndex = 213;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(54, 42);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 212;
            this.label20.Text = "Nome:";
            // 
            // nudValeAlimentação
            // 
            this.nudValeAlimentação.DecimalPlaces = 2;
            this.nudValeAlimentação.Location = new System.Drawing.Point(554, 464);
            this.nudValeAlimentação.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudValeAlimentação.Name = "nudValeAlimentação";
            this.nudValeAlimentação.Size = new System.Drawing.Size(158, 20);
            this.nudValeAlimentação.TabIndex = 211;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(445, 473);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 13);
            this.label15.TabIndex = 210;
            this.label15.Text = "Vale Alimentação:";
            // 
            // nudValeRefeição
            // 
            this.nudValeRefeição.DecimalPlaces = 2;
            this.nudValeRefeição.Location = new System.Drawing.Point(554, 438);
            this.nudValeRefeição.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudValeRefeição.Name = "nudValeRefeição";
            this.nudValeRefeição.Size = new System.Drawing.Size(158, 20);
            this.nudValeRefeição.TabIndex = 209;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(460, 445);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 208;
            this.label16.Text = "Vale Refeição:";
            // 
            // nudValeTransporte
            // 
            this.nudValeTransporte.DecimalPlaces = 2;
            this.nudValeTransporte.Location = new System.Drawing.Point(554, 412);
            this.nudValeTransporte.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudValeTransporte.Name = "nudValeTransporte";
            this.nudValeTransporte.Size = new System.Drawing.Size(158, 20);
            this.nudValeTransporte.TabIndex = 207;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(454, 419);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 13);
            this.label17.TabIndex = 206;
            this.label17.Text = "Vale Transporte:";
            // 
            // nudPlanoOdontológico
            // 
            this.nudPlanoOdontológico.DecimalPlaces = 2;
            this.nudPlanoOdontológico.Location = new System.Drawing.Point(554, 386);
            this.nudPlanoOdontológico.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudPlanoOdontológico.Name = "nudPlanoOdontológico";
            this.nudPlanoOdontológico.Size = new System.Drawing.Size(158, 20);
            this.nudPlanoOdontológico.TabIndex = 205;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(434, 393);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(122, 13);
            this.label12.TabIndex = 204;
            this.label12.Text = "Plano Odontológico:";
            // 
            // nudPlanoDeSaude
            // 
            this.nudPlanoDeSaude.DecimalPlaces = 2;
            this.nudPlanoDeSaude.Location = new System.Drawing.Point(554, 360);
            this.nudPlanoDeSaude.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudPlanoDeSaude.Name = "nudPlanoDeSaude";
            this.nudPlanoDeSaude.Size = new System.Drawing.Size(158, 20);
            this.nudPlanoDeSaude.TabIndex = 203;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(451, 369);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 13);
            this.label13.TabIndex = 202;
            this.label13.Text = "Plano de Saúde:";
            // 
            // nudSeguroVida
            // 
            this.nudSeguroVida.DecimalPlaces = 2;
            this.nudSeguroVida.Location = new System.Drawing.Point(554, 334);
            this.nudSeguroVida.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.nudSeguroVida.Name = "nudSeguroVida";
            this.nudSeguroVida.Size = new System.Drawing.Size(158, 20);
            this.nudSeguroVida.TabIndex = 201;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(454, 341);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 200;
            this.label14.Text = "Seguro de Vida:";
            // 
            // nudRemuneração
            // 
            this.nudRemuneração.DecimalPlaces = 2;
            this.nudRemuneração.Location = new System.Drawing.Point(104, 222);
            this.nudRemuneração.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudRemuneração.Name = "nudRemuneração";
            this.nudRemuneração.Size = new System.Drawing.Size(158, 20);
            this.nudRemuneração.TabIndex = 199;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 224);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 198;
            this.label9.Text = "Renumeração:";
            // 
            // nudPericulosidade
            // 
            this.nudPericulosidade.DecimalPlaces = 2;
            this.nudPericulosidade.Location = new System.Drawing.Point(135, 395);
            this.nudPericulosidade.Name = "nudPericulosidade";
            this.nudPericulosidade.Size = new System.Drawing.Size(158, 20);
            this.nudPericulosidade.TabIndex = 197;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 397);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 196;
            this.label7.Text = "Periculosidade:";
            // 
            // nudSalárioFamília
            // 
            this.nudSalárioFamília.DecimalPlaces = 2;
            this.nudSalárioFamília.Location = new System.Drawing.Point(135, 369);
            this.nudSalárioFamília.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSalárioFamília.Name = "nudSalárioFamília";
            this.nudSalárioFamília.Size = new System.Drawing.Size(158, 20);
            this.nudSalárioFamília.TabIndex = 195;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 371);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 194;
            this.label6.Text = "Salario Família:";
            // 
            // nudINSS
            // 
            this.nudINSS.DecimalPlaces = 2;
            this.nudINSS.Location = new System.Drawing.Point(554, 490);
            this.nudINSS.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudINSS.Name = "nudINSS";
            this.nudINSS.Size = new System.Drawing.Size(158, 20);
            this.nudINSS.TabIndex = 193;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(606, 215);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 192;
            this.label5.Text = "Demissão:";
            // 
            // dtpDemissão
            // 
            this.dtpDemissão.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDemissão.Location = new System.Drawing.Point(673, 209);
            this.dtpDemissão.Name = "dtpDemissão";
            this.dtpDemissão.Size = new System.Drawing.Size(96, 20);
            this.dtpDemissão.TabIndex = 191;
            this.dtpDemissão.Value = new System.DateTime(9998, 1, 1, 0, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(502, 497);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 190;
            this.label4.Text = "INSS:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(54, 172);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 189;
            this.label19.Text = "Nome:";
            // 
            // txtFuncionario
            // 
            this.txtFuncionario.Location = new System.Drawing.Point(103, 169);
            this.txtFuncionario.Name = "txtFuncionario";
            this.txtFuncionario.Size = new System.Drawing.Size(158, 20);
            this.txtFuncionario.TabIndex = 188;
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Location = new System.Drawing.Point(104, 39);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(158, 21);
            this.cboFuncionario.TabIndex = 186;
            this.cboFuncionario.SelectedIndexChanged += new System.EventHandler(this.cboFuncionario_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 185;
            this.label1.Text = "Empresa:";
            // 
            // txtEmpresa
            // 
            this.txtEmpresa.Location = new System.Drawing.Point(104, 97);
            this.txtEmpresa.Name = "txtEmpresa";
            this.txtEmpresa.Size = new System.Drawing.Size(158, 20);
            this.txtEmpresa.TabIndex = 184;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(527, 609);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 242;
            this.label29.Text = "Total:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(312, 668);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(97, 13);
            this.label31.TabIndex = 243;
            this.label31.Text = "Salário Líquido:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(131, 609);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(40, 13);
            this.label32.TabIndex = 244;
            this.label32.Text = "Total:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.nudFaltas);
            this.groupBox1.Controls.Add(this.nudSalárioLíquido);
            this.groupBox1.Controls.Add(this.nudTotalDescontos);
            this.groupBox1.Controls.Add(this.nudTotalProventos);
            this.groupBox1.Controls.Add(this.txtEmpresa);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cboFuncionario);
            this.groupBox1.Controls.Add(this.txtFuncionario);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.btnGerarFolha);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.nudFGTS);
            this.groupBox1.Controls.Add(this.dtpDemissão);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.nudINSS);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.nudDSR);
            this.groupBox1.Controls.Add(this.nudSalárioFamília);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.nudPericulosidade);
            this.groupBox1.Controls.Add(this.txtCNPJ);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nudGratificação);
            this.groupBox1.Controls.Add(this.nudRemuneração);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.nudHoraExtra);
            this.groupBox1.Controls.Add(this.nudSeguroVida);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.nudSalárioBruto);
            this.groupBox1.Controls.Add(this.nudPlanoDeSaude);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Controls.Add(this.nudPlanoOdontológico);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.nudValeTransporte);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.nudValeRefeição);
            this.groupBox1.Controls.Add(this.dtpAdmissão);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.nudValeAlimentação);
            this.groupBox1.Controls.Add(this.cboCargo);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.dtpPagamento);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(823, 716);
            this.groupBox1.TabIndex = 247;
            this.groupBox1.TabStop = false;
            // 
            // nudFaltas
            // 
            this.nudFaltas.Location = new System.Drawing.Point(554, 516);
            this.nudFaltas.Name = "nudFaltas";
            this.nudFaltas.Size = new System.Drawing.Size(158, 20);
            this.nudFaltas.TabIndex = 250;
            // 
            // nudSalárioLíquido
            // 
            this.nudSalárioLíquido.DecimalPlaces = 2;
            this.nudSalárioLíquido.Location = new System.Drawing.Point(408, 666);
            this.nudSalárioLíquido.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudSalárioLíquido.Minimum = new decimal(new int[] {
            2440,
            0,
            0,
            -2147483648});
            this.nudSalárioLíquido.Name = "nudSalárioLíquido";
            this.nudSalárioLíquido.Size = new System.Drawing.Size(93, 20);
            this.nudSalárioLíquido.TabIndex = 249;
            // 
            // nudTotalDescontos
            // 
            this.nudTotalDescontos.DecimalPlaces = 2;
            this.nudTotalDescontos.Location = new System.Drawing.Point(595, 607);
            this.nudTotalDescontos.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudTotalDescontos.Minimum = new decimal(new int[] {
            2440,
            0,
            0,
            -2147483648});
            this.nudTotalDescontos.Name = "nudTotalDescontos";
            this.nudTotalDescontos.Size = new System.Drawing.Size(93, 20);
            this.nudTotalDescontos.TabIndex = 248;
            // 
            // nudTotalProventos
            // 
            this.nudTotalProventos.DecimalPlaces = 2;
            this.nudTotalProventos.Location = new System.Drawing.Point(199, 607);
            this.nudTotalProventos.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudTotalProventos.Minimum = new decimal(new int[] {
            2440,
            0,
            0,
            -2147483648});
            this.nudTotalProventos.Name = "nudTotalProventos";
            this.nudTotalProventos.Size = new System.Drawing.Size(93, 20);
            this.nudTotalProventos.TabIndex = 247;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkRed;
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.btnMinimizar);
            this.panel5.Controls.Add(this.btnFechar);
            this.panel5.Controls.Add(this.picVoltar);
            this.panel5.ForeColor = System.Drawing.Color.Black;
            this.panel5.Location = new System.Drawing.Point(-3, -3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(853, 345);
            this.panel5.TabIndex = 248;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(296, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(253, 29);
            this.label2.TabIndex = 41;
            this.label2.Text = "Folha de pagamento";
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.Location = new System.Drawing.Point(794, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(19, 26);
            this.btnMinimizar.TabIndex = 39;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(819, 5);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(19, 23);
            this.btnFechar.TabIndex = 40;
            this.btnFechar.Text = "X";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // picVoltar
            // 
            this.picVoltar.BackColor = System.Drawing.Color.DarkRed;
            this.picVoltar.Image = global::The_Barber_s_House.Properties.Resources.Previous_free_vector_icons_designed_by_Gregor_Cresnar;
            this.picVoltar.Location = new System.Drawing.Point(31, 6);
            this.picVoltar.Name = "picVoltar";
            this.picVoltar.Size = new System.Drawing.Size(34, 32);
            this.picVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVoltar.TabIndex = 38;
            this.picVoltar.TabStop = false;
            this.picVoltar.Click += new System.EventHandler(this.picVoltar_Click);
            // 
            // NovaFopag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 768);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.label28);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NovaFopag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NovaFopag";
            ((System.ComponentModel.ISupportInitialize)(this.nudFGTS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDSR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGratificação)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraExtra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalárioBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentação)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeição)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTransporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoOdontológico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoDeSaude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSeguroVida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRemuneração)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalárioFamília)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudINSS)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalárioLíquido)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalDescontos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalProventos)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnGerarFolha;
        private System.Windows.Forms.NumericUpDown nudFGTS;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown nudDSR;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtCNPJ;
        private System.Windows.Forms.NumericUpDown nudGratificação;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudHoraExtra;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudSalárioBruto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DateTimePicker dtpAdmissão;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtpPagamento;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown nudValeAlimentação;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nudValeRefeição;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudValeTransporte;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown nudPlanoOdontológico;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudPlanoDeSaude;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudSeguroVida;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudRemuneração;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudPericulosidade;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudSalárioFamília;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudINSS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpDemissão;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtFuncionario;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmpresa;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.NumericUpDown nudSalárioLíquido;
        private System.Windows.Forms.NumericUpDown nudTotalDescontos;
        private System.Windows.Forms.NumericUpDown nudTotalProventos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.PictureBox picVoltar;
        private System.Windows.Forms.NumericUpDown nudFaltas;
    }
}