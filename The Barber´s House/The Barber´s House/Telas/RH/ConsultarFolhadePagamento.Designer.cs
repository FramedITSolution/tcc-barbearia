﻿namespace The_Barber_s_House.Telas.RH
{
    partial class ConsultarFolhadePagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nudFgts = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.nudDsr = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.nudGratificacao = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.nudHoraExtra = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.nudSalarioBruto = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.nudAlimentacao = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.nudValeRefeicao = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.nudValeTransporte = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.nudOdontologico = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.nudPlanoSaude = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.nudSeguroVida = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.nudPericulosidade = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudSalarioFamilia = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nudInss = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.dgvLista = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_salario_bruto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_pagamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_adimissao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_demitido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_salario_familia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_periculosidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qt_hora_extra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_gratificacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_adiantamento_semanal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qt_falta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_fgts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_salario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_total_desconto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_total_proventos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_adicional_noturno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_plr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_inss = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_empresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_cpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_endereco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_vt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_vale_alimentacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_vale_refeicao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_plano_odonto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_plano_saude = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_plano_vida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblTotalProventos = new System.Windows.Forms.Label();
            this.lblSalarioLiquido = new System.Windows.Forms.Label();
            this.txtCnpj = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.dtpAdmissao = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpDataPagamento = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nudRenumeracao = new System.Windows.Forms.NumericUpDown();
            this.cboNomeBuscar = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpDataPagamentoBuscar = new System.Windows.Forms.DateTimePicker();
            this.dtpDemissao = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmpresa = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.picVoltar = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudFaltas = new System.Windows.Forms.NumericUpDown();
            this.lblTotaldedescontos = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudFgts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDsr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGratificacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraExtra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioBruto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAlimentacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeicao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTransporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOdontologico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoSaude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSeguroVida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioFamilia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRenumeracao)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).BeginInit();
            this.SuspendLayout();
            // 
            // nudFgts
            // 
            this.nudFgts.DecimalPlaces = 2;
            this.nudFgts.Location = new System.Drawing.Point(583, 535);
            this.nudFgts.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudFgts.Name = "nudFgts";
            this.nudFgts.Size = new System.Drawing.Size(158, 20);
            this.nudFgts.TabIndex = 346;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(524, 537);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 13);
            this.label27.TabIndex = 345;
            this.label27.Text = "FGTS:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(524, 511);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(45, 13);
            this.label26.TabIndex = 343;
            this.label26.Text = "Faltas:";
            // 
            // nudDsr
            // 
            this.nudDsr.DecimalPlaces = 2;
            this.nudDsr.Location = new System.Drawing.Point(152, 508);
            this.nudDsr.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudDsr.Name = "nudDsr";
            this.nudDsr.Size = new System.Drawing.Size(158, 20);
            this.nudDsr.TabIndex = 342;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(93, 515);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(37, 13);
            this.label25.TabIndex = 341;
            this.label25.Text = "DSR:";
            // 
            // nudGratificacao
            // 
            this.nudGratificacao.DecimalPlaces = 2;
            this.nudGratificacao.Location = new System.Drawing.Point(151, 482);
            this.nudGratificacao.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudGratificacao.Name = "nudGratificacao";
            this.nudGratificacao.Size = new System.Drawing.Size(158, 20);
            this.nudGratificacao.TabIndex = 338;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(66, 485);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 337;
            this.label10.Text = "Gratificação:";
            // 
            // nudHoraExtra
            // 
            this.nudHoraExtra.DecimalPlaces = 2;
            this.nudHoraExtra.Location = new System.Drawing.Point(152, 456);
            this.nudHoraExtra.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudHoraExtra.Name = "nudHoraExtra";
            this.nudHoraExtra.Size = new System.Drawing.Size(158, 20);
            this.nudHoraExtra.TabIndex = 336;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(67, 459);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 13);
            this.label8.TabIndex = 335;
            this.label8.Text = "Hora Extra:";
            // 
            // nudSalarioBruto
            // 
            this.nudSalarioBruto.DecimalPlaces = 2;
            this.nudSalarioBruto.Location = new System.Drawing.Point(151, 376);
            this.nudSalarioBruto.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudSalarioBruto.Name = "nudSalarioBruto";
            this.nudSalarioBruto.Size = new System.Drawing.Size(158, 20);
            this.nudSalarioBruto.TabIndex = 334;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 383);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 333;
            this.label3.Text = "Salário Bruto:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Black;
            this.panel4.Location = new System.Drawing.Point(407, 267);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1, 350);
            this.panel4.TabIndex = 328;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(560, 283);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(104, 24);
            this.label24.TabIndex = 332;
            this.label24.Text = "Descontos:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(158, 283);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(99, 24);
            this.label23.TabIndex = 331;
            this.label23.Text = "Proventos:";
            // 
            // nudAlimentacao
            // 
            this.nudAlimentacao.DecimalPlaces = 2;
            this.nudAlimentacao.Location = new System.Drawing.Point(583, 457);
            this.nudAlimentacao.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudAlimentacao.Name = "nudAlimentacao";
            this.nudAlimentacao.Size = new System.Drawing.Size(158, 20);
            this.nudAlimentacao.TabIndex = 321;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(467, 461);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 13);
            this.label15.TabIndex = 320;
            this.label15.Text = "Vale Alimentação:";
            // 
            // nudValeRefeicao
            // 
            this.nudValeRefeicao.DecimalPlaces = 2;
            this.nudValeRefeicao.Location = new System.Drawing.Point(583, 431);
            this.nudValeRefeicao.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudValeRefeicao.Name = "nudValeRefeicao";
            this.nudValeRefeicao.Size = new System.Drawing.Size(158, 20);
            this.nudValeRefeicao.TabIndex = 319;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(482, 433);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(91, 13);
            this.label16.TabIndex = 318;
            this.label16.Text = "Vale Refeição:";
            // 
            // nudValeTransporte
            // 
            this.nudValeTransporte.DecimalPlaces = 2;
            this.nudValeTransporte.Location = new System.Drawing.Point(583, 405);
            this.nudValeTransporte.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudValeTransporte.Name = "nudValeTransporte";
            this.nudValeTransporte.Size = new System.Drawing.Size(158, 20);
            this.nudValeTransporte.TabIndex = 317;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(476, 407);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 13);
            this.label17.TabIndex = 316;
            this.label17.Text = "Vale Transporte:";
            // 
            // nudOdontologico
            // 
            this.nudOdontologico.DecimalPlaces = 2;
            this.nudOdontologico.Location = new System.Drawing.Point(583, 379);
            this.nudOdontologico.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudOdontologico.Name = "nudOdontologico";
            this.nudOdontologico.Size = new System.Drawing.Size(158, 20);
            this.nudOdontologico.TabIndex = 315;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(456, 381);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(122, 13);
            this.label12.TabIndex = 314;
            this.label12.Text = "Plano Odontológico:";
            // 
            // nudPlanoSaude
            // 
            this.nudPlanoSaude.DecimalPlaces = 2;
            this.nudPlanoSaude.Location = new System.Drawing.Point(583, 353);
            this.nudPlanoSaude.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudPlanoSaude.Name = "nudPlanoSaude";
            this.nudPlanoSaude.Size = new System.Drawing.Size(158, 20);
            this.nudPlanoSaude.TabIndex = 313;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(473, 357);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 13);
            this.label13.TabIndex = 312;
            this.label13.Text = "Plano de Saúde:";
            // 
            // nudSeguroVida
            // 
            this.nudSeguroVida.DecimalPlaces = 2;
            this.nudSeguroVida.Location = new System.Drawing.Point(583, 327);
            this.nudSeguroVida.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudSeguroVida.Name = "nudSeguroVida";
            this.nudSeguroVida.Size = new System.Drawing.Size(158, 20);
            this.nudSeguroVida.TabIndex = 311;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(476, 329);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 310;
            this.label14.Text = "Seguro de Vida:";
            // 
            // nudPericulosidade
            // 
            this.nudPericulosidade.DecimalPlaces = 2;
            this.nudPericulosidade.Location = new System.Drawing.Point(152, 430);
            this.nudPericulosidade.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudPericulosidade.Name = "nudPericulosidade";
            this.nudPericulosidade.Size = new System.Drawing.Size(158, 20);
            this.nudPericulosidade.TabIndex = 307;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(52, 432);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 306;
            this.label7.Text = "Periculosidade:";
            // 
            // nudSalarioFamilia
            // 
            this.nudSalarioFamilia.DecimalPlaces = 2;
            this.nudSalarioFamilia.Location = new System.Drawing.Point(152, 404);
            this.nudSalarioFamilia.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudSalarioFamilia.Name = "nudSalarioFamilia";
            this.nudSalarioFamilia.Size = new System.Drawing.Size(158, 20);
            this.nudSalarioFamilia.TabIndex = 305;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(52, 406);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 304;
            this.label6.Text = "Salario Família:";
            // 
            // nudInss
            // 
            this.nudInss.DecimalPlaces = 2;
            this.nudInss.Location = new System.Drawing.Point(583, 483);
            this.nudInss.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudInss.Name = "nudInss";
            this.nudInss.Size = new System.Drawing.Size(158, 20);
            this.nudInss.TabIndex = 303;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(524, 485);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 300;
            this.label4.Text = "INSS:";
            // 
            // dgvLista
            // 
            this.dgvLista.AllowUserToAddRows = false;
            this.dgvLista.AllowUserToDeleteRows = false;
            this.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLista.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.Funcionario,
            this.Cargo,
            this.vl_salario_bruto,
            this.dt_pagamento,
            this.dt_adimissao,
            this.dt_demitido,
            this.vl_salario_familia,
            this.vl_periculosidade,
            this.qt_hora_extra,
            this.vl_gratificacao,
            this.ds_adiantamento_semanal,
            this.qt_falta,
            this.vl_fgts,
            this.vl_salario,
            this.vl_total_desconto,
            this.vl_total_proventos,
            this.vl_adicional_noturno,
            this.vl_plr,
            this.vl_inss,
            this.nm_empresa,
            this.ds_cpf,
            this.ds_endereco,
            this.ds_telefone,
            this.ds_email,
            this.vl_vt,
            this.vl_vale_alimentacao,
            this.vl_vale_refeicao,
            this.vl_plano_odonto,
            this.vl_plano_saude,
            this.vl_plano_vida});
            this.dgvLista.Location = new System.Drawing.Point(0, 648);
            this.dgvLista.Name = "dgvLista";
            this.dgvLista.ReadOnly = true;
            this.dgvLista.Size = new System.Drawing.Size(819, 104);
            this.dgvLista.TabIndex = 353;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "id_fopag";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            // 
            // Funcionario
            // 
            this.Funcionario.DataPropertyName = "nm_funcionario";
            this.Funcionario.HeaderText = " Nome do Funcionario";
            this.Funcionario.Name = "Funcionario";
            this.Funcionario.ReadOnly = true;
            // 
            // Cargo
            // 
            this.Cargo.DataPropertyName = "ds_cargo";
            this.Cargo.HeaderText = "Cargo";
            this.Cargo.Name = "Cargo";
            this.Cargo.ReadOnly = true;
            // 
            // vl_salario_bruto
            // 
            this.vl_salario_bruto.DataPropertyName = "vl_salario_bruto";
            this.vl_salario_bruto.HeaderText = "Salario Bruto";
            this.vl_salario_bruto.Name = "vl_salario_bruto";
            this.vl_salario_bruto.ReadOnly = true;
            // 
            // dt_pagamento
            // 
            this.dt_pagamento.DataPropertyName = "dt_pagamento";
            this.dt_pagamento.HeaderText = "Data de Pagamento";
            this.dt_pagamento.Name = "dt_pagamento";
            this.dt_pagamento.ReadOnly = true;
            // 
            // dt_adimissao
            // 
            this.dt_adimissao.DataPropertyName = "dt_adimissao";
            this.dt_adimissao.HeaderText = "Admissão";
            this.dt_adimissao.Name = "dt_adimissao";
            this.dt_adimissao.ReadOnly = true;
            // 
            // dt_demitido
            // 
            this.dt_demitido.DataPropertyName = "dt_demitido";
            this.dt_demitido.HeaderText = "Demissão";
            this.dt_demitido.Name = "dt_demitido";
            this.dt_demitido.ReadOnly = true;
            // 
            // vl_salario_familia
            // 
            this.vl_salario_familia.DataPropertyName = "vl_salario_familia";
            this.vl_salario_familia.HeaderText = "Salario Família";
            this.vl_salario_familia.Name = "vl_salario_familia";
            this.vl_salario_familia.ReadOnly = true;
            // 
            // vl_periculosidade
            // 
            this.vl_periculosidade.DataPropertyName = "vl_periculosidade";
            this.vl_periculosidade.HeaderText = "Periculosidade";
            this.vl_periculosidade.Name = "vl_periculosidade";
            this.vl_periculosidade.ReadOnly = true;
            // 
            // qt_hora_extra
            // 
            this.qt_hora_extra.DataPropertyName = "qt_hora_extra";
            this.qt_hora_extra.HeaderText = "Hora Extra";
            this.qt_hora_extra.Name = "qt_hora_extra";
            this.qt_hora_extra.ReadOnly = true;
            // 
            // vl_gratificacao
            // 
            this.vl_gratificacao.DataPropertyName = "vl_gratificacao";
            this.vl_gratificacao.HeaderText = "Gratificação";
            this.vl_gratificacao.Name = "vl_gratificacao";
            this.vl_gratificacao.ReadOnly = true;
            // 
            // ds_adiantamento_semanal
            // 
            this.ds_adiantamento_semanal.DataPropertyName = "ds_adiantamento_semanal";
            this.ds_adiantamento_semanal.HeaderText = "DSR";
            this.ds_adiantamento_semanal.Name = "ds_adiantamento_semanal";
            this.ds_adiantamento_semanal.ReadOnly = true;
            // 
            // qt_falta
            // 
            this.qt_falta.DataPropertyName = "qt_falta";
            this.qt_falta.HeaderText = "Faltas";
            this.qt_falta.Name = "qt_falta";
            this.qt_falta.ReadOnly = true;
            // 
            // vl_fgts
            // 
            this.vl_fgts.DataPropertyName = "vl_fgts";
            this.vl_fgts.HeaderText = "FGTS";
            this.vl_fgts.Name = "vl_fgts";
            this.vl_fgts.ReadOnly = true;
            // 
            // vl_salario
            // 
            this.vl_salario.DataPropertyName = "vl_salario";
            this.vl_salario.HeaderText = "Salario Líquido";
            this.vl_salario.Name = "vl_salario";
            this.vl_salario.ReadOnly = true;
            // 
            // vl_total_desconto
            // 
            this.vl_total_desconto.DataPropertyName = "vl_total_desconto";
            this.vl_total_desconto.HeaderText = "Total Descontos";
            this.vl_total_desconto.Name = "vl_total_desconto";
            this.vl_total_desconto.ReadOnly = true;
            // 
            // vl_total_proventos
            // 
            this.vl_total_proventos.DataPropertyName = "vl_total_proventos";
            this.vl_total_proventos.HeaderText = "Total Proventos";
            this.vl_total_proventos.Name = "vl_total_proventos";
            this.vl_total_proventos.ReadOnly = true;
            // 
            // vl_adicional_noturno
            // 
            this.vl_adicional_noturno.DataPropertyName = "vl_adicional_noturno";
            this.vl_adicional_noturno.HeaderText = "Adicional Noturno";
            this.vl_adicional_noturno.Name = "vl_adicional_noturno";
            this.vl_adicional_noturno.ReadOnly = true;
            // 
            // vl_plr
            // 
            this.vl_plr.DataPropertyName = "vl_plr";
            this.vl_plr.HeaderText = "PLR";
            this.vl_plr.Name = "vl_plr";
            this.vl_plr.ReadOnly = true;
            // 
            // vl_inss
            // 
            this.vl_inss.DataPropertyName = "vl_inss";
            this.vl_inss.HeaderText = "INSS";
            this.vl_inss.Name = "vl_inss";
            this.vl_inss.ReadOnly = true;
            // 
            // nm_empresa
            // 
            this.nm_empresa.DataPropertyName = "vl_salario_familia";
            this.nm_empresa.HeaderText = "Empresa";
            this.nm_empresa.Name = "nm_empresa";
            this.nm_empresa.ReadOnly = true;
            // 
            // ds_cpf
            // 
            this.ds_cpf.DataPropertyName = "ds_cpf";
            this.ds_cpf.HeaderText = "CPF";
            this.ds_cpf.Name = "ds_cpf";
            this.ds_cpf.ReadOnly = true;
            // 
            // ds_endereco
            // 
            this.ds_endereco.DataPropertyName = "ds_endereco";
            this.ds_endereco.HeaderText = "Endereço";
            this.ds_endereco.Name = "ds_endereco";
            this.ds_endereco.ReadOnly = true;
            // 
            // ds_telefone
            // 
            this.ds_telefone.DataPropertyName = "ds_telefone";
            this.ds_telefone.HeaderText = "Telefone";
            this.ds_telefone.Name = "ds_telefone";
            this.ds_telefone.ReadOnly = true;
            // 
            // ds_email
            // 
            this.ds_email.DataPropertyName = "ds_email";
            this.ds_email.HeaderText = "E-mail";
            this.ds_email.Name = "ds_email";
            this.ds_email.ReadOnly = true;
            // 
            // vl_vt
            // 
            this.vl_vt.DataPropertyName = "vl_vt";
            this.vl_vt.HeaderText = "Vale Transporte";
            this.vl_vt.Name = "vl_vt";
            this.vl_vt.ReadOnly = true;
            // 
            // vl_vale_alimentacao
            // 
            this.vl_vale_alimentacao.DataPropertyName = "vl_vale_alimentacao";
            this.vl_vale_alimentacao.HeaderText = "Vale Alimentação";
            this.vl_vale_alimentacao.Name = "vl_vale_alimentacao";
            this.vl_vale_alimentacao.ReadOnly = true;
            // 
            // vl_vale_refeicao
            // 
            this.vl_vale_refeicao.DataPropertyName = "vl_vale_refeicao";
            this.vl_vale_refeicao.HeaderText = "Vale Refeição";
            this.vl_vale_refeicao.Name = "vl_vale_refeicao";
            this.vl_vale_refeicao.ReadOnly = true;
            // 
            // vl_plano_odonto
            // 
            this.vl_plano_odonto.DataPropertyName = "vl_plano_odonto";
            this.vl_plano_odonto.HeaderText = "Plano Odontológico";
            this.vl_plano_odonto.Name = "vl_plano_odonto";
            this.vl_plano_odonto.ReadOnly = true;
            // 
            // vl_plano_saude
            // 
            this.vl_plano_saude.DataPropertyName = "vl_plano_saude";
            this.vl_plano_saude.HeaderText = "Plano de Saúde";
            this.vl_plano_saude.Name = "vl_plano_saude";
            this.vl_plano_saude.ReadOnly = true;
            // 
            // vl_plano_vida
            // 
            this.vl_plano_vida.DataPropertyName = "vl_plano_vida";
            this.vl_plano_vida.HeaderText = "Seguro de Vida";
            this.vl_plano_vida.Name = "vl_plano_vida";
            this.vl_plano_vida.ReadOnly = true;
            // 
            // lblTotalProventos
            // 
            this.lblTotalProventos.AutoSize = true;
            this.lblTotalProventos.Location = new System.Drawing.Point(246, 582);
            this.lblTotalProventos.Name = "lblTotalProventos";
            this.lblTotalProventos.Size = new System.Drawing.Size(40, 13);
            this.lblTotalProventos.TabIndex = 354;
            this.lblTotalProventos.Text = "Total:";
            // 
            // lblSalarioLiquido
            // 
            this.lblSalarioLiquido.AutoSize = true;
            this.lblSalarioLiquido.Location = new System.Drawing.Point(366, 627);
            this.lblSalarioLiquido.Name = "lblSalarioLiquido";
            this.lblSalarioLiquido.Size = new System.Drawing.Size(97, 13);
            this.lblSalarioLiquido.TabIndex = 355;
            this.lblSalarioLiquido.Text = "Salário Líquido:";
            // 
            // txtCnpj
            // 
            this.txtCnpj.Location = new System.Drawing.Point(90, 130);
            this.txtCnpj.Name = "txtCnpj";
            this.txtCnpj.Size = new System.Drawing.Size(158, 20);
            this.txtCnpj.TabIndex = 339;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(45, 133);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 13);
            this.label11.TabIndex = 340;
            this.label11.Text = "CNPJ:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(600, 173);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(64, 13);
            this.label22.TabIndex = 330;
            this.label22.Text = "Admissão:";
            // 
            // dtpAdmissao
            // 
            this.dtpAdmissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAdmissao.Location = new System.Drawing.Point(673, 168);
            this.dtpAdmissao.Name = "dtpAdmissao";
            this.dtpAdmissao.Size = new System.Drawing.Size(96, 20);
            this.dtpAdmissao.TabIndex = 329;
            this.dtpAdmissao.Value = new System.DateTime(2019, 1, 1, 0, 0, 0, 0);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(45, 193);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 13);
            this.label21.TabIndex = 327;
            this.label21.Text = "Cargo:";
            // 
            // cboCargo
            // 
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Location = new System.Drawing.Point(91, 190);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(158, 21);
            this.cboCargo.TabIndex = 325;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Location = new System.Drawing.Point(11, 160);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(782, 1);
            this.panel2.TabIndex = 324;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.Location = new System.Drawing.Point(11, 250);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(782, 1);
            this.panel3.TabIndex = 326;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(545, 107);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 13);
            this.label18.TabIndex = 323;
            this.label18.Text = "Data de Pagamento:";
            // 
            // dtpDataPagamento
            // 
            this.dtpDataPagamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataPagamento.Location = new System.Drawing.Point(673, 101);
            this.dtpDataPagamento.Name = "dtpDataPagamento";
            this.dtpDataPagamento.Size = new System.Drawing.Size(96, 20);
            this.dtpDataPagamento.TabIndex = 322;
            this.dtpDataPagamento.Value = new System.DateTime(2019, 1, 1, 0, 0, 0, 0);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(7, 93);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 1);
            this.panel1.TabIndex = 347;
            // 
            // nudRenumeracao
            // 
            this.nudRenumeracao.DecimalPlaces = 2;
            this.nudRenumeracao.Location = new System.Drawing.Point(108, 217);
            this.nudRenumeracao.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.nudRenumeracao.Name = "nudRenumeracao";
            this.nudRenumeracao.Size = new System.Drawing.Size(140, 20);
            this.nudRenumeracao.TabIndex = 309;
            // 
            // cboNomeBuscar
            // 
            this.cboNomeBuscar.FormattingEnabled = true;
            this.cboNomeBuscar.Location = new System.Drawing.Point(82, 19);
            this.cboNomeBuscar.Name = "cboNomeBuscar";
            this.cboNomeBuscar.Size = new System.Drawing.Size(176, 21);
            this.cboNomeBuscar.TabIndex = 348;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 224);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 13);
            this.label9.TabIndex = 308;
            this.label9.Text = "Renumeração:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(41, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 349;
            this.label20.Text = "Nome:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(603, 209);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 302;
            this.label5.Text = "Demissão:";
            // 
            // dtpDataPagamentoBuscar
            // 
            this.dtpDataPagamentoBuscar.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataPagamentoBuscar.Location = new System.Drawing.Point(162, 55);
            this.dtpDataPagamentoBuscar.Name = "dtpDataPagamentoBuscar";
            this.dtpDataPagamentoBuscar.Size = new System.Drawing.Size(96, 20);
            this.dtpDataPagamentoBuscar.TabIndex = 350;
            this.dtpDataPagamentoBuscar.Value = new System.DateTime(2019, 1, 1, 0, 0, 0, 0);
            // 
            // dtpDemissao
            // 
            this.dtpDemissao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDemissao.Location = new System.Drawing.Point(673, 204);
            this.dtpDemissao.Name = "dtpDemissao";
            this.dtpDemissao.Size = new System.Drawing.Size(96, 20);
            this.dtpDemissao.TabIndex = 301;
            this.dtpDemissao.Value = new System.DateTime(2019, 1, 1, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 13);
            this.label2.TabIndex = 351;
            this.label2.Text = "Data de Pagamento:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(47, 167);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(43, 13);
            this.label19.TabIndex = 299;
            this.label19.Text = "Nome:";
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.DarkRed;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.White;
            this.btnBuscar.Location = new System.Drawing.Point(273, 31);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(516, 33);
            this.btnBuscar.TabIndex = 352;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(91, 164);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(158, 20);
            this.txtNome.TabIndex = 298;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 297;
            this.label1.Text = "Empresa:";
            // 
            // txtEmpresa
            // 
            this.txtEmpresa.Location = new System.Drawing.Point(90, 100);
            this.txtEmpresa.Name = "txtEmpresa";
            this.txtEmpresa.Size = new System.Drawing.Size(158, 20);
            this.txtEmpresa.TabIndex = 296;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkRed;
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.btnMinimizar);
            this.panel5.Controls.Add(this.btnFechar);
            this.panel5.Controls.Add(this.picVoltar);
            this.panel5.Location = new System.Drawing.Point(-3, -1);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(890, 340);
            this.panel5.TabIndex = 357;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(304, 10);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(371, 29);
            this.label28.TabIndex = 37;
            this.label28.Text = "Consultar Folha de pagamento";
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.Location = new System.Drawing.Point(796, 0);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(19, 26);
            this.btnMinimizar.TabIndex = 33;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(821, 5);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(19, 23);
            this.btnFechar.TabIndex = 34;
            this.btnFechar.Text = "X";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.button1_Click);
            // 
            // picVoltar
            // 
            this.picVoltar.BackColor = System.Drawing.Color.DarkRed;
            this.picVoltar.Image = global::The_Barber_s_House.Properties.Resources.Previous_free_vector_icons_designed_by_Gregor_Cresnar;
            this.picVoltar.Location = new System.Drawing.Point(52, 7);
            this.picVoltar.Name = "picVoltar";
            this.picVoltar.Size = new System.Drawing.Size(34, 32);
            this.picVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picVoltar.TabIndex = 31;
            this.picVoltar.TabStop = false;
            this.picVoltar.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.nudFaltas);
            this.groupBox1.Controls.Add(this.lblTotaldedescontos);
            this.groupBox1.Controls.Add(this.cboNomeBuscar);
            this.groupBox1.Controls.Add(this.nudFgts);
            this.groupBox1.Controls.Add(this.lblSalarioLiquido);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.lblTotalProventos);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.txtEmpresa);
            this.groupBox1.Controls.Add(this.nudAlimentacao);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Controls.Add(this.nudValeRefeicao);
            this.groupBox1.Controls.Add(this.dgvLista);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.btnBuscar);
            this.groupBox1.Controls.Add(this.nudValeTransporte);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nudOdontologico);
            this.groupBox1.Controls.Add(this.panel4);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.nudDsr);
            this.groupBox1.Controls.Add(this.nudPlanoSaude);
            this.groupBox1.Controls.Add(this.dtpDemissao);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.nudSeguroVida);
            this.groupBox1.Controls.Add(this.dtpDataPagamentoBuscar);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.nudGratificacao);
            this.groupBox1.Controls.Add(this.nudInss);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.nudHoraExtra);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.nudRenumeracao);
            this.groupBox1.Controls.Add(this.nudSalarioBruto);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dtpDataPagamento);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.cboCargo);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.dtpAdmissao);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.txtCnpj);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.nudSalarioFamilia);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.nudPericulosidade);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(15, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(819, 732);
            this.groupBox1.TabIndex = 358;
            this.groupBox1.TabStop = false;
            // 
            // nudFaltas
            // 
            this.nudFaltas.Location = new System.Drawing.Point(583, 508);
            this.nudFaltas.Name = "nudFaltas";
            this.nudFaltas.Size = new System.Drawing.Size(158, 20);
            this.nudFaltas.TabIndex = 360;
            // 
            // lblTotaldedescontos
            // 
            this.lblTotaldedescontos.AutoSize = true;
            this.lblTotaldedescontos.Location = new System.Drawing.Point(495, 582);
            this.lblTotaldedescontos.Name = "lblTotaldedescontos";
            this.lblTotaldedescontos.Size = new System.Drawing.Size(40, 13);
            this.lblTotaldedescontos.TabIndex = 359;
            this.lblTotaldedescontos.Text = "Total:";
            // 
            // ConsultarFolhadePagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 808);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ConsultarFolhadePagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConsultarFolhadePagamento";
            ((System.ComponentModel.ISupportInitialize)(this.nudFgts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDsr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGratificacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHoraExtra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioBruto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudAlimentacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeicao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTransporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOdontologico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoSaude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSeguroVida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPericulosidade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioFamilia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudInss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRenumeracao)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picVoltar)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFaltas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.NumericUpDown nudFgts;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown nudDsr;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown nudGratificacao;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudHoraExtra;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudSalarioBruto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown nudAlimentacao;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nudValeRefeicao;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudValeTransporte;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown nudOdontologico;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudPlanoSaude;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown nudSeguroVida;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudPericulosidade;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudSalarioFamilia;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudInss;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvLista;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cargo;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_salario_bruto;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_pagamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_adimissao;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_demitido;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_salario_familia;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_periculosidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn qt_hora_extra;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_gratificacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_adiantamento_semanal;
        private System.Windows.Forms.DataGridViewTextBoxColumn qt_falta;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_fgts;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_salario;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_total_desconto;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_total_proventos;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_adicional_noturno;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_plr;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_inss;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_empresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_cpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_endereco;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_email;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_vt;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_vale_alimentacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_vale_refeicao;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_plano_odonto;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_plano_saude;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_plano_vida;
        private System.Windows.Forms.Label lblTotalProventos;
        private System.Windows.Forms.Label lblSalarioLiquido;
        private System.Windows.Forms.TextBox txtCnpj;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DateTimePicker dtpAdmissao;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dtpDataPagamento;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown nudRenumeracao;
        private System.Windows.Forms.ComboBox cboNomeBuscar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpDataPagamentoBuscar;
        private System.Windows.Forms.DateTimePicker dtpDemissao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmpresa;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.PictureBox picVoltar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTotaldedescontos;
        private System.Windows.Forms.NumericUpDown nudFaltas;
    }
}