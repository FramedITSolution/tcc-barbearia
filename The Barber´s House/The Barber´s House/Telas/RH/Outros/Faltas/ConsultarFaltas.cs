﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Outros.Faltas
{
    public partial class ConsultarFaltas : Form
    {
        public ConsultarFaltas()
        {
            InitializeComponent();
            dgvFaltas.AutoGenerateColumns = false;
            CarregarCombo();
        }

        Business.RH.Outros.BusinessFaltas db = new Business.RH.Outros.BusinessFaltas();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvFaltas.DataSource = db.ListarFaltas();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_funcionario model = cboFunc.SelectedItem as Database.Entity.tb_funcionario;

                dgvFaltas.DataSource = db.ConsultarFaltasPorFunc(model.id_funcionario);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void CarregarCombo()
        {
            Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();

            cboFunc.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboFunc.DataSource = func.ListarFuncionarios();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
