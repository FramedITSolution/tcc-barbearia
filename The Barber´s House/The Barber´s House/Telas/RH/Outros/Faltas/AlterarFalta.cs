﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Outros.Faltas
{
    public partial class AlterarFalta : Form
    {
        public AlterarFalta()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.RH.Outros.BusinessFaltas db = new Business.RH.Outros.BusinessFaltas();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                //Instanciando o modelo
                Database.Entity.tb_faltas model = new Database.Entity.tb_faltas();
                //Convertendo os itens do Combo para modelo
                Database.Entity.tb_funcionario func = cboNomedofuncionario.SelectedItem as Database.Entity.tb_funcionario;
                //Carregando o modelo
                model.id_faltas = Convert.ToInt32(cboIDFalta.Text);
                model.id_funcionario = func.id_funcionario;
                model.dt_falta = dtpFalta.Value.Date;
                //Alterando a falta
                db.AlterarFalta(model);
                //Exibindo a mensagem de confirmação
                MessageBox.Show("Falta alterada com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void cboNomedofuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_funcionario func = cboNomedofuncionario.SelectedItem as Database.Entity.tb_funcionario;

                cboIDFalta.DisplayMember = nameof(Database.Entity.tb_faltas.id_faltas);
                cboIDFalta.DataSource = db.ConsultarFaltasPorFunc(func.id_funcionario);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void cboIDFalta_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_faltas faltamodel = cboIDFalta.SelectedItem as Database.Entity.tb_faltas;

                dtpFalta.Value = faltamodel.dt_falta.Date;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void CarregarCombo()
        {
            try
            {
                Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();

                cboNomedofuncionario.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
                cboNomedofuncionario.DataSource = func.ListarFuncionarios();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
       
    }
}
