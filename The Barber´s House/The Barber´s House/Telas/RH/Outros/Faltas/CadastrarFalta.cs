﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Outros.Faltas
{
    public partial class CadastrarFalta : Form
    {
        public CadastrarFalta()
        {
            InitializeComponent();
            CarregarCombo();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Business.RH.Outros.BusinessFaltas business = new Business.RH.Outros.BusinessFaltas();
                Database.Entity.tb_funcionario funcModel = cboFuncionário.SelectedItem as Database.Entity.tb_funcionario;
                Database.Entity.tb_faltas falta = new Database.Entity.tb_faltas();
                falta.id_funcionario = funcModel.id_funcionario;
                falta.dt_falta = dtpFalta.Value;
                business.NovaFalta(falta);

                MessageBox.Show("Falta registrada com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void CarregarCombo()
        {
            try
            {
                Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();

                cboFuncionário.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
                cboFuncionário.DataSource = func.ListarFuncionarios();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }            
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
