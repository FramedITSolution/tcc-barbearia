﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Outros.Atrasos
{
    public partial class ConsultarAtrasos : Form
    {
        public ConsultarAtrasos()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.RH.Outros.BusinessAtrasos business = new Business.RH.Outros.BusinessAtrasos();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvAtrasos.DataSource = business.ListarAtrasos();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_funcionario funcModel = cboFunc.SelectedItem as Database.Entity.tb_funcionario;
                dgvAtrasos.DataSource = business.ConsultarAtrasosPorFunc(funcModel.id_funcionario);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void CarregarCombo()
        {
            try
            {
                Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();

                cboFunc.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
                cboFunc.DataSource = func.ListarFuncionarios();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
