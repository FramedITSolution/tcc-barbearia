﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.RH.Outros.Atrasos
{
    public partial class AlterarAtraso : Form
    {
        public AlterarAtraso()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.RH.Outros.BusinessAtrasos db = new Business.RH.Outros.BusinessAtrasos();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                //Instanciando o modelo
                Database.Entity.tb_atrasos model = new Database.Entity.tb_atrasos();
                //Convertendo os itens do Combo para modelo
                Database.Entity.tb_funcionario func = cboNomedofuncionario.SelectedItem as Database.Entity.tb_funcionario;
                //Carregando o modelo
                model.id_atraso = Convert.ToInt32(cboIDAtraso.Text);
                model.id_funcionario = func.id_funcionario;
                model.dt_atraso = dtpAtraso.Value.Date;
                model.ds_tempo_de_atraso = TimeSpan.Parse(txtTempoAtraso.Text);
                //Alterando o Atraso
                db.AlterarAtraso(model);
                //Exibindo a mensagem de confirmação
                MessageBox.Show("Atraso alterado com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void cboNomedofuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_funcionario func = cboNomedofuncionario.SelectedItem as Database.Entity.tb_funcionario;

                cboIDAtraso.DisplayMember = nameof(Database.Entity.tb_atrasos.id_atraso);
                cboIDAtraso.DataSource = db.ConsultarAtrasosPorFunc(func.id_funcionario);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void cboIDFalta_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_atrasos atrasomodel = cboIDAtraso.SelectedItem as Database.Entity.tb_atrasos;

                dtpAtraso.Value = atrasomodel.dt_atraso.Date;
                txtTempoAtraso.Text = Convert.ToString(atrasomodel.ds_tempo_de_atraso);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void CarregarCombo()
        {
            try
            {
                Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();

                cboNomedofuncionario.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
                cboNomedofuncionario.DataSource = func.ListarFuncionarios();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Telas.Outros.Menu menu = new Telas.Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
       
    }
}
