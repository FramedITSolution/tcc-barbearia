﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Estoque
{
    public partial class AlterarEstoque : Form
    {
        public AlterarEstoque()
        {
            InitializeComponent();
            CarregarCombo();
        }
        
        Business.Estoque.BusinessEstoque estoque = new Business.Estoque.BusinessEstoque();
        Business.Produtos.BusinessProdutos_de_Compras compras = new Business.Produtos.BusinessProdutos_de_Compras();
        Business.Outros.Custos.BusinessCustos db = new Business.Outros.Custos.BusinessCustos();
        Business.Fornecedor.BusinessFornecedor fornecedor = new Business.Fornecedor.BusinessFornecedor();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Alterar();
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_produto_compra combo = cboProduto.SelectedItem as Database.Entity.tb_produto_compra;

                Database.Entity.tb_estoque model = estoque.ConsultarPorIDP(combo.id_produto_compra);
                Database.Entity.tb_custo_item custos = db.ConsultarPorIDP(combo.id_produto_compra);
                Database.Entity.tb_fornecedor forn = fornecedor.ConsultarPorId(combo.id_fornecedor);
                Database.Entity.tb_custo tb = db.ConsultarPorIDF(forn.id_fornecedor);

                nudQuantidade.Value = model.qt_estoque;
                txtDetalhe.Text = model.ds_detalhe;
                txtSituação.Text = model.ds_situacao_produto;
                dtpValidade.Value = model.dt_validade_prevista;
                chkAbastecer.Checked = model.bt_abastecer;
                chkUrgente.Checked = model.bt_urgente;
                cboCategoria.Text = combo.ds_categoria;
                nudValorUnitário.Value = custos.vl_unitario;
                dtpDatadeCompra.Value = tb.dt_compra;
                dtpEntrega.Value = tb.dt_previsao_de_entrega;
                
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }

        }

        private void CarregarCombo()
        {
            cboProduto.DisplayMember = nameof(Database.Entity.tb_produto_compra.nm_produto);
            cboProduto.DataSource = compras.ListarProdutos();


            cboCategoria.DisplayMember = nameof(Database.Entity.tb_produto_compra.ds_categoria);
            cboCategoria.DataSource = compras.ListarProdutos();

            List<Database.Entity.tb_fornecedor> fornecedors = fornecedor.ListarFornecedores();
            cboFornecedor.DisplayMember = nameof(Database.Entity.tb_fornecedor.nm_fantasia);
            cboFornecedor.DataSource = fornecedors;
        }

        private void btnSalvar_KeyPress(object sender, KeyPressEventArgs e)
        {
            Alterar();
        }

        private void Alterar()
        {
            try
            {
                Database.Entity.tb_estoque tb_Estoque = new Database.Entity.tb_estoque();
                Database.Entity.tb_custo tb_Custo = new Database.Entity.tb_custo();
                Database.Entity.tb_custo_item tb_Custo_Item = new Database.Entity.tb_custo_item();
                Database.Entity.tb_produto_compra tb_Produto_Compra = new Database.Entity.tb_produto_compra();
            Database.Entity.tb_fornecedor tb_Forn = cboFornecedor.SelectedItem as Database.Entity.tb_fornecedor;
            Database.Entity.tb_produto_compra produto = cboProduto.SelectedItem as Database.Entity.tb_produto_compra;

            Database.Entity.tb_custo_item custo_item = db.ConsultarPorIDP(produto.id_produto_compra);
            Database.Entity.tb_custo custo = db.ConsultarPorIDF(produto.id_fornecedor);

            tb_Produto_Compra.nm_produto = cboProduto.Text;
            tb_Produto_Compra.ds_categoria = cboCategoria.Text;
            tb_Produto_Compra.vl_preco = nudValorUnitário.Value;
            tb_Produto_Compra.id_fornecedor = tb_Forn.id_fornecedor;
            compras.AlterarProduto(tb_Produto_Compra);
            tb_Estoque.id_produto_compra = produto.id_produto_compra;
            tb_Estoque.qt_estoque = Convert.ToInt32(nudQuantidade.Value);
            tb_Estoque.vl_total = nudValorUnitário.Value * nudQuantidade.Value;
            tb_Estoque.dt_validade_prevista = dtpValidade.Value.Date;
            tb_Estoque.bt_urgente = chkUrgente.Checked;
            tb_Estoque.bt_abastecer = chkAbastecer.Checked;
            tb_Estoque.ds_detalhe = txtDetalhe.Text;
            tb_Estoque.ds_situacao_produto = txtSituação.Text;
            estoque.AlterarEstoque(tb_Estoque);
            tb_Custo.id_custo = custo.id_custo;
            tb_Custo.dt_compra = dtpDatadeCompra.Value.Date;
            tb_Custo.dt_previsao_de_entrega = dtpEntrega.Value.Date;
            tb_Custo.id_fornecedor = tb_Forn.id_fornecedor;
            tb_Custo.qt_compra = Convert.ToInt32(nudQuantidade.Value);
            tb_Custo.vl_total = nudValorUnitário.Value * nudQuantidade.Value;
            db.AlterarCusto(tb_Custo);
            tb_Custo_Item.id_custo_item = custo_item.id_custo_item;
            tb_Custo_Item.vl_unitario = nudValorUnitário.Value;
            tb_Custo_Item.id_custo = custo.id_custo;
            tb_Custo_Item.id_produto_compra = produto.id_produto_compra;                     
            db.AlterarCustoItem(tb_Custo_Item);

            MessageBox.Show("Alteração feita com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();            
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
