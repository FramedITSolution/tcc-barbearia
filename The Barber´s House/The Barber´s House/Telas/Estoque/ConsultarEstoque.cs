﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Estoque
{
    public partial class ConsultarEstoque : Form
    {
        public ConsultarEstoque()
        {
            InitializeComponent();
            dgvEstoque.AutoGenerateColumns = false;
        }

        Business.Estoque.BusinessEstoque estoque = new Business.Estoque.BusinessEstoque();

        private void LblFechar_Click(object sender, EventArgs e)
        {
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
            Hide();
        }

        private void LblMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
            Hide();
        }

        private void btnListar_Click_1(object sender, EventArgs e)
        {
            try
            {
                int n = 0;
                List<Database.Entity.tb_estoque> lista_estoque = estoque.ListaEstoque();
                foreach (Database.Entity.tb_estoque resposta in lista_estoque)
                {
                    if (resposta.qt_estoque <= 4)
                    {
                        dgvEstoque.Rows[n].DefaultCellStyle.BackColor = Color.Red;
                        dgvEstoque.DataSource = lista_estoque;
                    }
                    n++;
                    dgvEstoque.DataSource = lista_estoque;
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudIDProduto.Value);
                List<Database.Entity.tb_estoque> lista_estoque = estoque.Consultar_ID(id); 
                foreach (Database.Entity.tb_estoque resposta in lista_estoque)
                {
                    if (resposta.qt_estoque <= 4)
                    {
                        dgvEstoque.DefaultCellStyle.BackColor = Color.Red;
                        dgvEstoque.DataSource = lista_estoque;
                    }
                    else
                    {
                        dgvEstoque.DefaultCellStyle.BackColor = Color.White;
                        dgvEstoque.DataSource = lista_estoque;
                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
    }
}
