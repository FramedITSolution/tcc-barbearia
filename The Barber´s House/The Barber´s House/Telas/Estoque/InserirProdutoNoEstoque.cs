﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Estoque
{
    public partial class InserirProdutoNoEstoque : Form
    {
        public InserirProdutoNoEstoque()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Database.Fornecedor.DatabaseFornecedor forn = new Database.Fornecedor.DatabaseFornecedor();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Business.Produtos.BusinessProdutos_de_Compras compras = new Business.Produtos.BusinessProdutos_de_Compras();
                Business.Estoque.BusinessEstoque estoquebusiness = new Business.Estoque.BusinessEstoque();
                Business.Outros.Custos.BusinessCustos businesscustos = new Business.Outros.Custos.BusinessCustos();
                Database.Entity.tb_produto_compra model = new Database.Entity.tb_produto_compra();
                Database.Entity.tb_estoque estoque = new Database.Entity.tb_estoque();
                Database.Entity.tb_custo custo = new Database.Entity.tb_custo();
                Database.Entity.tb_custo_item custo_item = new Database.Entity.tb_custo_item();
                Database.Entity.tb_fornecedor fornecedor = forn.ConsultarPorNome(cboFornecedor.Text);
            

                model.nm_produto = txtNomedoProduto.Text;
                model.ds_categoria = txtCategoria.Text;
                model.id_fornecedor = fornecedor.id_fornecedor;
                model.vl_preco = nudValorUnitário.Value;                          
                compras.NovoProduto(model);
                Database.Entity.tb_produto_compra produtos = compras.ConsultarPorNome(txtNomedoProduto.Text);
                estoque.id_produto_compra = produtos.id_produto_compra;
                estoque.qt_estoque = Convert.ToInt32(nudQuantidade.Value);
                estoque.vl_total = nudValorUnitário.Value * nudQuantidade.Value;
                estoque.dt_validade_prevista = dtpDatadeValidade.Value.Date;
                estoque.ds_situacao_produto = txtSituacao.Text;
                estoque.ds_detalhe = txtDetalhe.Text;
                estoquebusiness.InserirNoEstoque(estoque);                
                custo.dt_compra = dtpDatadeCompra.Value.Date;
                custo.dt_previsao_de_entrega = dtpEntrega.Value.Date;
                custo.qt_compra = Convert.ToInt32(nudQuantidade.Value);
                custo.id_fornecedor = fornecedor.id_fornecedor;
                custo.vl_total = nudValorUnitário.Value * nudQuantidade.Value;
                businesscustos.InserirCusto(custo);
                custo_item.id_produto_compra = model.id_produto_compra;
                custo_item.vl_unitario = nudValorUnitário.Value;            
                custo_item.id_custo = businesscustos.ConsultarPorIDF(fornecedor.id_fornecedor).id_custo;                                            
                businesscustos.InserirCustoItem(custo_item);

                MessageBox.Show("Produto registrado com sucesso");
            }
            catch (ArgumentException ex)
            {
               MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }

        }

        private void CarregarCombo()
        {
            List<Database.Entity.tb_fornecedor> lista = forn.ListarFornecedores();
            cboFornecedor.DisplayMember = nameof(Database.Entity.tb_fornecedor.nm_fantasia);
            cboFornecedor.DataSource = lista;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
