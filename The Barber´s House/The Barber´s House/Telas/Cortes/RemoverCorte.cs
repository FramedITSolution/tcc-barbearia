﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Cortes
{
    public partial class RemoverCorte : Form
    {
        public RemoverCorte()
        {
            InitializeComponent();
            CarregarId();
        }

        Database.Entity.tb_corte corte = new Database.Entity.tb_corte();
        Business.Cortes.BusinessCortes business = new Business.Cortes.BusinessCortes();
        Business.Serviços.BusinessServiços servicos = new Business.Serviços.BusinessServiços();
        Business.Serviços.BusinessServiçosPorCliente serviçosPorCliente = new Business.Serviços.BusinessServiçosPorCliente();

        private void btnRemover_Click(object sender, EventArgs e)
        {
            Remover();
        }

        private void Remover()
        {
            try
            {
                Database.Entity.tb_servico model = servicos.ConsultarPorCorte(Convert.ToInt32(cboIdCorte.Text));
                corte.id_corte = Convert.ToInt32(cboIdCorte.Text);
                corte.nm_corte = txtCorte.Text;
                corte.vl_corte = nudPreço.Value;
                if(model != null)
                {
                    serviçosPorCliente.RemoverServiçoPorClientePorID(model.id_servico);
                }
                business.RemoverServiçoPorCorte(corte.id_corte);
                business.RemoverCorte(corte);

                MessageBox.Show("Corte removido com sucesso");
                CarregarId();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //catch (Exception)
            //{
            //    MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            //}
        }

        private void CarregarId()
        {
            List<Database.Entity.tb_corte> lista = business.ListarCortes();
            cboIdCorte.ValueMember = nameof(Database.Entity.tb_corte.id_corte);
            cboIdCorte.DataSource = lista;
        }

        private void btnRemover_KeyPress(object sender, KeyPressEventArgs e)
        {
            Remover();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void cboIdCorte_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_corte db = cboIdCorte.SelectedItem as Database.Entity.tb_corte;
            txtCorte.Text = db.nm_corte;
            nudPreço.Value = db.vl_corte;            
        }
    }
}
