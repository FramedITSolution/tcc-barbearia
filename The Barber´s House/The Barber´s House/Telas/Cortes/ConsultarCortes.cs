﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Cortes
{
    public partial class ConsultarCortes : Form
    {
        public ConsultarCortes()
        {
            InitializeComponent();
            dgvCorte.AutoGenerateColumns = false;
        }

        Business.Cortes.BusinessCortes cortes = new Business.Cortes.BusinessCortes();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvCorte.DataSource = cortes.ListarCortes();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudIDProduto.Value);
                dgvCorte.DataSource = cortes.ConsultarPorID(id);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu start = new Outros.Menu();
            start.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;

        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu start = new Outros.Menu();
            start.Show();
        }
    }
}
