﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Cortes
{
    public partial class AlterarCorte : Form
    {
        public AlterarCorte()
        {
            InitializeComponent();
            CarregarId();
        }

        Database.Entity.tb_corte corte = new Database.Entity.tb_corte();
        Business.Cortes.BusinessCortes business = new Business.Cortes.BusinessCortes();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            SalvarAlteração();
        }

        private void CarregarId()
        {
            List<Database.Entity.tb_corte> lista = business.ListarCortes();
            cboCorte.ValueMember = nameof(Database.Entity.tb_corte.id_corte);
            cboCorte.DataSource = lista;
        }

        private void SalvarAlteração()
        {
            try
            {
                Database.Entity.tb_corte db = cboCorte.SelectedItem as Database.Entity.tb_corte;
                corte.id_corte = db.id_corte;
                corte.nm_corte = txtNomeCorte.Text;
                corte.vl_corte = nudPreço.Value;

                business.AlterarCorte(corte);

                MessageBox.Show("Corte alterado com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void cboCorte_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_corte db = cboCorte.SelectedItem as Database.Entity.tb_corte;
            txtNomeCorte.Text = db.nm_corte;
            nudPreço.Value = db.vl_corte;
        }

        private void AlterarCorte_KeyPress(object sender, KeyPressEventArgs e)
        {
            SalvarAlteração();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        
    }
}
