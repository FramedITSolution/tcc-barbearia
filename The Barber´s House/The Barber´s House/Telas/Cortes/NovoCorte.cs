﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Cortes
{
    public partial class NovoCorte : Form
    {
        public NovoCorte()
        {
            InitializeComponent();
        }

        private void btnCorte_Click(object sender, EventArgs e)
        {
            CadastrarCorte();
        }

        private void NovoCorte_KeyPress(object sender, KeyPressEventArgs e)
        {
            CadastrarCorte();
        }

        private void CadastrarCorte()
        {
            try
            {
                Database.Entity.tb_corte corte = new Database.Entity.tb_corte();
                Business.Cortes.BusinessCortes business = new Business.Cortes.BusinessCortes();
                corte.nm_corte = txtCorte.Text;
                corte.vl_corte = nduValor.Value;
                business.NovoCorte(corte);
                MessageBox.Show("Seu corte foi cadastrado com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }
        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
