﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Outros
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        Business.Usuário.BusinessLog logBusiness = new Business.Usuário.BusinessLog();

        private void inserirProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Produto";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    ProdutosVendas.NovoProduto pro = new ProdutosVendas.NovoProduto();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarMateriaPrimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Matéria Prima";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Telas.Estoque.AlterarEstoque pro = new Telas.Estoque.AlterarEstoque();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void inserirMateriaPrimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Matéria Prima";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Telas.Estoque.InserirProdutoNoEstoque pro = new Telas.Estoque.InserirProdutoNoEstoque();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerMateriaPrimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Remover Matéria Prima";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Telas.Estoque.RemoverdoEstoque pro = new Telas.Estoque.RemoverdoEstoque();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarMateriaPrimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Matéria Prima";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Telas.Estoque.ConsultarEstoque pro = new Telas.Estoque.ConsultarEstoque();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Produto";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    ProdutosVendas.AlterarProduto pro = new ProdutosVendas.AlterarProduto();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Produto";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "3461")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    ProdutosVendas.ConsultarProdutos pro = new ProdutosVendas.ConsultarProdutos();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Remover Produto";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    ProdutosVendas.RemoverProduto pro = new ProdutosVendas.RemoverProduto();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void inserirVendaProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Venda Produto";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "3461")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Vendas.NovaVenda pro = new Vendas.NovaVenda();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarFolhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Folha de Pagamento";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.ConsultarFolhadePagamento fp = new RH.ConsultarFolhadePagamento();
                    fp.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarFuncionarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Funcionário";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Funcionário.ConsultarFuncionário func = new RH.Funcionário.ConsultarFuncionário();
                    func.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }            
        }

        private void consultarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Cliente";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Clientes.ConsultarClientes cliente = new Clientes.ConsultarClientes();
                    cliente.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void inserirClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Cliente";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Clientes.NovoCliente pro = new Clientes.NovoCliente();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void inserirFuncionarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Funcionário";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Funcionário.NovoFuncionário pro = new RH.Funcionário.NovoFuncionário();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Fluxo de Caixa";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Financeiro.FluxoDeCaixa pro = new Financeiro.FluxoDeCaixa();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void inserirDespesasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Despesa";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Financeiro.InserirDespesa des = new Financeiro.InserirDespesa();
                    des.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Funcionário";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Funcionário.AlterarFuncionario func = new RH.Funcionário.AlterarFuncionario();
                    func.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }           
        }

        private void alterarClienteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Cliente";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Clientes.AlterarCliente cliente = new Clientes.AlterarCliente();
                    cliente.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }            
        }

        private void removerFuncionárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Remover Funcionário";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Funcionário.RemoverFuncionario func = new RH.Funcionário.RemoverFuncionario();
                    func.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Remover Cliente";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Clientes.RemoverCliente cliente = new Clientes.RemoverCliente();
                    cliente.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }            
        }

        private void consultarDespesasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Consultar Despesas";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    Financeiro.ConsultarDespesa finan = new Financeiro.ConsultarDespesa();
                    finan.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }          
        }

        private void alterarDespesaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Despesas";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Financeiro.AlterarDespesa finan = new Financeiro.AlterarDespesa();
                    finan.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerDespesaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Remover Despesas";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Financeiro.RemoverDespesa finan = new Financeiro.RemoverDespesa();
                    finan.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void novoCorteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Novo Corte";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "5900")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Cortes.NovoCorte corte = new Cortes.NovoCorte();
                    corte.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarCortesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Cortes";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "5900")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Cortes.ConsultarCortes corte = new Cortes.ConsultarCortes();
                    corte.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarCorteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Cortes";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "5900")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Cortes.AlterarCorte corte = new Cortes.AlterarCorte();
                    corte.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerCorteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Remover Corte";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "5900")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Cortes.RemoverCorte corte = new Cortes.RemoverCorte();
                    corte.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void novaFolhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Folha de Pagamento";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.NovaFopag fp = new RH.NovaFopag();
                    fp.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void novoAtendimentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Atendimento";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Serviços.NovoAtendimento serviço = new Serviços.NovoAtendimento();
                    serviço.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarAtendimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Atendimentos";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Serviços.ConsultarServiço serviço = new Serviços.ConsultarServiço();
                    serviço.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarAtendimentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Atendimento";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Serviços.AlterarServiço serviço = new Serviços.AlterarServiço();
                    serviço.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerAtendimentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Remover Atendimento";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Serviços.RemoverServiço serviço = new Serviços.RemoverServiço();
                    serviço.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void novoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Inserir Fornecedor";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Fornecedor.NovoFornecedor pro = new Fornecedor.NovoFornecedor();
                    pro.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarFornecedoresToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Fornecedores";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Fornecedor.ConsultarFornecedor forn = new Fornecedor.ConsultarFornecedor();
                    forn.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarFornecedorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Fornecedor";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Fornecedor.AlterarFornecedor forn = new Fornecedor.AlterarFornecedor();
                    forn.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerFornecedorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "remover Fornecedor";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Fornecedor.RemoverFornecedor forn = new Fornecedor.RemoverFornecedor();
                    forn.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
      
        private void consultarUsuáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Consultar Usuários";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Usuário.ConsultarUsuarios usuario = new Usuário.ConsultarUsuarios();
                    usuario.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarUsuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_log log = new Database.Entity.tb_log();
                log.ds_tela = "Alterar Usuário";
                log.dt_entrada = DateTime.Now;
                log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870" ||
                    Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "1598")
                {
                    logBusiness.NovoLog(log);
                    Hide();
                    Usuário.AlterarUsuario usuario = new Usuário.AlterarUsuario();
                    usuario.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerUsuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Remover Usuário";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    Usuário.RemoverUsuario usuario = new Usuário.RemoverUsuario();
                    usuario.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Usuário.Login login = new Usuário.Login();
            login.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void atribuirAtrasoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Novo Atraso";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Atrasos.CadastrarAtraso atraso = new RH.Outros.Atrasos.CadastrarAtraso();
                    atraso.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarAtrasosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Consultar Atrasos";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Atrasos.ConsultarAtrasos atraso = new RH.Outros.Atrasos.ConsultarAtrasos();
                    atraso.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarAtrasoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Alterar Atraso";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Atrasos.AlterarAtraso atraso = new RH.Outros.Atrasos.AlterarAtraso();
                    atraso.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerAtrasoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Remover Atraso";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Atrasos.RemoverAtraso atraso = new RH.Outros.Atrasos.RemoverAtraso();
                    atraso.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void atribuirFaltaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Nova Falta";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Faltas.CadastrarFalta falta = new RH.Outros.Faltas.CadastrarFalta();
                    falta.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void consultarFaltasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Consultar Faltas";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Faltas.ConsultarFaltas falta = new RH.Outros.Faltas.ConsultarFaltas();
                    falta.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void alterarFaltaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Alterar Falta";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Faltas.AlterarFalta falta = new RH.Outros.Faltas.AlterarFalta();
                    falta.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void removerFaltaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Modelos.Usuário.UsuarioModelo.tb_funcionario.tb_cargo.ds_codigo_cargo == "4870")
                {
                    Database.Entity.tb_log log = new Database.Entity.tb_log();
                    log.ds_tela = "Remover Falta";
                    log.dt_entrada = DateTime.Now;
                    log.id_usuario = Modelos.Usuário.UsuarioModelo.id_usuario;
                    logBusiness.NovoLog(log);
                    Hide();
                    RH.Outros.Faltas.RemoverFalta falta = new RH.Outros.Faltas.RemoverFalta();
                    falta.Show();
                }
                else
                {
                    throw new ArgumentException("Você não possui permissão para entrar nessa tela");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
    }
}
