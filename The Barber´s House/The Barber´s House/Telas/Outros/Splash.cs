﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Outros
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();
        }

        private void Splash_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (ArgumentException)
            {
                
            }
            catch (Exception)
           {
                MessageBox.Show("Ocorreu um erro");
           }
        }

        private void timerSplash_Tick(object sender, EventArgs e)
        {
            timerSplash.Enabled = false;
            Hide();
            Usuário.Login start = new Usuário.Login();
            start.Show();
        }
    }
}
