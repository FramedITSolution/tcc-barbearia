﻿namespace The_Barber_s_House.Telas.Outros
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirMateriaPrimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarMateriaPrimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarMateriaPrimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerMateriaPrimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.suprimentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirVendaProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recursosHumanosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirFuncionarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFuncionarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarFuncionárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerFuncionárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarClienteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removerClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novaFolhaDePagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFolhaDePagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.financeiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inserirDespesasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarDespesasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarDespesaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerDespesaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFornecedoresToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarFornecedorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removerFornecedorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cortesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoCorteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarCortesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarCorteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerCorteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoAtendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarAtendimentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarAtendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerAtendimentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarUsuáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarUsuárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerUsuárioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnMinimizar = new System.Windows.Forms.Button();
            this.btnFechar = new System.Windows.Forms.Button();
            this.outrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atrasosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.faltasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atribuirAtrasoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarAtrasosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarAtrasoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerAtrasoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atribuirFaltaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarFaltasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarFaltaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerFaltaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.estoqueToolStripMenuItem,
            this.suprimentosToolStripMenuItem,
            this.recursosHumanosToolStripMenuItem,
            this.financeiroToolStripMenuItem,
            this.cortesToolStripMenuItem,
            this.atendimentoToolStripMenuItem,
            this.usuárioToolStripMenuItem});
            this.menuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(617, 42);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inserirMateriaPrimaToolStripMenuItem,
            this.consultarMateriaPrimaToolStripMenuItem,
            this.alterarMateriaPrimaToolStripMenuItem,
            this.removerMateriaPrimaToolStripMenuItem,
            this.inserirProdutoToolStripMenuItem,
            this.consultarProdutoToolStripMenuItem,
            this.alterarProdutoToolStripMenuItem,
            this.removerProdutoToolStripMenuItem});
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(61, 19);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            // 
            // inserirMateriaPrimaToolStripMenuItem
            // 
            this.inserirMateriaPrimaToolStripMenuItem.Name = "inserirMateriaPrimaToolStripMenuItem";
            this.inserirMateriaPrimaToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.inserirMateriaPrimaToolStripMenuItem.Text = "Inserir Materia Prima";
            this.inserirMateriaPrimaToolStripMenuItem.Click += new System.EventHandler(this.inserirMateriaPrimaToolStripMenuItem_Click);
            // 
            // consultarMateriaPrimaToolStripMenuItem
            // 
            this.consultarMateriaPrimaToolStripMenuItem.Name = "consultarMateriaPrimaToolStripMenuItem";
            this.consultarMateriaPrimaToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.consultarMateriaPrimaToolStripMenuItem.Text = "Consultar Materia Prima";
            this.consultarMateriaPrimaToolStripMenuItem.Click += new System.EventHandler(this.consultarMateriaPrimaToolStripMenuItem_Click);
            // 
            // alterarMateriaPrimaToolStripMenuItem
            // 
            this.alterarMateriaPrimaToolStripMenuItem.Name = "alterarMateriaPrimaToolStripMenuItem";
            this.alterarMateriaPrimaToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.alterarMateriaPrimaToolStripMenuItem.Text = "AlterarMateriaPrima";
            this.alterarMateriaPrimaToolStripMenuItem.Click += new System.EventHandler(this.alterarMateriaPrimaToolStripMenuItem_Click);
            // 
            // removerMateriaPrimaToolStripMenuItem
            // 
            this.removerMateriaPrimaToolStripMenuItem.Name = "removerMateriaPrimaToolStripMenuItem";
            this.removerMateriaPrimaToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.removerMateriaPrimaToolStripMenuItem.Text = "Remover Materia Prima";
            this.removerMateriaPrimaToolStripMenuItem.Click += new System.EventHandler(this.removerMateriaPrimaToolStripMenuItem_Click);
            // 
            // inserirProdutoToolStripMenuItem
            // 
            this.inserirProdutoToolStripMenuItem.Name = "inserirProdutoToolStripMenuItem";
            this.inserirProdutoToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.inserirProdutoToolStripMenuItem.Text = "Inserir Produto";
            this.inserirProdutoToolStripMenuItem.Click += new System.EventHandler(this.inserirProdutoToolStripMenuItem_Click);
            // 
            // consultarProdutoToolStripMenuItem
            // 
            this.consultarProdutoToolStripMenuItem.Name = "consultarProdutoToolStripMenuItem";
            this.consultarProdutoToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.consultarProdutoToolStripMenuItem.Text = "ConsultarProduto";
            this.consultarProdutoToolStripMenuItem.Click += new System.EventHandler(this.consultarProdutoToolStripMenuItem_Click);
            // 
            // alterarProdutoToolStripMenuItem
            // 
            this.alterarProdutoToolStripMenuItem.Name = "alterarProdutoToolStripMenuItem";
            this.alterarProdutoToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.alterarProdutoToolStripMenuItem.Text = "AlterarProduto";
            this.alterarProdutoToolStripMenuItem.Click += new System.EventHandler(this.alterarProdutoToolStripMenuItem_Click);
            // 
            // removerProdutoToolStripMenuItem
            // 
            this.removerProdutoToolStripMenuItem.Name = "removerProdutoToolStripMenuItem";
            this.removerProdutoToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.removerProdutoToolStripMenuItem.Text = "Remover Produto";
            this.removerProdutoToolStripMenuItem.Click += new System.EventHandler(this.removerProdutoToolStripMenuItem_Click);
            // 
            // suprimentosToolStripMenuItem
            // 
            this.suprimentosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inserirVendaProdutoToolStripMenuItem});
            this.suprimentosToolStripMenuItem.Name = "suprimentosToolStripMenuItem";
            this.suprimentosToolStripMenuItem.Size = new System.Drawing.Size(86, 19);
            this.suprimentosToolStripMenuItem.Text = "Suprimentos";
            // 
            // inserirVendaProdutoToolStripMenuItem
            // 
            this.inserirVendaProdutoToolStripMenuItem.Name = "inserirVendaProdutoToolStripMenuItem";
            this.inserirVendaProdutoToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.inserirVendaProdutoToolStripMenuItem.Text = "Inserir Venda Produto";
            this.inserirVendaProdutoToolStripMenuItem.Click += new System.EventHandler(this.inserirVendaProdutoToolStripMenuItem_Click);
            // 
            // recursosHumanosToolStripMenuItem
            // 
            this.recursosHumanosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inserirFuncionarioToolStripMenuItem,
            this.consultarFuncionarioToolStripMenuItem,
            this.alterarFuncionárioToolStripMenuItem,
            this.removerFuncionárioToolStripMenuItem,
            this.inserirClienteToolStripMenuItem,
            this.consultarClienteToolStripMenuItem,
            this.alterarClienteToolStripMenuItem1,
            this.removerClienteToolStripMenuItem,
            this.novaFolhaDePagamentoToolStripMenuItem,
            this.consultarFolhaDePagamentoToolStripMenuItem,
            this.outrosToolStripMenuItem});
            this.recursosHumanosToolStripMenuItem.Name = "recursosHumanosToolStripMenuItem";
            this.recursosHumanosToolStripMenuItem.Size = new System.Drawing.Size(121, 19);
            this.recursosHumanosToolStripMenuItem.Text = "Recursos Humanos";
            // 
            // inserirFuncionarioToolStripMenuItem
            // 
            this.inserirFuncionarioToolStripMenuItem.Name = "inserirFuncionarioToolStripMenuItem";
            this.inserirFuncionarioToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.inserirFuncionarioToolStripMenuItem.Text = "Inserir Funcionario";
            this.inserirFuncionarioToolStripMenuItem.Click += new System.EventHandler(this.inserirFuncionarioToolStripMenuItem_Click);
            // 
            // consultarFuncionarioToolStripMenuItem
            // 
            this.consultarFuncionarioToolStripMenuItem.Name = "consultarFuncionarioToolStripMenuItem";
            this.consultarFuncionarioToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.consultarFuncionarioToolStripMenuItem.Text = "Consultar Funcionario";
            this.consultarFuncionarioToolStripMenuItem.Click += new System.EventHandler(this.consultarFuncionarioToolStripMenuItem_Click);
            // 
            // alterarFuncionárioToolStripMenuItem
            // 
            this.alterarFuncionárioToolStripMenuItem.Name = "alterarFuncionárioToolStripMenuItem";
            this.alterarFuncionárioToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.alterarFuncionárioToolStripMenuItem.Text = "Alterar Funcionário";
            this.alterarFuncionárioToolStripMenuItem.Click += new System.EventHandler(this.alterarFuncionárioToolStripMenuItem_Click);
            // 
            // removerFuncionárioToolStripMenuItem
            // 
            this.removerFuncionárioToolStripMenuItem.Name = "removerFuncionárioToolStripMenuItem";
            this.removerFuncionárioToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.removerFuncionárioToolStripMenuItem.Text = "Remover Funcionário";
            this.removerFuncionárioToolStripMenuItem.Click += new System.EventHandler(this.removerFuncionárioToolStripMenuItem_Click);
            // 
            // inserirClienteToolStripMenuItem
            // 
            this.inserirClienteToolStripMenuItem.Name = "inserirClienteToolStripMenuItem";
            this.inserirClienteToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.inserirClienteToolStripMenuItem.Text = "Inserir Cliente";
            this.inserirClienteToolStripMenuItem.Click += new System.EventHandler(this.inserirClienteToolStripMenuItem_Click);
            // 
            // consultarClienteToolStripMenuItem
            // 
            this.consultarClienteToolStripMenuItem.Name = "consultarClienteToolStripMenuItem";
            this.consultarClienteToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.consultarClienteToolStripMenuItem.Text = "Consultar Cliente";
            this.consultarClienteToolStripMenuItem.Click += new System.EventHandler(this.consultarClienteToolStripMenuItem_Click);
            // 
            // alterarClienteToolStripMenuItem1
            // 
            this.alterarClienteToolStripMenuItem1.Name = "alterarClienteToolStripMenuItem1";
            this.alterarClienteToolStripMenuItem1.Size = new System.Drawing.Size(237, 22);
            this.alterarClienteToolStripMenuItem1.Text = "Alterar Cliente";
            this.alterarClienteToolStripMenuItem1.Click += new System.EventHandler(this.alterarClienteToolStripMenuItem1_Click);
            // 
            // removerClienteToolStripMenuItem
            // 
            this.removerClienteToolStripMenuItem.Name = "removerClienteToolStripMenuItem";
            this.removerClienteToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.removerClienteToolStripMenuItem.Text = "Remover Cliente";
            this.removerClienteToolStripMenuItem.Click += new System.EventHandler(this.removerClienteToolStripMenuItem_Click);
            // 
            // novaFolhaDePagamentoToolStripMenuItem
            // 
            this.novaFolhaDePagamentoToolStripMenuItem.Name = "novaFolhaDePagamentoToolStripMenuItem";
            this.novaFolhaDePagamentoToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.novaFolhaDePagamentoToolStripMenuItem.Text = "Nova Folha de Pagamento";
            this.novaFolhaDePagamentoToolStripMenuItem.Click += new System.EventHandler(this.novaFolhaDePagamentoToolStripMenuItem_Click);
            // 
            // consultarFolhaDePagamentoToolStripMenuItem
            // 
            this.consultarFolhaDePagamentoToolStripMenuItem.Name = "consultarFolhaDePagamentoToolStripMenuItem";
            this.consultarFolhaDePagamentoToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.consultarFolhaDePagamentoToolStripMenuItem.Text = "Consultar Folha de Pagamento";
            this.consultarFolhaDePagamentoToolStripMenuItem.Click += new System.EventHandler(this.consultarFolhaDePagamentoToolStripMenuItem_Click);
            // 
            // financeiroToolStripMenuItem
            // 
            this.financeiroToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fluxoDeCaixaToolStripMenuItem,
            this.inserirDespesasToolStripMenuItem,
            this.consultarDespesasToolStripMenuItem,
            this.alterarDespesaToolStripMenuItem,
            this.removerDespesaToolStripMenuItem,
            this.novoFornecedorToolStripMenuItem,
            this.consultarFornecedoresToolStripMenuItem1,
            this.alterarFornecedorToolStripMenuItem1,
            this.removerFornecedorToolStripMenuItem1});
            this.financeiroToolStripMenuItem.Name = "financeiroToolStripMenuItem";
            this.financeiroToolStripMenuItem.Size = new System.Drawing.Size(74, 19);
            this.financeiroToolStripMenuItem.Text = "Financeiro";
            // 
            // fluxoDeCaixaToolStripMenuItem
            // 
            this.fluxoDeCaixaToolStripMenuItem.Name = "fluxoDeCaixaToolStripMenuItem";
            this.fluxoDeCaixaToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.fluxoDeCaixaToolStripMenuItem.Text = "Fluxo de Caixa";
            this.fluxoDeCaixaToolStripMenuItem.Click += new System.EventHandler(this.fluxoDeCaixaToolStripMenuItem_Click);
            // 
            // inserirDespesasToolStripMenuItem
            // 
            this.inserirDespesasToolStripMenuItem.Name = "inserirDespesasToolStripMenuItem";
            this.inserirDespesasToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.inserirDespesasToolStripMenuItem.Text = "Inserir Despesas";
            this.inserirDespesasToolStripMenuItem.Click += new System.EventHandler(this.inserirDespesasToolStripMenuItem_Click);
            // 
            // consultarDespesasToolStripMenuItem
            // 
            this.consultarDespesasToolStripMenuItem.Name = "consultarDespesasToolStripMenuItem";
            this.consultarDespesasToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.consultarDespesasToolStripMenuItem.Text = "Consultar Despesas";
            this.consultarDespesasToolStripMenuItem.Click += new System.EventHandler(this.consultarDespesasToolStripMenuItem_Click);
            // 
            // alterarDespesaToolStripMenuItem
            // 
            this.alterarDespesaToolStripMenuItem.Name = "alterarDespesaToolStripMenuItem";
            this.alterarDespesaToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.alterarDespesaToolStripMenuItem.Text = "Alterar Despesa";
            this.alterarDespesaToolStripMenuItem.Click += new System.EventHandler(this.alterarDespesaToolStripMenuItem_Click);
            // 
            // removerDespesaToolStripMenuItem
            // 
            this.removerDespesaToolStripMenuItem.Name = "removerDespesaToolStripMenuItem";
            this.removerDespesaToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.removerDespesaToolStripMenuItem.Text = "Remover Despesa";
            this.removerDespesaToolStripMenuItem.Click += new System.EventHandler(this.removerDespesaToolStripMenuItem_Click);
            // 
            // novoFornecedorToolStripMenuItem
            // 
            this.novoFornecedorToolStripMenuItem.Name = "novoFornecedorToolStripMenuItem";
            this.novoFornecedorToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.novoFornecedorToolStripMenuItem.Text = "Novo Fornecedor";
            this.novoFornecedorToolStripMenuItem.Click += new System.EventHandler(this.novoFornecedorToolStripMenuItem_Click);
            // 
            // consultarFornecedoresToolStripMenuItem1
            // 
            this.consultarFornecedoresToolStripMenuItem1.Name = "consultarFornecedoresToolStripMenuItem1";
            this.consultarFornecedoresToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.consultarFornecedoresToolStripMenuItem1.Text = "Consultar Fornecedores";
            this.consultarFornecedoresToolStripMenuItem1.Click += new System.EventHandler(this.consultarFornecedoresToolStripMenuItem1_Click);
            // 
            // alterarFornecedorToolStripMenuItem1
            // 
            this.alterarFornecedorToolStripMenuItem1.Name = "alterarFornecedorToolStripMenuItem1";
            this.alterarFornecedorToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.alterarFornecedorToolStripMenuItem1.Text = "Alterar Fornecedor";
            this.alterarFornecedorToolStripMenuItem1.Click += new System.EventHandler(this.alterarFornecedorToolStripMenuItem1_Click);
            // 
            // removerFornecedorToolStripMenuItem1
            // 
            this.removerFornecedorToolStripMenuItem1.Name = "removerFornecedorToolStripMenuItem1";
            this.removerFornecedorToolStripMenuItem1.Size = new System.Drawing.Size(199, 22);
            this.removerFornecedorToolStripMenuItem1.Text = "Remover Fornecedor";
            this.removerFornecedorToolStripMenuItem1.Click += new System.EventHandler(this.removerFornecedorToolStripMenuItem1_Click);
            // 
            // cortesToolStripMenuItem
            // 
            this.cortesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoCorteToolStripMenuItem,
            this.consultarCortesToolStripMenuItem,
            this.alterarCorteToolStripMenuItem,
            this.removerCorteToolStripMenuItem});
            this.cortesToolStripMenuItem.Name = "cortesToolStripMenuItem";
            this.cortesToolStripMenuItem.Size = new System.Drawing.Size(53, 19);
            this.cortesToolStripMenuItem.Text = "Cortes";
            // 
            // novoCorteToolStripMenuItem
            // 
            this.novoCorteToolStripMenuItem.Name = "novoCorteToolStripMenuItem";
            this.novoCorteToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.novoCorteToolStripMenuItem.Text = "Novo Corte";
            this.novoCorteToolStripMenuItem.Click += new System.EventHandler(this.novoCorteToolStripMenuItem_Click);
            // 
            // consultarCortesToolStripMenuItem
            // 
            this.consultarCortesToolStripMenuItem.Name = "consultarCortesToolStripMenuItem";
            this.consultarCortesToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.consultarCortesToolStripMenuItem.Text = "Consultar Cortes";
            this.consultarCortesToolStripMenuItem.Click += new System.EventHandler(this.consultarCortesToolStripMenuItem_Click);
            // 
            // alterarCorteToolStripMenuItem
            // 
            this.alterarCorteToolStripMenuItem.Name = "alterarCorteToolStripMenuItem";
            this.alterarCorteToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.alterarCorteToolStripMenuItem.Text = "Alterar Corte";
            this.alterarCorteToolStripMenuItem.Click += new System.EventHandler(this.alterarCorteToolStripMenuItem_Click);
            // 
            // removerCorteToolStripMenuItem
            // 
            this.removerCorteToolStripMenuItem.Name = "removerCorteToolStripMenuItem";
            this.removerCorteToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.removerCorteToolStripMenuItem.Text = "Remover Corte";
            this.removerCorteToolStripMenuItem.Click += new System.EventHandler(this.removerCorteToolStripMenuItem_Click);
            // 
            // atendimentoToolStripMenuItem
            // 
            this.atendimentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoAtendimentoToolStripMenuItem,
            this.consultarAtendimentosToolStripMenuItem,
            this.alterarAtendimentoToolStripMenuItem,
            this.removerAtendimentoToolStripMenuItem});
            this.atendimentoToolStripMenuItem.Name = "atendimentoToolStripMenuItem";
            this.atendimentoToolStripMenuItem.Size = new System.Drawing.Size(89, 19);
            this.atendimentoToolStripMenuItem.Text = "Atendimento";
            // 
            // novoAtendimentoToolStripMenuItem
            // 
            this.novoAtendimentoToolStripMenuItem.Name = "novoAtendimentoToolStripMenuItem";
            this.novoAtendimentoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.novoAtendimentoToolStripMenuItem.Text = "Novo Atendimento";
            this.novoAtendimentoToolStripMenuItem.Click += new System.EventHandler(this.novoAtendimentoToolStripMenuItem_Click);
            // 
            // consultarAtendimentosToolStripMenuItem
            // 
            this.consultarAtendimentosToolStripMenuItem.Name = "consultarAtendimentosToolStripMenuItem";
            this.consultarAtendimentosToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.consultarAtendimentosToolStripMenuItem.Text = "Consultar Atendimentos";
            this.consultarAtendimentosToolStripMenuItem.Click += new System.EventHandler(this.consultarAtendimentosToolStripMenuItem_Click);
            // 
            // alterarAtendimentoToolStripMenuItem
            // 
            this.alterarAtendimentoToolStripMenuItem.Name = "alterarAtendimentoToolStripMenuItem";
            this.alterarAtendimentoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.alterarAtendimentoToolStripMenuItem.Text = "Alterar Atendimento";
            this.alterarAtendimentoToolStripMenuItem.Click += new System.EventHandler(this.alterarAtendimentoToolStripMenuItem_Click);
            // 
            // removerAtendimentoToolStripMenuItem
            // 
            this.removerAtendimentoToolStripMenuItem.Name = "removerAtendimentoToolStripMenuItem";
            this.removerAtendimentoToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.removerAtendimentoToolStripMenuItem.Text = "Remover Atendimento";
            this.removerAtendimentoToolStripMenuItem.Click += new System.EventHandler(this.removerAtendimentoToolStripMenuItem_Click);
            // 
            // usuárioToolStripMenuItem
            // 
            this.usuárioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarUsuáriosToolStripMenuItem,
            this.alterarUsuárioToolStripMenuItem,
            this.removerUsuárioToolStripMenuItem});
            this.usuárioToolStripMenuItem.Name = "usuárioToolStripMenuItem";
            this.usuárioToolStripMenuItem.Size = new System.Drawing.Size(59, 19);
            this.usuárioToolStripMenuItem.Text = "Usuário";
            // 
            // consultarUsuáriosToolStripMenuItem
            // 
            this.consultarUsuáriosToolStripMenuItem.Name = "consultarUsuáriosToolStripMenuItem";
            this.consultarUsuáriosToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.consultarUsuáriosToolStripMenuItem.Text = "Consultar Usuários";
            this.consultarUsuáriosToolStripMenuItem.Click += new System.EventHandler(this.consultarUsuáriosToolStripMenuItem_Click);
            // 
            // alterarUsuárioToolStripMenuItem
            // 
            this.alterarUsuárioToolStripMenuItem.Name = "alterarUsuárioToolStripMenuItem";
            this.alterarUsuárioToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.alterarUsuárioToolStripMenuItem.Text = "Alterar Usuário";
            this.alterarUsuárioToolStripMenuItem.Click += new System.EventHandler(this.alterarUsuárioToolStripMenuItem_Click);
            // 
            // removerUsuárioToolStripMenuItem
            // 
            this.removerUsuárioToolStripMenuItem.Name = "removerUsuárioToolStripMenuItem";
            this.removerUsuárioToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.removerUsuárioToolStripMenuItem.Text = "Remover Usuário";
            this.removerUsuárioToolStripMenuItem.Click += new System.EventHandler(this.removerUsuárioToolStripMenuItem_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::The_Barber_s_House.Properties.Resources.barbearia_design;
            this.pictureBox2.Location = new System.Drawing.Point(0, 26);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(617, 327);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.Location = new System.Drawing.Point(565, -6);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(21, 26);
            this.btnMinimizar.TabIndex = 41;
            this.btnMinimizar.Text = "-";
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.FlatAppearance.BorderSize = 0;
            this.btnFechar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFechar.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFechar.Location = new System.Drawing.Point(592, -1);
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(21, 23);
            this.btnFechar.TabIndex = 42;
            this.btnFechar.Text = "X";
            this.btnFechar.UseVisualStyleBackColor = true;
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // outrosToolStripMenuItem
            // 
            this.outrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.atrasosToolStripMenuItem,
            this.faltasToolStripMenuItem});
            this.outrosToolStripMenuItem.Name = "outrosToolStripMenuItem";
            this.outrosToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.outrosToolStripMenuItem.Text = "Outros";
            // 
            // atrasosToolStripMenuItem
            // 
            this.atrasosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.atribuirAtrasoToolStripMenuItem,
            this.consultarAtrasosToolStripMenuItem,
            this.alterarAtrasoToolStripMenuItem,
            this.removerAtrasoToolStripMenuItem});
            this.atrasosToolStripMenuItem.Name = "atrasosToolStripMenuItem";
            this.atrasosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.atrasosToolStripMenuItem.Text = "Atrasos";
            // 
            // faltasToolStripMenuItem
            // 
            this.faltasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.atribuirFaltaToolStripMenuItem,
            this.consultarFaltasToolStripMenuItem,
            this.alterarFaltaToolStripMenuItem,
            this.removerFaltaToolStripMenuItem});
            this.faltasToolStripMenuItem.Name = "faltasToolStripMenuItem";
            this.faltasToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.faltasToolStripMenuItem.Text = "Faltas";
            // 
            // atribuirAtrasoToolStripMenuItem
            // 
            this.atribuirAtrasoToolStripMenuItem.Name = "atribuirAtrasoToolStripMenuItem";
            this.atribuirAtrasoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.atribuirAtrasoToolStripMenuItem.Text = "Atribuir Atraso";
            this.atribuirAtrasoToolStripMenuItem.Click += new System.EventHandler(this.atribuirAtrasoToolStripMenuItem_Click);
            // 
            // consultarAtrasosToolStripMenuItem
            // 
            this.consultarAtrasosToolStripMenuItem.Name = "consultarAtrasosToolStripMenuItem";
            this.consultarAtrasosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.consultarAtrasosToolStripMenuItem.Text = "Consultar Atrasos";
            this.consultarAtrasosToolStripMenuItem.Click += new System.EventHandler(this.consultarAtrasosToolStripMenuItem_Click);
            // 
            // alterarAtrasoToolStripMenuItem
            // 
            this.alterarAtrasoToolStripMenuItem.Name = "alterarAtrasoToolStripMenuItem";
            this.alterarAtrasoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.alterarAtrasoToolStripMenuItem.Text = "Alterar Atraso";
            this.alterarAtrasoToolStripMenuItem.Click += new System.EventHandler(this.alterarAtrasoToolStripMenuItem_Click);
            // 
            // removerAtrasoToolStripMenuItem
            // 
            this.removerAtrasoToolStripMenuItem.Name = "removerAtrasoToolStripMenuItem";
            this.removerAtrasoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.removerAtrasoToolStripMenuItem.Text = "Remover Atraso";
            this.removerAtrasoToolStripMenuItem.Click += new System.EventHandler(this.removerAtrasoToolStripMenuItem_Click);
            // 
            // atribuirFaltaToolStripMenuItem
            // 
            this.atribuirFaltaToolStripMenuItem.Name = "atribuirFaltaToolStripMenuItem";
            this.atribuirFaltaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.atribuirFaltaToolStripMenuItem.Text = "Atribuir Falta";
            this.atribuirFaltaToolStripMenuItem.Click += new System.EventHandler(this.atribuirFaltaToolStripMenuItem_Click);
            // 
            // consultarFaltasToolStripMenuItem
            // 
            this.consultarFaltasToolStripMenuItem.Name = "consultarFaltasToolStripMenuItem";
            this.consultarFaltasToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.consultarFaltasToolStripMenuItem.Text = "Consultar Faltas";
            this.consultarFaltasToolStripMenuItem.Click += new System.EventHandler(this.consultarFaltasToolStripMenuItem_Click);
            // 
            // alterarFaltaToolStripMenuItem
            // 
            this.alterarFaltaToolStripMenuItem.Name = "alterarFaltaToolStripMenuItem";
            this.alterarFaltaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.alterarFaltaToolStripMenuItem.Text = "Alterar Falta";
            this.alterarFaltaToolStripMenuItem.Click += new System.EventHandler(this.alterarFaltaToolStripMenuItem_Click);
            // 
            // removerFaltaToolStripMenuItem
            // 
            this.removerFaltaToolStripMenuItem.Name = "removerFaltaToolStripMenuItem";
            this.removerFaltaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.removerFaltaToolStripMenuItem.Text = "Remover Falta";
            this.removerFaltaToolStripMenuItem.Click += new System.EventHandler(this.removerFaltaToolStripMenuItem_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 354);
            this.Controls.Add(this.btnMinimizar);
            this.Controls.Add(this.btnFechar);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuProvisório";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarMateriaPrimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarMateriaPrimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirMateriaPrimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recursosHumanosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFolhaDePagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFuncionarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirFuncionarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerMateriaPrimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarFuncionárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarClienteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem removerFuncionárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cortesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoCorteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarCortesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarCorteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerCorteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novaFolhaDePagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoAtendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarAtendimentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarAtendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerAtendimentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem suprimentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirVendaProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem financeiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inserirDespesasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarDespesasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarDespesaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerDespesaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFornecedoresToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alterarFornecedorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem removerFornecedorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usuárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarUsuáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarUsuárioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerUsuárioToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnMinimizar;
        private System.Windows.Forms.Button btnFechar;
        private System.Windows.Forms.ToolStripMenuItem outrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atrasosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atribuirAtrasoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarAtrasosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarAtrasoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerAtrasoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem faltasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atribuirFaltaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarFaltasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarFaltaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerFaltaToolStripMenuItem;
    }
}