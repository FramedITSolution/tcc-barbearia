﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Outros
{
    public partial class EsqueceuASenha : Form
    {
        public EsqueceuASenha()
        {
            InitializeComponent();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                string usuario = txtUsuário.Text;

                Business.Usuário.BusinessUsuário usuarioBusiness = new Business.Usuário.BusinessUsuário();
                usuarioBusiness.ConsultarUsuario(usuario, chkWhats.Checked);
                MessageBox.Show("Código enviado");
                Hide();
                Telas.Usuário.AlterarSenha start = new Telas.Usuário.AlterarSenha();
                start.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //catch (Exception)
            //{
            //    MessageBox.Show("Ocorreu um erro");
            //}
        }
    }
}
