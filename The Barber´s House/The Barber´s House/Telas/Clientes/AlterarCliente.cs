﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Clientes
{
    public partial class AlterarCliente : Form
    {
        public AlterarCliente()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Clientes.BusinessClientes business = new Business.Clientes.BusinessClientes();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Objetos.UsoGeral validar = new Objetos.UsoGeral();
                Database.Entity.tb_cliente cliente = new Database.Entity.tb_cliente();
                cliente.nm_cliente = cboNomedocliente.Text;
                cliente.dt_primeira_compra = dtpPrimeiraCompra.Value;
                cliente.ds_endereco = txtEndereço.Text;               
                cliente.bt_fidelizado = chkFidelizado.Checked;
                cliente.bt_ativo = chkAtivo.Checked;
                if (txtTelefone.MaskFull == true)
                {
                    cliente.ds_telefone = txtTelefone.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o telefone");
                }

                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                if (contem == true && txtEmail.Text != string.Empty && invalido == false)
                {
                    cliente.ds_email = txtEmail.Text;
                    business.AlterarCliente(cliente);
                    MessageBox.Show("Cliente alterado com sucesso");
                }
                else
                {
                    MessageBox.Show("Email inválido, preencha novamente");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }
        }

        private void CarregarCombo()
        {
            cboNomedocliente.DisplayMember = nameof(Database.Entity.tb_cliente.nm_cliente);
            cboNomedocliente.DataSource = business.ListarClientes();
        }

        private void cboNomedocliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_cliente model = cboNomedocliente.SelectedItem as Database.Entity.tb_cliente;
            txtTelefone.Text = model.ds_telefone;
            txtEndereço.Text = model.ds_endereco;
            txtEmail.Text = model.ds_email;
            chkAtivo.Checked = model.bt_ativo;
            chkFidelizado.Checked = model.bt_fidelizado;
            dtpPrimeiraCompra.Value = model.dt_primeira_compra.Value;
        }
        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
