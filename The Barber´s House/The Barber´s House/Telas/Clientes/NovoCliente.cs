﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Clientes
{
    public partial class NovoCliente : Form
    {
        public NovoCliente()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Business.Clientes.BusinessClientes business = new Business.Clientes.BusinessClientes();
                Database.Entity.tb_cliente cliente = new Database.Entity.tb_cliente();
                Objetos.UsoGeral validar = new Objetos.UsoGeral();

                cliente.nm_cliente = txtNomedocliente.Text;
                cliente.dt_primeira_compra = dtpDatadeprimeiracompra.Value;
                bool telefoneValido = validar.ValidarTelefone(txtTelefone.Text, rdnCelular.Checked);
                if (txtTelefone.MaskFull == true & rdnCelular.Checked == false || telefoneValido == true)
                {
                    cliente.ds_telefone = txtTelefone.Text;
                }
                else
                {
                    MessageBox.Show("Preencha o telefone");
                }
                cliente.ds_endereco = txtEndereço.Text;
                cliente.bt_fidelizado = chkFidelizado.Checked;
                cliente.bt_ativo = chkAtivo.Checked;

                bool contem = validar.VerificarEmail(txtEmail.Text);
                bool invalido = validar.ValidarEmail(txtEmail.Text);
                if (contem == true && txtEmail.Text != string.Empty && invalido == false)
                {
                    cliente.ds_email = txtEmail.Text;
                    business.NovoCliente(cliente);
                    MessageBox.Show("Cliente cadastrado com sucesso");
                    LimparCampos();
                }
                else
                {
                    MessageBox.Show("Email inválido, preencha novamente");
                }                                                               
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde.");
            }

        }

        private void LimparCampos()
        {
            txtNomedocliente.Text = string.Empty;           
            txtEndereço.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtTelefone.Text = string.Empty;
            chkAtivo.Checked = false;
            chkFidelizado.Checked = false;
            rdnCelular.Checked = false;
            rdnFixo.Checked = false;
            dtpDatadeprimeiracompra.Value = DateTime.Now;
            txtTelefone.Visible = false;
        }

        private void rdnFixo_CheckedChanged(object sender, EventArgs e)
        {
            txtTelefone.Mask = "####-####";
            txtTelefone.Visible = true;           
        }

        private void rdnCelular_CheckedChanged(object sender, EventArgs e)
        {
            txtTelefone.Mask = "(##) #####-####";
            txtTelefone.Visible = true;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
