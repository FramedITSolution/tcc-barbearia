﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Usuário
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                // Coleta de Dados
                string usuario = txtNome.Text;
                string senha = txtSenha.Text;

                // Instanciamento da Business
                Business.Usuário.BusinessUsuário usuarioBusiness = new Business.Usuário.BusinessUsuário();
                // Invocando a Função EfetuarLogin da Business
                bool resposta = usuarioBusiness.EfetuarLogin(usuario, senha);
                // Condição para tomar a decisão de ir para o Menu ou não
                if (resposta == false)
                {
                    LimparCampos();// Limpa os campos caso não tenha um usuário válido
                }
                else
                {
                    Database.Entity.tb_usuario modelo = usuarioBusiness.ModeloUsuarioAtivo(usuario);
                    Modelos.Usuário.UsuarioModelo = modelo;

                    Hide();
                    Outros.Menu start = new Outros.Menu();
                    start.Show();//Vai para o Menu
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
        public void LimparCampos()
        {
            txtNome.Text = string.Empty;
            txtSenha.Text = string.Empty;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            Hide();
            CadastrarUsuario usuario = new CadastrarUsuario();
            usuario.Show();           
        }

        private void btnLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                ClicarNoBotao();
            }
        }

        private void ClicarNoBotao()
        {
            try
            {
                // Coleta de Dados
                string usuario = txtNome.Text;
                string senha = txtSenha.Text;

                // Instanciamento da Business
                Business.Usuário.BusinessUsuário usuarioBusiness = new Business.Usuário.BusinessUsuário();
                // Invocando a Função EfetuarLogin da Business
                bool resposta = usuarioBusiness.EfetuarLogin(usuario, senha);
                // Condição para tomar a decisão de ir para o Menu ou não
                if (resposta == false)
                {
                    LimparCampos();// Limpa os campos caso não tenha um usuário válido
                }
                else
                {
                    Database.Entity.tb_usuario modelo = usuarioBusiness.ModeloUsuarioAtivo(usuario);
                    Modelos.Usuário.UsuarioModelo = modelo;

                    Hide();
                    Outros.Menu start = new Outros.Menu();
                    start.Show();//Vai para o Menu
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
               MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}


