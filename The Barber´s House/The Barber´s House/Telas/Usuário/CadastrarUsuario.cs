﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Usuário
{
    public partial class CadastrarUsuario : Form
    {
        public CadastrarUsuario()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Usuário.BusinessUsuário business = new Business.Usuário.BusinessUsuário();
        Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();
        Database.Entity.tb_usuario usuario = new Database.Entity.tb_usuario();
       
        private void CarregarCombo()
        {
            List<Database.Entity.tb_funcionario> lista = func.ListarFuncionarios();
            cboFuncionário.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboFuncionário.DataSource = lista;            
         }     

        private void BtnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                usuario.nm_usuario = txtUsuario.Text;
                usuario.nm_senha = txtSenha.Text;
                Database.Entity.tb_funcionario model = func.ConsultarPorNome(cboFuncionário.Text);
                usuario.id_funcionario = model.id_funcionario;

                if (txtSenha.Text == txtConfSenha.Text)
                {
                    business.Cadastro(usuario);
                    MessageBox.Show("Usuário cadastrado com sucesso");
                }
                else
                {
                    MessageBox.Show("Confirme sua senha");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //catch (Exception)
            //{
            //    MessageBox.Show("Ocorreu um erro.");
            //}
        }

        private void CadastrarUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Cadastrar();
        }

        private void Cadastrar()
        {
            try
            {
                usuario.nm_usuario = txtUsuario.Text;
                usuario.nm_senha = txtSenha.Text;
                Database.Entity.tb_funcionario model = func.ConsultarPorNome(cboFuncionário.Text);
                usuario.id_funcionario = model.id_funcionario;

                if (txtSenha.Text == txtConfSenha.Text)
                {
                    business.Cadastro(usuario);
                    MessageBox.Show("Usuário cadastrado com sucesso");
                }
                else
                {
                    MessageBox.Show("Confirme sua senha");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "Usuario:")
            {
                txtUsuario.Text = string.Empty;
            }
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if(txtUsuario.Text == string.Empty)
            {
                txtUsuario.Text = "Usuario:";
            }
        }

        private void txtSenha_Enter(object sender, EventArgs e)
        {
            if (txtSenha.Text == "Senha:")
            {
                txtSenha.Text = string.Empty;
            }
        }

        private void txtSenha_Leave(object sender, EventArgs e)
        {
            if (txtSenha.Text == string.Empty)
            {
                txtSenha.Text = "Senha:";
            }
        }

        private void txtConfSenha_Enter(object sender, EventArgs e)
        {
            if (txtConfSenha.Text == "Confirmar Senha:")
            {
                txtConfSenha.Text = string.Empty;
            }
        }

        private void txtConfSenha_Leave(object sender, EventArgs e)
        {
            if (txtConfSenha.Text == string.Empty)
            {
                txtConfSenha.Text = "Confirmar Senha:";
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            Hide();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Login login = new Login();
            login.Show();
            Hide();
        }
    }
}
