﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Usuário
{
    public partial class AlterarUsuario : Form
    {
        public AlterarUsuario()
        {
            InitializeComponent();
            CarregarCombo();
        }

        Business.Usuário.BusinessUsuário business = new Business.Usuário.BusinessUsuário();
        Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();
        
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_usuario usuario = new Database.Entity.tb_usuario();
                usuario.nm_usuario = cboUsuario.Text;
                usuario.nm_senha = txtSenha.Text;
                Database.Entity.tb_funcionario model = func.ConsultarPorNome(cboFunc.Text);
                usuario.id_funcionario = model.id_funcionario;

                if (txtSenha.Text == txtConfSenha.Text)
                {
                    business.AlterarUsuario(usuario);
                    MessageBox.Show("Usuário alterado com sucesso");
                }
                else
                {
                    MessageBox.Show("Confirme sua senha");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void CarregarCombo()
        {
            List<Database.Entity.tb_funcionario> lista = func.ListarFuncionarios();
            cboFunc.DisplayMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboFunc.DataSource = lista;
        }

        private void cboFunc_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Carrega os Funcionários
            Database.Entity.tb_funcionario model = func.ConsultarPorNome(cboFunc.Text);
            int funcionario = model.id_funcionario;

            List<Database.Entity.tb_usuario> lista = business.ConsultarPorFuncionario(funcionario);

            cboUsuario.DisplayMember = nameof(Database.Entity.tb_usuario.nm_usuario);
            cboUsuario.DataSource = lista;            
        }

        private void cboUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Carrega os usuários
            string usuario = cboUsuario.Text;

            Database.Entity.tb_usuario model = business.ConsultarPorUsuario(usuario);
            txtSenha.Text = model.nm_senha;
            txtConfSenha.Text = model.nm_senha;
        }

        private void AlterarUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Database.Entity.tb_usuario usuario = new Database.Entity.tb_usuario();
                usuario.nm_usuario = cboUsuario.Text;
                usuario.nm_senha = txtSenha.Text;
                Database.Entity.tb_funcionario model = func.ConsultarPorNome(cboFunc.Text);
                usuario.id_funcionario = model.id_funcionario;

                if (txtSenha.Text == txtConfSenha.Text)
                {
                    business.AlterarUsuario(usuario);
                    MessageBox.Show("Usuário alterado com sucesso");
                }
                else
                {
                    MessageBox.Show("Confirme sua senha");
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro.");
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
    }
}
