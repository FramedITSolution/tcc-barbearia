﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Usuário
{
    public partial class AlterarSenha : Form
    {
        public AlterarSenha()
        {
            InitializeComponent();
        }

        Business.Usuário.BusinessUsuário usuarioBusiness = new Business.Usuário.BusinessUsuário();

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                string usuario = txtUsuário.Text;
                string codigo = txtCódigo.Text;
                string senha = txtSenha.Text;
                string confirmar = txtConfirmarSenha.Text;

                if (senha != confirmar)
                    throw new ArgumentException("As senhas não correspondem");

                usuarioBusiness.AlterarSenha(usuario, senha, codigo);
                MessageBox.Show("Senha alterada com sucesso!");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }
    }
}
