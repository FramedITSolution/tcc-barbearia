﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Usuário
{
    public partial class ConsultarUsuarios : Form
    {
        public ConsultarUsuarios()
        {
            InitializeComponent();
        }

        Business.Usuário.BusinessUsuário business = new Business.Usuário.BusinessUsuário();
        Database.Usuário.DatabaseUsuário usuario = new Database.Usuário.DatabaseUsuário();

        private void btnConsultar_Click_1(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudID.Value);
                List<Database.Entity.tb_usuario> lista = business.ConsultarPorId(id);
                dgvUsuarios.AutoGenerateColumns = false;
                dgvUsuarios.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnListar_Click_1(object sender, EventArgs e)
        {
            try
            {
                List<Database.Entity.tb_usuario> lista = business.ListarUsuarios();
                dgvUsuarios.AutoGenerateColumns = false;
                dgvUsuarios.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Hide();
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
        }
    }
}
