﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Usuário
{
    public partial class Cadastro : Form
    {
        public Cadastro()
        {
            InitializeComponent();
        }

        private void BtnCadastrar_Click(object sender, EventArgs e)
        {
            //try
            //{
            Business.usuarioBusiness usuarioBusiness = new Business.usuarioBusiness();
            Database.Entity.tb_usuario tb_usuario = new Database.Entity.tb_usuario();

            Database.Entity.tb_funcionario func = cboFuncionário.SelectedItem as Database.Entity.tb_funcionario;


            // Coleta de Dados
            tb_usuario.nm_usuario = txtUsuario.Text;
            tb_usuario.nm_senha = txtSenha.Text;
            tb_usuario.id_funcionario = func.id_funcionario;

            bool funUsu = usuarioBusiness.VerificarFuncionario(func.id_funcionario);
            if (funUsu == false)
            {
                throw new ArgumentException("Funcionário não encontrado");
                LimparCampos();
            }
            usuarioBusiness.Cadastro(tb_usuario);
            MessageBox.Show("Usuario Cadastrado", "", MessageBoxButtons.OK);
            //}
            //catch (ArgumentException ex)
            //{
            // MessageBox.Show(ex.Message);
            //}
            //catch (Exception)
            //{
            //MessageBox.Show("Ocorreu um erro", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

        }
        private void LblMinimizar_Click(object sender, EventArgs e)
        {
            // Minimiza a Tela de Cadastro
            WindowState = FormWindowState.Minimized;
        }

        private void LblFechar_Click(object sender, EventArgs e)
        {
            Close();// Fecha a Tela de Cadastro
            Consultar.Outro.frmLogin start = new Consultar.Outro.frmLogin();
            start.Show();// Abre a Tela de Login
        }



        public void CarregarCampos()
        {
            Business.funcionarioBusiness funcionarioBusiness = new Business.funcionarioBusiness();

            List<Database.Entity.tb_funcionario> lista = funcionarioBusiness.ConsultarTodos();

            cboFuncionário.ValueMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
            cboFuncionário.DataSource = lista;
        }

        public void LimparCampos()
        {
            txtUsuario.Text = string.Empty;
            txtSenha.Text = string.Empty;
            cboFuncionário.Text = "Funcionário:";
        }

    }
}
