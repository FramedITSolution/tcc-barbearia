﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Vendas
{
    public partial class NovaVenda : Form
    {
        public NovaVenda()
        {
            InitializeComponent();
            CarregarCampos();
        }

        Business.Produtos.Vendas.BusinessProdutos_de_Vendas produtos = new Business.Produtos.Vendas.BusinessProdutos_de_Vendas();
        Business.RH.BusinessFuncionário func = new Business.RH.BusinessFuncionário();
        Business.Clientes.BusinessClientes clientes = new Business.Clientes.BusinessClientes();
        Business.Vendas.BusinessVendaItem vendaItem = new Business.Vendas.BusinessVendaItem();
        Business.Vendas.BusinessVendasPorCliente vendaPorCliente = new Business.Vendas.BusinessVendasPorCliente();

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_produto_venda produto = cboProduto.SelectedItem as Database.Entity.tb_produto_venda;

                Database.Entity.tb_venda_item vendaProduto = new Database.Entity.tb_venda_item();
                vendaProduto.id_produto_venda = produto.id_produtos;
                vendaProduto.tb_produto_venda = produto;               
                vendaProduto.qt_produto = Convert.ToInt32(nudQuantidade.Value);
                nudTotal.Value += nudPreco.Value * Convert.ToInt32(nudQuantidade.Value);
                vendaProduto.vl_total = nudTotalProduto.Value * nudQuantidade.Value;

                var produtos = dgvListaDeProdutos.DataSource as List<Database.Entity.tb_venda_item>;
                if (produtos == null)
                    produtos = new List<Database.Entity.tb_venda_item>();
                produtos.Add(vendaProduto);

                dgvListaDeProdutos.AutoGenerateColumns = false;
                dgvListaDeProdutos.DataSource = null;
                dgvListaDeProdutos.DataSource = produtos;

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void CarregarCampos()
        {
            //try
            //{
                cboProduto.ValueMember = nameof(Database.Entity.tb_produto_venda.nm_produto);
                cboProduto.DataSource = produtos.ListarProdutos();

                cboFuncionario.ValueMember = nameof(Database.Entity.tb_funcionario.nm_funcionario);
                cboFuncionario.DataSource = func.ListarFuncionarios();

                cboCliente.ValueMember = nameof(Database.Entity.tb_cliente.nm_cliente);
                cboCliente.DataSource = clientes.ListarClientes();

                cboCategoria.ValueMember = nameof(Database.Entity.tb_produto_venda.ds_categoria);
                cboCategoria.DataSource = produtos.ListarProdutos();
            //}
            //catch (Exception)
            //{
            //    MessageBox.Show("Ocorreu um erro");
            //}

        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_funcionario funcionario = cboFuncionario.SelectedItem as Database.Entity.tb_funcionario;
                Database.Entity.tb_cliente cliente = cboCliente.SelectedItem as Database.Entity.tb_cliente;
                Database.Entity.tb_venda_por_cliente VendaPorCliente = new Database.Entity.tb_venda_por_cliente();
                Database.Entity.tb_produto_venda pv = cboProduto.SelectedItem as Database.Entity.tb_produto_venda;
                VendaPorCliente.id_cliente = cliente.id_cliente;
                VendaPorCliente.nm_responsavel_venda = funcionario.nm_funcionario;
                VendaPorCliente.qt_parcelas = Convert.ToInt32(nudParcelas.Value);                
                VendaPorCliente.dt_venda = dtpDataDeVenda.Value;
                VendaPorCliente.ds_pagamento = cboModoDePagamento.Text;
                VendaPorCliente.id_funcionario = funcionario.id_funcionario;

                vendaPorCliente.InserirVendaPorCliente(VendaPorCliente);                

                var produtos = dgvListaDeProdutos.DataSource as List<Database.Entity.tb_venda_item>;
                foreach (Database.Entity.tb_venda_item item in produtos)
                {
                    Database.Entity.tb_venda_item product = new Database.Entity.tb_venda_item();
                    item.id_venda_por_cliente = VendaPorCliente.id_venda_por_cliente;
                    item.tb_produto_venda = null;
                    item.id_produto_venda = pv.id_produtos;
                    product.id_venda_por_cliente = item.id_venda_por_cliente;
                    product.id_produto_venda = item.id_produto_venda;
                    product.qt_produto = item.qt_produto;
                    product.vl_total = item.vl_total;

                    vendaItem.InserirVendaItem(product);
                }
                MessageBox.Show("Venda concluída com sucesso");

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //catch (Exception)
            //{
            //    MessageBox.Show("Ocorreu um erro");
            //}
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Database.Entity.tb_produto_venda produto = cboProduto.SelectedItem as Database.Entity.tb_produto_venda;

            nudPreco.Value = produto.vl_preco;
            txtObservacao.Text = produto.ds_observacao;
            nudTotalProduto.Value = produto.vl_preco;
            nudQuantidade.Value = 1;
        }

        
    }
}
