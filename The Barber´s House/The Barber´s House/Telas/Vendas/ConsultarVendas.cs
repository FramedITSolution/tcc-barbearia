﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace The_Barber_s_House.Telas.Vendas
{
    public partial class ConsultarVendas : Form
    {
        public ConsultarVendas()
        {
            InitializeComponent();
            dgvLista.AutoGenerateColumns = false;
        }

        Business.Vendas.BusinessVendasPorCliente venda = new Business.Vendas.BusinessVendasPorCliente();

        private void btnListar_Click(object sender, EventArgs e)
        {
            try
            {
                dgvLista.DataSource = venda.ListarVendas();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudID.Value);
                dgvLista.DataSource = venda.ConsultarVendaPorFunc(id);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro");
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
            Hide();
        }

        private void picVoltar_Click(object sender, EventArgs e)
        {
            Outros.Menu menu = new Outros.Menu();
            menu.Show();
            Hide();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
    }
}
