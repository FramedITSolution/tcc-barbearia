﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Objetos
{
    class FolhasDePagamento
    {
        public Modelos.folhaDePagamento GerarFolha(Database.Entity.tb_fopag folha, decimal taxaFGTS, decimal horaExtra, decimal taxaAte,
            decimal taxaEntre1, decimal taxaEntre2,
            decimal taxaAcima, int qtFaltas, int passagesVezes, decimal valorAte)
        {
            Modelos.folhaDePagamento resultado = new Modelos.folhaDePagamento();

            //resultado.TotalProventos = CalcularProventos(folha.tb_funcionario.vl_salario_bruto, horaExtra, folha);

            resultado.INSS = CalcularINSS(folha.tb_funcionario.vl_salario_bruto, horaExtra, valorAte, taxaAte, taxaEntre1, taxaEntre2, taxaAcima);

            resultado.FGTS = CalcularFGTS(folha.tb_funcionario.vl_salario_bruto, horaExtra, taxaFGTS);

            resultado.VT = CalcularVT(folha.tb_funcionario.vl_salario_bruto, passagesVezes);

            resultado.Faltas = CalcularFaltas(qtFaltas, folha.tb_funcionario.vl_salario_bruto);

            resultado.Insalubridade = CalcularInsalubridade(1039, CalcularSalarioBase(folha.tb_funcionario.vl_salario_bruto, horaExtra));

            resultado.SalarioBase = CalcularSalarioBase(folha.tb_funcionario.vl_salario_bruto, horaExtra);

            resultado.HoraExtra = CalcularHoraExtra50(folha.tb_funcionario.vl_salario_bruto, horaExtra);

            resultado.DSR = CalcularDSR(resultado.HoraExtra);

            resultado.TotalDescontos = resultado.VT + resultado.INSS + folha.tb_funcionario.tb_vale_refeicao.vl_vale_refeicao + folha.tb_funcionario.tb_vale_alimentacao.vl_vale_alimentacao +
                folha.tb_funcionario.tb_plano_de_saude.vl_plano + folha.tb_funcionario.tb_plano_odontologico.vl_plano + folha.tb_funcionario.tb_seguro_vida.vl_plano +
                resultado.Faltas;

            resultado.SalarioLiquido = resultado.TotalProventos - resultado.TotalDescontos;


            return resultado;
        }

       /*private decimal CalcularProventos(decimal salario, decimal horaExtra, Database.Entity.tb_fopag folha)
        {
            decimal horaExtraTotal = CalcularHoraExtra50(salario, horaExtra);
            decimal dsr = CalcularDSR(horaExtraTotal);
            decimal salarioFamilia = folha.vl_salario_familia;
            decimal insalubridade = CalcularInsalubridade(980.00m, horaExtraTotal + dsr + salario); // Alterar O salário mímimo
            decimal proventos = horaExtraTotal + salario + dsr + salarioFamilia + insalubridade;
            return proventos;
        }
        */
        private decimal CalcularHoraExtra50(decimal salario, decimal horaExtra)//Provento
        {
            decimal horaExtraValor = salario / 220;
            decimal horaExtraTrabalhada = (horaExtraValor * 0.50m) + horaExtraValor;
            decimal total = horaExtraTrabalhada * horaExtra;
            return total;
        }

        public decimal CalcularHoraExtra100(decimal salario, decimal horaExtra)//Provento
        {
            decimal horaExtraValor = salario / 220;
            decimal horaExtraTrabalhada = (horaExtraValor * 1.00m) + horaExtraValor;
            decimal total = horaExtraTrabalhada * horaExtra;
            return total;
        }

        private decimal CalcularDSR(decimal horaExtraTotal)//Provento
        {
            decimal dsr = (horaExtraTotal / 26) * 4;
            return dsr;
        }

        //Desconto
        private decimal CalcularINSS(decimal salario, decimal horaExtra, decimal valorAte, decimal taxaAte,
            decimal taxaEntre1, decimal taxaEntre2, decimal taxaAcima)
        {
            Database.Entity.tb_inss insstaxa = new Database.Entity.tb_inss();

            decimal baseInss = CalcularSalarioBase(salario, horaExtra);
            decimal inss = 0;
            if (salario <= valorAte)//Vai vir de consulta
            {
                inss = baseInss * taxaAte;
            }

            else if (salario >= 0 && salario < insstaxa.vl_salario_inicial)
            {
                inss = baseInss * taxaEntre1;
            }

            else if (salario >= insstaxa.vl_salario_inicial && salario < insstaxa.vl_salario_maximo)
            {
                inss = baseInss * taxaEntre2;
            }

            else
            {
                inss = baseInss * taxaAcima;
            }

            return inss;
        }

        private decimal CalcularFGTS(decimal salario, decimal horaExtra, decimal taxaFGTS)
        {
            decimal baseFGTS = CalcularSalarioBase(salario, horaExtra);
            decimal FGTS = baseFGTS * taxaFGTS;
            return FGTS;
        }
        Business.RH.Outros.BusinessFGTS tb_fgtsDatabase = new Business.RH.Outros.BusinessFGTS();
        Business.RH.Outros.BusinessTaxaVTD taxaVTDatabase = new Business.RH.Outros.BusinessTaxaVTD();

        private decimal CalcularVT(decimal salario, int passagesVezes)
        {
            Database.Entity.tb_taxa_vt taxaVT = taxaVTDatabase.ConsultarTaxa();


            decimal vtBase = salario * taxaVT.vl_taxa;
            decimal passagemTotal = (passagesVezes * 4.20m) * 26;
            decimal vt;

            if (vtBase < passagemTotal)
            {
                vt = vtBase;
            }
            else
            {
                vt = passagemTotal;
            }

            return vt;
        }

        private decimal CalcularFaltas(int qtFaltas, decimal salario)
        {
            decimal valorFalta = salario / 30;
            decimal totalFaltas = valorFalta * qtFaltas;
            return totalFaltas;
        }

        private decimal CalcularInsalubridade(decimal salarioMinimo, decimal salarioBase)
        {
            decimal insalubridade = (salarioMinimo * 0.10m) + salarioBase;
            return insalubridade;
        }

        private decimal CalcularSalarioBase(decimal salario, decimal horaExtra)
        {
            decimal horaExtraTotal = CalcularHoraExtra50(salario, horaExtra);
            decimal dsr = CalcularDSR(horaExtraTotal);
            decimal salarioBase = salario + horaExtraTotal + dsr;
            return salarioBase;
        }
    }
}
