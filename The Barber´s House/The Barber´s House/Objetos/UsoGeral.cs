﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace The_Barber_s_House.Objetos
{
    class UsoGeral
    {
        public bool VerificarEmail(string email)
        {
            // Verifica se o e-mail possui "@"
            bool contem = email.Contains("@");
            return contem;
        }

        public bool ValidarEmail(string email)
        {
            //Verifica se a primeira ou a última letra é "@" ou se não contém ".com" no final do e-mail
            string primeiraLetra = email.Substring(0, 1);
            int tamanho = email.Length;
            string parteFinal = email.Substring(tamanho - 4, 4);
            string últimaLetra = email.Substring(tamanho - 1, 1);
            if (primeiraLetra == "@" || últimaLetra == "@" || parteFinal != ".com")
            {
                return true;
            }
            return false;
        }

        public bool ValidarTelefone(string telefone, bool checado)
        {
            string valido = telefone.Substring(5, 1);
            if (valido == "9" && checado == true)
            {
                return true;
            }
            return false;
        }

        public bool ValidarDatadeNasc(DateTime nasc, DateTime atual)
        {
            if(atual.Year - nasc.Year >= 16 && nasc.Day <= atual.Day)
            {
                return true;
            }
            return false;
        }

        public bool VerificarHorário(string horário)
        {
            int n0 = Convert.ToInt32(horário.Substring(0, 2));
            int n1 = Convert.ToInt32(horário.Substring(0, 1));
            int n2 = Convert.ToInt32(horário.Substring(1, 1));
            int n3 = Convert.ToInt32(horário.Substring(3, 1));

            while (n0 == 00 || n1 == 0 || n1 >= 3 && n2 >= 5 || n3 >= 6)
            {
                return true;
            }
            return false;
        }
    }
}
